# Q2 2024 Learning Strategy

## Algorithms and Data Structures

### Video

[Princeton University Algorithms, Part I](https://www.coursera.org/learn/algorithms-part1)

[Princeton University Algorithms, Part II](https://www.coursera.org/learn/algorithms-part2)

[MIT Introduction To Algorithms](https://ocw.mit.edu/courses/6-006-introduction-to-algorithms-spring-2020/)

[MIT Introduction to Algorithms - YT Playlist](https://www.youtube.com/playlist?list=PLUl4u3cNGP63EdVPNLG3ToM6LaEUuStEY)

[Stanford University - Divide and Conquer, Sorting and Searching, and Randomized Algorithms](https://www.coursera.org/learn/algorithms-divide-conquer)

[Algorithms Illuminated Omnibus Edition (Tim Roughgarden 2022)](https://www.algorithmsilluminated.org/)

[datastructur.es CS 61B Spring 2024 > JAVA](https://sp24.datastructur.es/)

[**University of California San Diego - Data Structures and Algorithms Specialization**](https://www.coursera.org/specializations/data-structures-algorithms?irclickid=19f2wbVDfxyPRNGXt0WQfTEoUkH3CGW%3Ad1h8QU0&irgwc=1&utm_medium=partners&utm_source=impact&utm_campaign=3117765&utm_content=b2c)


[University of California San Diego - Data Structures and Algorithms Specialization **YOUTUBE Playlist**](https://www.youtube.com/watch?v=CAfMYNNsVAo&list=PLtS8Ubq2bIlVAVwSa9NwPMQINvm00CG2L)

[UCSD Data Structures and Algorithms Specialization **EXAMPLES**](../_Full_Courses_completed/Algorithms_&_Data_Structure/Data-Structures-and-Algorithms-Coursera-UCSD-HSE-master)

[**Data Structures and Algorithms in Python**  Certification](https://jovian.com/learn/data-structures-and-algorithms-in-python)

### Books

[Introduction to Algorithms by CLRS](./Introduction.to.Algorithms.4th.Leiserson.Stein.Rivest.Cormen.MIT.Press.9780262046305.EBooksWorld.ir.pdf)

[Grokking Algorithms 2nd edition 2023 (Bhargava)](./books/Grokking%20Algorithms%20(MEAP%20v4)%202ed%202023.pdf)

## Statistics

### Video

[Análisis estadístico utilizando R UNQ](https://spiousas.github.io/Inferencia_con_R_UNQ2022/)

### Book

[An Introduction to Statistical Learning (Python)](./An%20Introduction%20to%20Statistical%20Learning%20(Python).pdf)

## Databases

## C++

### Video

### Text based course

[learncpp.com](https://www.learncpp.com/)

### Books

[C++ Primer (5th Edition) by Lippman](./books/C++%20Primer%20(5th%20Edition).pdf)

[C++ Primer Plus 6th Edition by Prata](./books/C++%20Primer%20Plus%206th%20Edition.pdf)

## Roadmap

[roadmap.sh > Computer Science](https://roadmap.sh/computer-science)

[UBA - Algoritmos y Estructura de Datos 1](https://campus.exactas.uba.ar/course/view.php?id=987&section=5)

[UBA - Data Mining](https://datamining.dc.uba.ar/datamining/programa-de-estudios/)

[c++ roadmap.sh](https://roadmap.sh/cpp)

## Resources

[https://the-algorithms.com/](https://the-algorithms.com/)

GitHub's largest open-source algorithm library

[Awesome Algorithms](https://github.com/tayllan/awesome-algorithms)

A curated list of awesome places to learn and/or practice algorithms.

[TheAlgorithms / Python](https://github.com/TheAlgorithms/Python)

Implementations are for learning purposes only. They may be less efficient than the implementations in the Python standard library. 

[CS Video Courses](https://github.com/Developer-Y/cs-video-courses?tab=readme-ov-file#database-systems)

[The Algorithms  - Javascript](https://github.com/TheAlgorithms/JavaScript)

JavaScript Repository of TheAlgorithms, which implements various algorithms and data structures in JavaScript.

[Modern CPP Programming](https://github.com/federico-busato/Modern-CPP-Programming)

[Programming in C/C++ Efficiently](https://github.com/ShiqiYu/CPP)

[datastructur.es CS 61B Spring 2024 > JAVA](https://sp24.datastructur.es/)

[Problem Solving with Algorithms and Data Structures using Python](https://runestone.academy/ns/books/published/pythonds3/index.html)

To review the ideas of computer science, programming, and problem-solving.