# Code Notes

Table of Contents

- [Code Notes](#code-notes)
  - [Programming](#programming)
    - [Python](#python)
    - [HTML](#html)
    - [Javascript](#javascript)
  - [Frameworks](#frameworks)
    - [Flask](#flask)
  - [Tools](#tools)
    - [Git](#git)
    - [Markdown](#markdown)
  - [Server](#server)
    - [LINUX](#linux)
  - [Algorithms and Data Structures](#algorithms-and-data-structures)
- [Full Stack](#full-stack)
  - [Database](#database)
  - [Frontend](#frontend)
  - [Audio](#audio)

## Programming

### Python

- [Comprehensive Python Cheatsheet](./topics/Python/Comprehensive_Python_Cheatsheet.md)
- [DSP](./topics/Python/DSP.md)
- [Virtual envs](topics/Python/VirtualEnv.md)
- [with](./topics/Python/with.md)
- [pip](./topics/Python/pip.md)
- [Work with relative paths](./topics/Python/Work_with_Relative_paths.md)
- [UnicodeEncodeError: 'ascii' codec can't encode character in position](./topics/Python/UnicodeEncodeError_'ascii'_codec_cant_encode.md)
- [Send Email list](./topics/Python/Send_emails_list.md)
- [Executable Python File](./topics/Python/Executable_Python_File.md)
- [Default Python pip version (MAC)](./topics/Python/Default_Python_version_(MAC).md)
- [Pandoc as a subprocess](./topics/Python/Pandoc_as_a_Subprocess.md)
- [Convert seconds to time format](./topics/Python/Convert_seconds_into_time_format.md)
- [Run x86-64 venv or libraries](./topics/Python/Run_x86_venv_libraries.md)
- [Run multiple Python version and virtual environments](./topics/Python/Run_multiple_python_versions_on_virtual_environtments.md)
- [CSV files basics](./topics/Python/CSV_files_basics.md)
- [Timestamps as a file name](./topics/Python/Timestmap_filename.md)
- [Selenium web scraping](./topics/Python/Selenium_webscrapping.md)
- [PyAutoGUI](./topics/Python/PyAutoGUI.md)
- [Schedule Library](./topics/Python/schedule_library.md)
- [Run selenium bot using headless mode](./topics/Python/Run_selenium_usign_headless.md)
- [Chrome Automation on Linux Server](./topics/Python/Chrome_Automation_on_server.md)

### HTML

- [HTML Basics](topics/HTML/Basics.md)
- [Hidden Button](./topics/HTML/Hidden_Button.md)
- [Print messages like terminal](./topics/HTML/Print_messages_like_terminal.md)
- [Change default text in input type="file"](./topics/HTML/Change%20default%20text%20in%20input%20type%3D'file'.md)
- [Columns](./topics/HTML/Columns.md)

### Javascript

- [Simple Recorder](./topics/Javascript/Simple_recorder.md)

## Frameworks

### Flask

- [FLask Basics](./topics/Flask/Basics.md)
- [Minimal app](https://flask.palletsprojects.com/en/2.2.x/quickstart/)
- [Routing](topics/Flask/Routing.md)
- [Redirect](./topics/Flask/Redirect.md)
- [Context](https://flask.palletsprojects.com/en/2.2.x/appcontext/)
- [Download a file with a button](./topics/Flask/Download_file.md)
- [Deploy Flask app on VPS](./topics/Flask/Deploy_Flask_app_on_VPS.md)
- [Configure Flask Dev Server to be Visible Across the Network](./topics/Flask/Configure_Flask_Dev_Server_to_be_Visible_Across_the_Network.md)
- [Send Values between pages and func](./topics/Flask/send_values_between_pages.md)
- [Send drom dropdown to Flask whithout press a button](./topics/Flask/Send_value_from_dropwdown_to_flask-in_whitout_press_a_button.md)
- [Read a text file and display the content](./topics/Flask/Read_text_file_and_display_content.md)
- [Separate long process in multiple pages](./topics/Flask/Separate_long_process_in_multiple_pages.md)
- [Upload button](./topics/Flask/Upload_button.md)
- [Display csv table](./topics/Flask/Display_csv_table.md)
- [List of useful how to](./topics/Flask/List_of_useful_how_to.md)

## Tools

### Git

- [Git Basics](./topics/Git/Basics.md)
- [Git cheatsheet Markdown](./topics/Git/Cheatsheet_1.md)
- [Github Git cheatsheet PDF](./topics/Git/pdf/git-cheat-sheet-education.pdf)
- [Gitlab Git cheatsheet](./topics/Git/pdf/gitlab-git-cheat-sheet.pdf)
- [Gitlab Git cheatsheet commands](./topics/Git/Cheatsheet_Git_commands.md)
- [Git Advance](./topics/Git/Advance.md)
- [Version Control Service (VCS)](./topics/Git/VCS.md)
- [Undoing](./topics/Git/Undoing.md)
- [gitignore](./topics/Git/gitignore.md)
- [Discart local changes](./topics/Git/Discart_local_changes.md)
- [Create a new branch](./topics/Git/Create_new_branch.md)
- [GIT Most Used Commands](./topics/Git/Most_Used_Commands.md)
- [Git Visual Cheatsheet](./topics/Git/Git_Visual_Cheatsheet.md)

### Markdown

- [MArkdown nad pandoc](./topics/Markdown/Markdown_and_pandoc.md)
- [Mardkdown metadata for pandoc](./topics/Markdown/Markdown_metadata_variables_for_pandoc.md)
- [Markdown Here](./topics/Markdown/Markdown_here.md)
- [Markdown tips and tricks](./topics/Markdown/Markdown_tips_and_tricks.md)
- [Markdown tools](./topics/Markdown/Markdown_tools.md)
- [MArkdown compatibility](./topics/Markdown/Markdown_compatibility.md)

## Server

### LINUX

- [Commands](./topics/Linux/Commands.md)
- [Folder Permission](./topics/Linux/FolderPermissions.md)
- [Server Monitor](./topics/Linux/server_monitor.md)
- [SSL](./topics/Linux/SSL.md)
- [Relative / absolute paths](./topics/Linux/Relative_absolute_paths.md)
- [Server Timeouts](./topics/Linux/server_timeouts.md)
- [Migrate a WP website to a VPS manually](./topics/Linux/Migrate_WP_web_to_vsps.md)
- [Hosting multiple Flask apps on subdomains using Apache/mod_wsgi](./topics/Linux/Hosting_multiple_Flask_apps_Apache.md)
- [Install SSL on VPS Using Certbot](./topics/Linux/Install%20SSL%20on%20VPS%20using%20Cerbot.md)
- [Deploy flask on subdomains (Apache)](./topics/Linux/Deploy_flask_on_subdomain.md)
- [Install PHP on Ubuntu](./topics/Linux/Install_PHP_Ubuntu.md)
- [Apache Cheatsheet](./topics/Linux/Apache_cheatsheet.md)
- [Cronetab schudle](./topics/Linux/Crontab_Schedule.md)
- [Folder Permissions](./topics/Linux/Folder_permissions_upload_write_flaskapp.md)

## Algorithms and Data Structures

- [Algorithms and Data Structures (FCC)](./topics/Algorithms_Data_Structures/00_Intruduction_Algorithms.md)
- [Linear search](./topics/Algorithms_Data_Structures/01_Linear_search.py)
- [Binary Search](./topics/Algorithms_Data_Structures/02_Binary_search.py)
- [Recursive Ninary search](./topics/Algorithms_Data_Structures/03_Recursive_binary_search.py)
- [Introduction to Data Structures](./topics/Algorithms_Data_Structures/04_Introduction_Data_Structures_Arrays.md)
- [Arrays Linear search](./topics/Algorithms_Data_Structures/05_Arrays_Linear_Search.py)
- [Merge sort](./topics/Algorithms_Data_Structures/07_merge_sort.py)
- [Linked list merge sorted](./topics/Algorithms_Data_Structures/08_Linked_list_merge_sorted.py)
- [Linked list](./topics/Algorithms_Data_Structures/Linked_list.py)

# Full Stack

## Database

- [JSON Cheatsheet](./topics/database/JSON_cheatsheet.md)

## Frontend

- [Multi Language website](./topics/Full_stack/Backend/Multilanguage_website_Json_JS.md)

## Audio
