# Change default text in input type="file"?

[**Back to index**](../../README.md)

[Reference link 1](https://www.w3schools.com/howto/howto_css_two_columns.asp)

[Reference link 2](https://wonderdevelop.com/div-2-columns/#:~:text=To%20split%20the%20div%20into%20two%20columns%2C%20we%20set%20the,the%20class%20name%20%E2%80%9Cright%E2%80%9D.)

```HTML
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 50%;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
</head>
<body>

<h2>Two Equal Columns</h2>

<div class="row">
  <div class="column" style="background-color:#aaa;">
    <h2>Column 1</h2>
    <p>Some text..</p>
  </div>
  <div class="column" style="background-color:#bbb;">
    <h2>Column 2</h2>
    <p>Some text..</p>
  </div>
</div>

</body>
</html>
```
