# Print messages like temrinal

[**Back to index**](../../README.md)

## Print messages like terminal in the index.html

- **Flask**

```python
from flask import Flask, render_template, request
import speech_recognition as sr

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/record', methods=['POST'])
def record():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        r.adjust_for_ambient_noise(source)
        print("Recording...")
        audio = r.listen(source)
        print("Done recording.")
    text = r.recognize_google(audio)
    return text

@app.route('/upload', methods=['POST'])
def upload():
    r = sr.Recognizer()
    audio_file = request.files['file']
    audio_content = audio_file.read()
    audio = sr.AudioFile(audio_content)
    with audio as source:
        r.adjust_for_ambient_noise(source)
        print("Transcribing...")
        audio = r.record(source)
        print("Done transcribing.")
    text = r.recognize_google(audio)
    return text

if __name__ == '__main__':
    app.run()
```

- **index.html**

In this HTML code, I've added a onclick event to the record button to call a JavaScript function that updates the recording status message for 5 seconds. However, if you prefer not to use JavaScript, you can remove the onclick event and just display a static message.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Speech to Text</title>
  </head>
  <body>
    <h1>Speech to Text</h1>
    <form action="/record" method="post">
      <button type="submit" onclick="recording()">Record</button>
      <span id="recording-status"></span>
    </form>
    <br />
    <form action="/upload" method="post" enctype="multipart/form-data">
      <input type="file" name="file" accept="audio/*" />
      <button type="submit">Upload</button>
      <span id="upload-status"></span>
    </form>
    <script>
      function recording() {
        const status = document.getElementById("recording-status");
        status.innerHTML = "Recording...";
        setTimeout(() => {
          status.innerHTML = "Stop recording.";
        }, 5000);
      }
    </script>
  </body>
</html>
```
