# Change default text in input type="file"?

[**Back to index**](../../README.md)

[Reference link](https://stackoverflow.com/questions/5138719/change-default-text-in-input-type-file)

## Solution 1

Use the for attribute of label for input.

```html
<div>
  <label for="files" class="btn">Select Image</label>
  <input id="files" style="visibility:hidden;" type="file">
</div>
```

Below is the code to fetch name of the uploaded file

```html
$("#files").change(function() {
  filename = this.files[0].name;
  console.log(filename);
});
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div>
    <label for="files" class="btn">Select Image</label>
    <input id="files" style="visibility:hidden;" type="file">
</div>
```

## Solution 2

```html
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>JS Bin</title>
</head>

<body>
  <button style="display:block;width:120px; height:30px;" onclick="document.getElementById('getFile').click()">Your text here</button>
  <input type='file' id="getFile" style="display:none">
</body>

</html>
```
