# Hiden Button

[**Back to index**](../../README.md)

HTML < button > hidden Attribute
A hidden attribute on a < button > tag hides the button.

Although the button is not visible, its position on the page is maintained.

Example

An invisible button:

```html
<p>An invisible button:</p>

<button type="button" hidden>My Button</button>
```

## Using hidden

The hidden attribute hides the < button > element.

You can specify either 'hidden' (without value) or 'hidden="hidden"'. Both are valid.

A hidden < button > is not visible, but maintains its position on the page.

Syntax

`<button hidden>`

or

`<button hidden="hidden">`
