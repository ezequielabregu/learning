# Separate long process in multiple pages

[**Back to index**](../../README.md)

## Problem

create a Flask app that uses a separate process for long-running tasks. Instead of running the task in the handling view, the view starts the running of the task in a separate process, then immediately returns a response. The response brings the user to a "Please wait, we're processing" page. This page can use push technologies to determine when the task was completed. The app uses the Flask library and the multiprocessing library for running processes. The steps involved in creating the app include importing necessary libraries, initializing Flask, creating a view function for starting the process, creating a function for the long-running task, creating a template for the processing page, updating the view function to redirect to the processing page, and starting the Flask app.

## Solution

This app has three endpoints:

1. `/`: Displays the homepage that contains a button to start the long-running task.
2. `/start-process`: Starts the long-running task in a separate process and redirects to the processing page.
3. `/processing`: Displays the "Please wait, we're processing" page and uses a JavaScript script to periodically check if the task has completed. Once the task is completed, the user is redirected to the result page.
The app uses the Flask library, the multiprocessing library for running processes, and a simple JavaScript script for real-time updates on the processing page.

The `long_running_task()` function simulates a long-running task that takes 10 seconds to complete. You can modify this function to perform any task that takes a long time to complete.

- Python Flask code

```python
from flask import Flask, render_template, redirect, url_for
import multiprocessing
import time

app = Flask(__name__)
long_running_process = None

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/start-process')
def start_process():
    global long_running_process
    long_running_process = multiprocessing.Process(target=long_running_task)
    long_running_process.start()
    return redirect(url_for('processing'))

@app.route('/processing')
def processing():
    return render_template('processing.html')

@app.route('/result')
def result():
    return render_template('result.html')

@app.route('/check-process')
def check_process():
    global long_running_process
    # Check if the long-running task has completed
    if long_running_process and long_running_process.is_alive():
        return "processing"
    else:
        return "completed"

def long_running_task():
    # Simulating long-running task
    time.sleep(10)
    return True

if __name__ == '__main__':
    app.run(debug=True)
```

- index.html
  
```html
<!DOCTYPE html>
<html>
  <head>
    <title>Long-Running Task Example</title>
  </head>
  <body>
    <h1>Long-Running Task Example</h1>
    <form action="{{ url_for('start_process') }}">
      <button type="submit">Start Long-Running Task</button>
    </form>
  </body>
</html>
```

- processing.hmtl

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Processing Page</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
      $(document).ready(function() {
        setInterval(function() {
          $.ajax({
            url: "{{ url_for('check_process') }}",
            type: "GET",
            success: function(data) {
              if (data === "completed") {
                window.location.replace("{{ url_for('result') }}");
              }
            },
            error: function() {
              console.log("Error checking process status");
            }
          });
        }, 2000);
      });
    </script>
  </head>
  <body>
    <h1>Please wait, we're processing your request.</h1>
  </body>
</html>
```

- result.html

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Result Page</title>
  </head>
  <body>
    <h1>Task Completed Successfully!</h1>
  </body>
</html>
```
