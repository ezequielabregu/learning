# Send Values between pages and func

[**Back to index**](../../README.md)

To store the value of a in a session variable and access it between views, you can use the session object provided by your web framework. The exact syntax may vary depending on the framework you are using, but here is a general example in Python using the Flask framework:

```python
from flask import Flask, session

app = Flask(__name__)
app.secret_key = 'your_secret_key_here'

@app.route('/set_a/<int:a>')
def set_a(a):
    session['a'] = a
    return 'Value of a set to {}'.format(a)

@app.route('/get_a')
def get_a():
    a = session.get('a')
    if a is None:
        return 'Value of a is not set'
    else:
        return 'Value of a is {}'.format(a)
```

Send a list to another page:

```python
from flask import Flask, request, session

app = Flask(__name__)
app.secret_key = 'your_secret_key'

@app.route('/submit-option', methods=['POST'])
def submit_option():
    data = request.get_json()
    selected = data['selectedOption']
    selected_option.append(selected)
    session['selected_option'] = selected_option
    print(selected_option)
    # Do something with the selected option
    return selected_option

@app.route('/another-page')
def another_page():
    selected_option = session.get('selected_option')
    # Do something with the selected option
    return 'Selected option: {}'.format(selected_option)

def my_function(session):
    selected_option = session.get('selected_option')
    # Do something with the selected option

```

## How to pass a variable between Flask pages

[Reference Link](https://stackoverflow.com/questions/27611216/how-to-pass-a-variable-between-flask-pages)

If you want to pass some python value around that the user doesn't need to see or have control over, you can use the session:

```python
@app.route('/a')
def a():
    session['my_var'] = 'my_value'
    return redirect(url_for('b'))

@app.route('/b')
def b():
    my_var = session.get('my_var', None)
    return my_var
```

You should avoid putting large amounts of data in the session, since it has to be sent to and from the client every request. For large amounts of data, use a database or other data storage. See Are global variables thread safe in flask? How do I share data between requests? and Store large data or a service connection per Flask session.

If you want to pass a value from a template in a url, you can use a query parameter:

`<a href="{{ url_for('b', my_var='my_value') }}">Send my_value</a>`
will produce the url:

`/b?my_var=my_value`

which can be read from b:

```python
@app.route('/b')
def b():
    my_var = request.args.get('my_var', None)
```

### Using the following python flask code show me how to send the selected_option list to print into transcribe ()

```python
from flask import Flask, render_template, request, redirect, url_for, send_file, session
import speech_recognition as sr
import os
import multiprocessing

#Crete a new instance Flask class
app = Flask(__name__)
app.secret_key = 'your_secret_key'

long_running_process = None
result = None

server_path = ""#"/var/www/html/apps/speech_text/"
audio_path = server_path + "audio.wav"
text_path = server_path + "output.txt"

selected_option = []

@app.route('/submit-option', methods=['POST'])
def submit_option():
    data = request.get_json()
    global selected_option
    selected = (data['selectedOption'])
    selected_option.append(selected)
    session["selected"] = "69"
    print(selected_option)
    # Do something with the selected option
    return 'selected_option'

@app.route("/", methods=['POST', 'GET'])
def index():
    if request.method == "POST":
        f = request.files['audio_data']
        with open(audio_path, 'wb') as audio:
            f.save(audio)
        print('file uploaded successfully')

        return render_template('index.html', request="POST")
    else:
        return render_template("index.html")

#Audio uploader
@app.route('/upload', methods=['POST'])
def upload():
    #requiest the file name
    f = request.files['file']
    #save the audio file
    f.save(audio_path)
    print ('Audio file uploaded')
    return render_template('index.html', uploadedName=f.filename)
    #return redirect(url_for('transcribe'))  
    #return 'File uploaded successfully'

@app.route('/download')
def download_file():
    path = text_path   
    return send_file(path, as_attachment=True)

@app.route('/start-process')
def start_process():
    global long_running_process
    long_running_process = multiprocessing.Process(target=transcribe)
    long_running_process.start()
    return redirect(url_for('processing'))

@app.route('/processing')
def processing():
    return render_template('processing.html')

@app.route('/result')
def result():
    with open(text_path, 'r') as file:
        text = file.read()
        return render_template('result.html', text=text)

@app.route('/transcribe')
def check_process():
    global long_running_process
    # Check if the long-running task has completed
    if long_running_process and long_running_process.is_alive():
        return "processing"
    else:
        return "completed"

def transcribe():
    selected = session.get('selected', None)
    print (selected)
    #create a new recognizer instance
    r = sr.Recognizer()
    #use the audio uploaded as a source
    with sr.AudioFile(audio_path) as source:
        audio = r.record(source)
        try: 
            #audio to text    
            text = r.recognize_google(audio)#, language=dropdown())
       # write the text to a file
            with open(text_path, "w", encoding='utf-8') as file:
                file.write(text)
                os.remove(audio_path)            
            return True
            #return render_template('index.html', text=text)
        
        except sr.UnknownValueError:
            text="Could not understand you. Try Again!"
            os.remove(audio_path)  
            return False
            #return render_template('index.html', text=text)

        except sr.RequestError as e:
            text = "Could not request results; {0}".format(e)    
            os.remove(audio_path)  
            return False
            #return render_template('index.html', text=text)



if __name__ == '__main__':
#    app.run(host="0.0.0.0", port=5000)
#    app.run()
    app.run(debug=True)
```

To send the selected_option list to print into transcribe() function, you can pass it as an argument to the transcribe() function when you start the process. Here is an updated start_process() function that does this:

```python
@app.route('/start-process')
def start_process():
    global long_running_process
    long_running_process = multiprocessing.Process(target=transcribe, args=(selected_option,))
    long_running_process.start()
    return redirect(url_for('processing'))
```

Then, in the transcribe() function, you can receive the selected_option list as an argument and use it as needed. For example:

```python
def transcribe(selected_option):
    # do something with the selected_option list
    print(selected_option)
    # rest of the function code
```

Note that you should also pass the selected_option list to the submit_option() function so that it can be updated with the selected options. You can do this by adding it to the render_template() function call in the index() function:

```python
@app.route("/", methods=['POST', 'GET'])
def index():
    if request.method == "POST":
        f = request.files['audio_data']
        with open(audio_path, 'wb') as audio:
            f.save(audio)
        print('file uploaded successfully')

        return render_template('index.html', request="POST", selected_option=selected_option)
    else:
        return render_template("index.html", selected_option=selected_option)
```

Then, in the index.html template file, you can access the selected_option list and pass it to the submit_option() function using JavaScript. For example:

```python
<select id="dropdown" onchange="submitOption()">
  <option value="option1">Option 1</option>
  <option value="option2">Option 2</option>
</select>

<script>
function submitOption() {
  var selectedOption = document.getElementById("dropdown").value;
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/submit-option');
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onload = function() {
    if (xhr.status === 200) {
      console.log(xhr.responseText);
    }
  };
  xhr.send(JSON.stringify({selectedOption: selectedOption}));
}
</script>
```

This code sends the selected option to the submit_option() function when the user selects an option from the dropdown list. The function updates the selected_option list and saves it to the session. When the start_process() function is called, it passes the selected_option list to the transcribe() function.
