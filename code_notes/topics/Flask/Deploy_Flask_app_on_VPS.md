# Deploy a flask app

[**Back to index**](../../README.md)

[Reference 0](https://www.askpython.com/python-modules/flask/deploy-flask-app-on-vps)

[Reference 1](https://www.youtube.com/watch?v=CjC-gI0hdVs&t=615s&ab_channel=UskoKruM2010)

[Reference 2](https://youtu.be/w0QDAg85Oow)

[Reference 3](https://www.rosehosting.com/blog/how-to-deploy-flask-application-with-nginx-and-gunicorn-on-ubuntu-20-04/)

[Reference 4](https://www.codewithharry.com/blogpost/flask-app-deploy-using-gunicorn-nginx/)

- Install Python and other required dependencies

`apt-get install python3 python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools -y`

- Install Apache

`sudo apt install apache2`

`sudo apt-get install libapache2-mod-wsgi-py3`

- Install pip

`sudo apt install python-pip`

- Install Flask

`sudo pip install flask`
`sudo pip install flask_sqlalchemy`

- Create a folder in /var/www/html

`mkdir flask_app`

- Clone github repository into the folder created

`git clone <github address> .`

- Add **app.wsgi** located in the same folder
  
```python
#flaskapp.wsgi
import sys
sys.path.insert(0, '/var/www/html/flask_app')
 
from app import app as application
```

- Eneable the mod_wsgi for Apache to interact with Flask

`cd /etc/apache2/sites-enabled/`

`nano 000-default.conf`

Below DocumentRoot /var/www/html line, add the code:

```bash
WSGIDaemonProcess flask_app threads=5
WSGIScriptAlias / /var/www/html/flask_app/app.wsgi
<Directory flask_app>
    WSGIProcessGroup flask_pp
    WSGIApplicationGroup %{GLOBAL}
    Order deny,allow
    Allow from all
</Directory>
```

see this [link](https://www.askpython.com/wp-content/uploads/2020/09/conf.png) to take a look.

- Set the appropriate permissions for the directory and files:

```bash
sudo chown -R www-data:www-data /var/www/subdomain.example.com/flaskapp/
sudo chmod -R 755 /var/www/subdomain.example.com/flaskapp/
```

if chmod 755 doesn't work, try with:

`chmod -R 777 .`

- Eneable site

`sudo a2ensite example.com.conf`

- Restart Apache server

`sudo service apache2 restart`

## Tips & tricks

### Problem with android trying upload a file over wifi

[Reference link 1](https://stackoverflow.com/questions/62571183/application-failed-to-connect-to-server-python-flask-android-studio)

[Reference Link 2](https://stackoverflow.com/questions/20778771/what-is-the-difference-between-0-0-0-0-127-0-0-1-and-localhost/20778887#20778887)

- Problem
  
    *Flask app refuse to upload a file on android + wifi*

127.0.0.1 is normally the IP address assigned to the "loopback" or local-only interface. This is a "fake" network adapter that can only communicate within the same host. It's often used when you want a network-capable application to only serve clients on the same host. A process that is listening on 127.0.0.1 for connections will only receive local connections on that socket.

"localhost" is normally the hostname for the 127.0.0.1 IP address. It's usually set in /etc/hosts (or the Windows equivalent named "hosts" somewhere under %WINDIR%). You can use it just like any other hostname - try "ping localhost" to see how it resolves to 127.0.0.1.

0.0.0.0 has a couple of different meanings, but in this context, when a server is told to listen on 0.0.0.0 that means:

`"listen on every available network interface"`

**The loopback adapter with IP address 127.0.0.1 from the perspective of the server process looks just like any other network adapter on the machine, so a server told to listen on 0.0.0.0 will accept connections on that interface too**.

- Solution

Run Flask app with the following

`app.run(host="0.0.0.0", port=5000)`
