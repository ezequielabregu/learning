# FLASK

[**Back to index**](../../README.md)

## Flask basics

<https://youtu.be/mqhxxeeTbu0>
<https://flask.palletsprojects.com/en/2.2.x/quickstart/#a-minimal-application>

<https://flask.palletsprojects.com/en/1.1.x/quickstart/#quickstart>

## Deploying a flask web app

<https://testdriven.io/blog/flask-render-deployment/>

<https://youtu.be/JiA-8oVgPIM>

<https://youtu.be/1FdrJPt77GU>

## Flask mehotds

<https://flask.palletsprojects.com/en/0.12.x/quickstart/#http-methods>

```python
from flask import request

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        do_the_login()
    else:
        show_the_login_form()
```

&nbsp;

## Send values from HTML to flask / python

<https://stackoverflow.com/questions/41885919/passing-variable-from-html-to-python-flask>

Overall though, the frontend is passing the variable to the backend, it's just that the variables are only accessible via the request object from within the route to which the data is passed.

- You want to setup your form to POST the data to your backend when submitted. If you don't specify an action, then it will POST to the same page the same page that you're currently viewing.

```html
<form method="POST">
    Asset Tag:<br>
    <input type="text" name="tag"><br>
    <input type="submit" value="Submit">
</form>
```

- You need to setup your route to accept a POST request so that it can receive data from the form on your page. See here for more information on HTTP methods.

```python
@app.route('/py', methods=['GET', 'POST'])
```

- inside of your route, you'll want to check whether it was a GET request (and load a normal page) or whether it was a POST (form data was sent so we should use it)

``` python
from flask import request

@app.route('/py', methods=['GET', 'POST'])
def server():
    if request.method == 'POST':
        # Then get the data from the form
        tag = request.form['tag']

        # Get the username/password associated with this tag
        user, password = tag_lookup(tag)

        # Generate just a boring response
        return 'The credentials for %s are %s and %s' % (tag, user, password) 
        # Or you could have a custom template for displaying the info
        # return render_template('asset_information.html',
        #                        username=user, 
        #                        password=password)

    # Otherwise this was a normal GET request
    else:   
        return render_template('main.html')
```

&nbsp;

## Create a slidedown Menu en flask / html

<https://memudualimatou.medium.com/creating-a-select-tag-on-a-web-application-using-flask-python-fffe6ea0c939>

``` python
@app.route('/')
def index():
    return render_template(
        'index.html',
        data=[{'gender': 'Gender'}, {'gender': 'female'}, {'gender': 'male'}],
        data1=[{'noc': 'Number of Children'}, {'noc': 0}, {'noc': 1}, {'noc': 2}, {'noc': 3}, {'noc': 4}, {'noc': 5}],
        data2=[{'smoke': 'Smoking Status'}, {'smoke': 'yes'}, {'smoke': 'no'}],
        data3=[{'region': "Region"}, {'region': "northeast"}, {'region': "northwest"},
               {'region': 'southeast'}, {'region': "southwest"}])
```

```html
<form action="{{url_for('predict')}}" method="post">

<select name="comp_select" class="Input">
      {% for o in data %}
      <option value="{{ o.gender }}">{{ o.gender }}</option>
      {% endfor %}
</select>

<select name="comp_select1" class="Input">
      {% for o in data1 %}
      <option value="{{ o.noc }}">{{ o.noc }}</option>
      {% endfor %}
 </select>
```

## Play audio file in flask

<https://www.geeksforgeeks.org/how-to-serve-static-files-in-flask/>

```python
# audio
@app.route("/audio")
def serve_audio():
 message = "Audio Route"
 return render_template('audio.html', message=message)

```

```html
<html>
<head>
 <title>Flask Static Demo</title>
 <link rel="stylesheet" href="/static/style.css" />
</head>
<body>
 <h1>{{message}}</h1>

 <audio controls>
 <source src="/static/audio.mp3" />
 </audio>

 <script src="/static/serve.js" charset="utf-8"></script>
</body>
</html>

```

&nbsp;

## Application and Request Contexts in Flask

<https://testdriven.io/blog/flask-contexts/>
<https://flask.palletsprojects.com/en/2.2.x/appcontext/>

## Download files in flask

<https://www.youtube.com/watch?v=sy1MNWt7om4&ab_channel=CodingShiksha>

<https://dev.to/grahammorby/let-users-download-files-in-flask-5gjg>

<https://roytuts.com/how-to-download-file-using-python-flask/>

``` python
@app.route('/download')
def download():
    path = 'samplefile.pdf'
    return send_file(path, as_attachment=True)
```

```html
<a class="btn btn-success" href="{{url_for('download')}}">Download</a>
```

## How to upload file

this was the main reference to implement the upload function
<https://www.geeksforgeeks.org/how-to-upload-file-in-python-flask/>

index.html

```html
<html>
<head>
 <title>success</title>
</head>
<body>
 <p>File uploaded successfully</p>
 <p>File Name: {{name}}</p>
</body>
</html>
```

Acknowledgement.html

```html
<html>
<head>
 <title>success</title>
</head>
<body>
 <p>File uploaded successfully</p>
 <p>File Name: {{name}}</p>
</body>
</html>

```

app.py

```python
from distutils.log import debug
from fileinput import filename
from flask import *
app = Flask(__name__)

@app.route('/')
def main():
 return render_template("index.html")

@app.route('/success', methods = ['POST'])
def success():
 if request.method == 'POST':
  f = request.files['file']
  f.save(f.filename)
  return render_template("Acknowledgement.html", name = f.filename)

if __name__ == '__main__':
 app.run(debug=True)
```
