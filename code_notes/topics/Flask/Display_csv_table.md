# Print messages like temrinal

[**Back to index**](../../README.md)

[Reference link](https://code-boxx.com/csv-table-python-flask/)

```python
# (A) INIT
# (A1) LOAD MODULES
from flask import Flask, render_template
import csv
 
# (A2) FLASK SETTINGS + INIT
HOST_NAME = "localhost"
HOST_PORT = 80
app = Flask(__name__)
# app.debug = True
 
# (B) DEMO - READ CSV & GENERATE HTML TABLE
@app.route("/")
def index():
  with open("S1_dummy.csv") as file:
    reader = csv.reader(file)
    return render_template("S3_csv_table.html", csv=reader)
 
# (C) START
if __name__ == "__main__":
  app.run(HOST_NAME, HOST_PORT)
  ```

- HTML

```HTML
<table id="demo">
{% for row in csv %}
  <tr>
  {% for col in row %}
  <td>{{ col }}</td>
  {% endfor %}
  </tr>
{% endfor %}
</table>
```
