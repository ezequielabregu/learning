# Read a text file and display the content

[**Back to index**](../../README.md)

- Python code

```python
from flask import Flask
app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello, world!'

@app.route('/')
def index():
    with open('file.txt', 'r') as file:
        content = file.read()
    return render_template('index.html', content=content)
```

- HTML code

```html
<!DOCTYPE html>
<html>
<head>
    <title>Display Text File Content</title>
</head>
<body>
    <h1>Text File Content:</h1>
    <p>{{ content }}</p>
</body>
</html>
```
