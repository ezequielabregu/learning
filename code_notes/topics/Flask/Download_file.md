# Download a file

[**Back to index**](../../README.md)

- app.py

```python
from flask import Flask, render_template, send_file

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/download')
def download_file():
    path = "/path/to/file/example.pdf"
    return send_file(path, as_attachment=True)

if __name__ == '__main__':
    app.run(debug=True)
```

- index.html

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Download File</title>
  </head>
  <body>
    <h1>Download File</h1>
    <form action="/download" method="get">
      <button type="submit">Download</button>
    </form>
  </body>
</html>
```

