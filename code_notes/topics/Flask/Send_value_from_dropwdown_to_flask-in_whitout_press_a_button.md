# Send drom dropdown to Flask whithout press a button

[**Back to index**](../../README.md)

You can use JavaScript to submit the value from a dropdown menu to Flask without the need to press a submit button. Here's an example of how you can achieve this:

First, you need to create the dropdown menu in your HTML template:

```html
<select id="my-dropdown">
  <option value="option1">Option 1</option>
  <option value="option2">Option 2</option>
  <option value="option3">Option 3</option>
</select>
```

Next, you need to add an event listener to the dropdown menu that listens for changes in the selected option. When a change occurs, the JavaScript function will be called:

```JS
<script>
  document.getElementById("my-dropdown").addEventListener("change", function() {
    var selectedOption = document.getElementById("my-dropdown").value;
    sendData(selectedOption);
  });
</script>
```

Finally, you need to define the sendData function that sends the selected option to Flask using an HTTP request:

```JS
<script>
  function sendData(selectedOption) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/submit-option");
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify({"selectedOption": selectedOption}));
  }
</script>
```

In this example, we're using an AJAX request to send the selected option to the Flask server using the /submit-option endpoint. On the Flask side, you can define a route that listens for this endpoint and processes the data:

```python
from flask import Flask, request

app = Flask(__name__)

@app.route('/submit-option', methods=['POST'])
def submit_option():
    data = request.get_json()
    selected_option = data['selectedOption']
    # Do something with the selected option
    return 'Option submitted successfully'
```
