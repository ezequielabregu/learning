# Print messages like temrinal

[**Back to index**](../../README.md)

[Reference link](https://pythonbasics.org/flask-upload-file/)

[Reference linkl](https://flask.palletsprojects.com/en/2.2.x/patterns/fileuploads/)

```python
from flask import Flask, render_template, request
from werkzeug import secure_filename
app = Flask(__name__)

@app.route('/upload')
def upload_file():
   return render_template('upload.html')
 
@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['file']
      f.save(secure_filename(f.filename))
      return 'file uploaded successfully'
  
if __name__ == '__main__':
   app.run(debug = True)

```

- HTML
  
```html
<html>
   <body>
      <form action = "http://localhost:5000/uploader" method = "POST" 
         enctype = "multipart/form-data">
         <input type = "file" name = "file" />
         <input type = "submit"/>
      </form>   
   </body>
</html>
```
