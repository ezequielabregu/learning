# Markdown metadata variables for pandoc

[**Back to index**](../../README.md)

[Reference link](https://pandoc.org/MANUAL.html)
[Pandoc demos](https://pandoc.org/demos.html)
[Header metadata extended tutorial](https://www.flutterbys.com.au/stats/tut/tut17.3.html)

## Metadada examples

```text
---
title: AI transcription example
author: Ursula Robbins
geometry: margin=3cmn
fontsize: 12pt
---
```

```text
---
documentclass: extarticle
fontsize: 11pt
title-meta: 'META Title'
author-meta: 'The Author'
date-meta: '1. Juni, 2020'
keywords:
- 'keyword1'
- 'keyword2'
- 'keyword3'
subject: My Favorite Subject
---

# Introduction

...
```
```text
  ---
  title: This is the title
  author: D. Author
  date: 14-02-2013
  ...

  # Section 1
  Some text
```

---

Metadata variables
title, author, date
allow identification of basic aspects of the document. Included in PDF metadata through LaTeX and ConTeXt. These can be set through a pandoc title block, which allows for multiple authors, or through a YAML metadata block:

```text
---
author:
- Aristotle
- Peter Abelard
---
```
...

Note that if you just want to set PDF or HTML metadata, without including a title block in the document itself, you can set the title-meta, author-meta, and date-meta variables. (By default these are set automatically, based on title, author, and date.) The page title in HTML is set by pagetitle, which is equal to title by default.

`subtitle`
  
document subtitle, included in HTML, EPUB, LaTeX, ConTeXt, and docx documents

`abstract`
  
document summary, included in HTML, LaTeX, ConTeXt, AsciiDoc, and docx documents

`abstract-title`

title of abstract, currently used only in HTML and EPUB. This will be set automatically to a localized value, depending on lang, but can be manually overridden.

`keywords`

list of keywords to be included in HTML, PDF, ODT, pptx, docx and AsciiDoc metadata; repeat as for author, above

`subject`

document subject, included in ODT, PDF, docx, EPUB, and pptx metadata

`description`

document description, included in ODT, docx and pptx metadata. Some applications show this as Comments metadata.

`category`

document category, included in docx and pptx metadata
Additionally, any root-level string metadata, not included in ODT, docx or pptx metadata is added as a custom property. The following YAML metadata block for instance:

```
---
title:  'This is the title'
subtitle: "This is the subtitle"
author:
- Author One
- Author Two
description: |
    This is a long
    description.

    It consists of two paragraphs
...
```

will include title, author and description as standard document properties and subtitle as a custom property when converting to docx, ODT or pptx.
