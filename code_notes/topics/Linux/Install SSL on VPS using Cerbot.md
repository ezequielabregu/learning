# How to Install SSL on VPS Using Certbot

[**Back to index**](../../README.md)

References

[How to install SSL on VPS with Cerbot](https://support.hostinger.com/en/articles/6865487-how-to-install-ssl-on-vps-using-certbot)

[The requested URL was not found on this server after SSL. Apache. WordPress. Self host](https://stackoverflow.com/questions/60501972/the-requested-url-was-not-found-on-this-server-after-ssl-apache-wordpress-sel)

[The Requested URL was Not Found on This Server Apache](https://www.nicesnippets.com/blog/the-requested-url-was-not-found-on-this-server-apache2441-in-ubuntu-2204)

[Digital Ocean Tutorial: How To Secure Apache with Let's Encrypt on Ubuntu 22.04](https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-22-04)

## Step 1 - Prepare the VPS

Before installing the SSL, make sure that your VPS:

- Has a web server running (eg. Apache, NGINX, etc.)

- The website hosted on your VPS is set up to be opened by entering the domain name on the address bar – not the IP

- The domain is fully propagated and pointing to your VPS child nameservers.

Installing an SSL with Certbot while the domain is still propagating will create a self-signed certificate, which may cause errors when accessing your website

## Step 2 - Install Dependencies

Certbot recommends using snapd for installation.

**Since snapd is not supported on Hostinger Linux-based VPS, you can use Python by installing it first on your server**.

To start the process, connect to your VPS using SSH. Next, install the required dependencies (Python 3.6+, venv and Augeas) according to your OS:

For APT-based distributions (such as Debian or Ubuntu), run the following:

```bash
sudo apt update
sudo apt install python3 python3-venv libaugeas0
 ```

For RPM-based distributions (Fedora, CentOS), use this command:

sudo dnf install python3 augeas-libs

NOTES:

For older distributions that do not support dnf, use yum instead

Some RHEL-based distributions use python3x instead of python3 (eg. python38). Please refer to the documentation of your Linux distribution 🙂

If you have issues installing cryptography, you may need to install additional dependencies. Check this article for more information: Building Cryptography on Linux

## Step 3 - Install Certbot

To prevent any conflicts with previous versions, remove any Certbot packages already installed before installing the newest version. You can use your package manager (apt, dnf, yum, etc.) for this.

Once it's ready, run the following to set up a Python virtual environment:

```bash
sudo python3 -m venv /opt/certbot/
sudo /opt/certbot/bin/pip install --upgrade pip
```

To install Certbot, run this on Apache:

```bash
sudo /opt/certbot/bin/pip install certbot certbot-apache
```

Or this for NGINX:

```bash
sudo /opt/certbot/bin/pip install certbot certbot-nginx
```

Next, create a symbolic link so that Certbot can be executed from any path:

```bash
sudo ln -s /opt/certbot/bin/certbot /usr/bin/certbot
```

Install and activate SSL for your websites and have Certbot do all the configurations by executing the following command for Apache:

```bash
sudo certbot --apache
```

For NGINX:

```bash
sudo certbot --nginx
```

NOTE:

To obtain only the certificates and configure the SSL manually, append certonly after certbot and before --apache or --nginx.

Since the SSL is active for 90 days, it is recommended to set up automatic renewal. You can do so by running the following:

```bash
echo "0 0,12 * * * root /opt/certbot/bin/python -c 'import random; import time; time.sleep(random.random() * 3600)' && sudo certbot renew -q" | sudo tee -a /etc/crontab > /dev/null
 ```

That’s it, you can now visit your website on an incognito window to verify that SSL is properly installed and working.

## The requested URL was not found on this server after SSL. Apache

**According to my test, just edit the *-le-ssl.conf (ex: ezequielabregu-le-ssl.conf)**.

Apatch do not give permission to redirect from .htacces by default. So I have changed

`/etc/apatche2/sites-avelable/domain_name.conf`

```bash
<Directory /var/www/mydomain>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>
```

## Delete Certbot Certificate

`sudo certbot delete`

or Delete Certbot Certificate by Domain Name

`sudo certbot delete --cert-name example.com`

then (replace the name of your *-le-ssl.conf)

`nano /etc/apache2/sites-enabled/000-default-le-ssl.conf`

Remove these lines

```bash
Include /etc/letsencrypt/options-ssl-apache.conf 
ServerName example.com 
SSLCertificateFile /etc/letsencrypt/live/example.com/fullchain.pem 
SSLCertificateKeyFile /etc/letsencrypt/live/example.com/privkey.pem
```

## Web Browser recognize a valid SSL Certificate flagged by Chrome as “Not secure”

Main References:

<https://developer.chrome.com/docs/devtools/resources/>

[Chrome "Active content with certificate errors"](https://stackoverflow.com/questions/44145936/chrome-active-content-with-certificate-errors)
[Certificate valid but flagged by Chrome as “Not secure”](https://community.letsencrypt.org/t/certificate-valid-but-flagged-by-chrome-as-not-secure/187035/10)

### Option 1

- Open developer tools (Inspect)
- Go to the Application tab
- Clear storage
- Close and re-open tab

Commenters have pointed out the following:

- you must close all tabs in all windows which are open to the same domain (including subdomains)
- you can uncheck "cookies" before clearing storage to preserve login information etc

**IMPORTANT: browser restart is mandatory!**

### Option 2

- This may be equivalent to the other methods but I found it while trying to avoid restarting my browser.

- Click on the "Not secure" left of the URL.
- Click the number under Cookies (since they are almost always set) eg 4 in use.
- Click on each URL and click Remove.
- Close the tab and reopen and the warnings should have gone away and it show "Secure".

---

## Certbot Apache error "Name duplicates previous WSGI daemon definition"

[Reference link](https://stackoverflow.com/questions/47803081/certbot-apache-error-name-duplicates-previous-wsgi-daemon-definition)

[see this BUG](https://github.com/certbot/certbot/issues/8373) 

The way to get cerbot to do this for you and avoid the error without changing your config structure is to comment out the offending line. After certbot succeeds, you'll then need to edit the config files manually to uncomment the lines and make sure you choose a new daemon process name for the new HTTPS config. So, in this case you should:

1. Put a # in front of the line starting with WSGIDaemonProcess.
2. Run cerbot again and ask it to attempt to reinstall the existing cert for you. It will succeed this time.
3. Edit the original configuration file and uncomment the WSGIDaemonProcess line.
4. Edit the new configuration file that certbot created for you and uncomment the line (certbot will have copied the entire original config file for you, including any comments).
5. You'll need to rename the daemon process in this file since you can't use the same name in two different virtual hosts; I'd recommend just adding an s to the name for secure: name -> names
6. Restart Apache.

