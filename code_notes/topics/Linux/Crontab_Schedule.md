# Crontab schudle

[**Back to index**](../../README.md)

[Reference linkl](https://www.geekbitzone.com/posts/macos/crontab/macos-schedule-tasks-with-crontab/)

## The anatomy of a crontab file

Each line of a crontab file represents a job and looks like this:

```text
┌───────────── minute (0 - 59)
│ ┌───────────── hour (0 - 23)
│ │ ┌───────────── day of the month (1 - 31)
│ │ │ ┌───────────── month (1 - 12)
│ │ │ │ ┌───────────── day of the week (0 - 6) 
│ │ │ │ │
│ │ │ │ │
│ │ │ │ │

* * * * * command

```

## Editing the crontab

`crontab -e`

## Giving cron full disk access

If you are using macOS Catalina (or higher) you will also need to give the cron service Full Disk Access.

Go to the System Preferences and select Security & Privacy.

In the left-hand column, select Full Disk Access, click the padlock icon, enter your password and click on the plus icon for a file browser to appear.

When the file browser appears, press ⌘ ⇧ G to open the go to folder dialog and type:

`/usr/sbin/cron`

Press Go to close the dialog and confirm that cron appears in the list of apps that have full disk access in the Security & Privacy window.

## Displaying the crontab

To display the contents of the crontab, type the following command:

`crontab -l`

## Removing the crontab

If you no longer wish to run the crontab, type the following command to remove it:

`crontab -r`

## Changing the default editor

It is possible to specify a different editor when you edit the crontab by typing the following command:

`export VISUAL=/usr/bin/nano`

## Crontab and virtualenv

[Reference Link](https://stackoverflow.com/questions/3287038/cron-and-virtualenv?rq=2)

You should be able to do this by using the python in your virtual environment:

`/home/my/virtual/bin/python /home/my/project/manage.py command arguments`

Another thing to try is to make the same change in your manage.py script at the very top:

`#!/home/my/virtual/bin/python`

## Crontab execute a bash .sh

[Reference link](https://stackoverflow.com/questions/66337313/schedule-python-script-with-crontab)

Check if the .sh has the permission to be executed

´chmod +x filename`