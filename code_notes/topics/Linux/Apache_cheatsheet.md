# Apache cheatsheet

[**Back to index**](../../README.md)

| Description   | Command                           |
| ------------- | --------------------------------- |
| Apache Status | `sudo systemctl status apache2`   |
| Enable site   | `sudo a2ensite domain-name.conf`  |
| Disable site  | `sudo a2dissite domain-name.conf` |
| Check Status  | `sudo systemctl status apache2`   |
| Check errors  | `sudo apache2ctl configtest`      |
| Stop Apache   | `sudo systemctl stop apache2`     |
| Start Apache  | `sudo systemctl start apache2`    |
| Restart       | `sudo systemctl restart apache2`  |
| Reload        | `sudo systemctl reload apache2`   |
| Disable       | sudo systemctl disable apache2    |
| Enable        | `sudo systemctl enable apache2`   |
