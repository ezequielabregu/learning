# SSL

[**Back to index**](../../README.md)

[Reference link](https://medium.com/@ahmetfurkandemir/deploy-the-python-flask-website-f43fcc5f2c80)

Thanks to Let’s Encrypt, we will receive a free 90-day SSL certificate for our website, so that the browsers we access our website will understand that our website is secure and will not give warnings.
First of all, we must install Let’s Encrypt on our Ubuntu operating system with the following commands.

```bash
sudo apt-get install snapd
sudo snap install core; sudo snap refresh core
sudo snap install --classic certbot
sudo apt-get install certbot
sudo apt-get install python3-certbot
sudo apt-get install python3-certbot-apache
```

After the installations, run the commands below so that we can stop apace2 and obtain an SSL certificate for our own domain name. Edit the command according to your own domain name.

```bash
sudo service apache2 stop
sudo certbot --apache -d my-domain-name.com -d www.my-domain-name.com
```

Now SSL certificates have been created for us under /etc/letsencrypt/live/kendimesordum.com/ and we can use them.
After making the following configuration on our website, we will now be able to access our site over https.

```python
app.run(host='0.0.0.0', port=5000, ssl_context=('/etc/letsencrypt/live/my-domain-name.com/fullchain.pem', '/etc/letsencrypt/live/my-domain-name.com/privkey.pem'))
```