# Folder permissions in Ubuntu server

[**Back to index**](../../README.md)

To set the appropriate permissions for the directory and files, you can follow these steps:

**Change the owner of the entire project directory:** This will ensure that the user running the Flask application is the owner of all files and directories in the project. You can do this with the chown command. Replace $USER with the username of the user running the Flask application and /path/to/your/project with the path to your project directory:

```bash
sudo chown -R $USER:$USER /path/to/your/project
```

for example:

```bash
sudo chown -R www-data:www-data /var/www/subdomain.example.com/flaskapp/
```

**Set the permissions for the entire project directory:** This will give the user running the Flask application read, write, and execute permissions for all files and directories in the project, and give all other users read and execute permissions. You can do this with the chmod command:

```bash
sudo chmod -R 755 /path/to/your/project
```

**Set special permissions for the 'uploads' directory:** This will give the user running the Flask application read, write, and execute permissions for the 'uploads' directory, and give all other users only read and execute permissions. This is necessary because the user needs to be able to write files to the 'uploads' directory:

```bash
sudo chmod 755 /path/to/your/project/uploads
```

If you want to set the permissions for the audio directory to 755 without changing the permissions for the static directory, you can do so with the following command:

```bash
sudo chmod 755 static/audio
```

**Set special permissions for the 'app.py' file:** This will give the user running the Flask application read, write, and execute permissions for the 'app.py' file, and give all other users only read permissions. This is necessary because the user needs to be able to execute the 'app.py' file:

```bash
sudo chmod 744 /path/to/your/project/app.py
```

After running these commands, the user running the Flask application should have the necessary permissions to read, write, and execute all files and directories in the project, and all other users should only have read and execute permissions. This should allow your Flask application to function correctly while also maintaining a reasonable level of security.