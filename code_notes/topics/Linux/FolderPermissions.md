# Linux Folder Permission

[**Back to index**](../../README.md)

- References

<https://www.pluralsight.com/blog/it-ops/linux-file-permissions>

<https://www.pluralsight.com/blog/it-ops/linux-file-permissions>

Changing Linux permissions in numeric code
You may need to know how to change permissions in numeric code in Linux, so to do this you use numbers instead of “r”, “w”, or “x”.

0 = No Permission

1 = Execute

2 = Write

4 = Read

- Permission numbers are:

0 = ---

1 = --x

2 = -w-

3 = -wx

4 = r-

5 = r-x

6 = rw-

7 = rwx

For example:

chmod 777 foldername will give read, write, and execute permissions for everyone.

chmod 700 foldername will give read, write, and execute permissions for the user only.

chmod 327 foldername will give write and execute (3) permission for the user, w (2) for the group, and read, write, and execute for the users.

---

Furthermore Apache looks like its running on the user account Apache so while doing a 777 a CHMOD on the folder you want writtable you might find that Apache got get into a parent folder due to the Exercute premission.

Change the ownership of /www/ to apache if its not already and furthermore when you do 777 ensure your using the -R attribute to make it recusive otherwise as I said a parent will be blocking it.

```bash
chmod -R 777 /var/www/
```

or

```bash
chmod -R 777 .
```

The above should do the trick.

If that doesn't work and this doesn't look like it would work but you could always try this in the upload file.

$upload_path = '$upload_path = '/denkproducties/uploads/';
