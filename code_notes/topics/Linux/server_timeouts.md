# Server timeout

[**Back to index**](../../README.md)

## Apache timeout

[Reference link 1](https://httpd.apache.org/docs/2.0/mod/core.html#timeout)

[Reference link 2](https://ioflood.com/blog/2020/02/21/what-is-apache-keepalive-timeout-how-to-optimize-this-critical-setting/)

TimeOut Directive
Description: Amount of time the server will wait for certain events before failing a request

Syntax: TimeOut seconds

Default: TimeOut 300

Context: server config, virtual host

Status: Core

Module: core

The TimeOut directive currently defines the amount of time Apache will wait for three things:

1. The total amount of time it takes to receive a GET request.
2. The amount of time between receipt of TCP packets on a POST or PUT request.
3. The amount of time between ACKs on transmissions of TCP packets in responses.

---

### The Problem

There are different timeout problems here because we connect different pieces together and this parts need to communicate. But what if one of the pieces does not respond in a given time or, even more bad, if one process is running forever like a bad SQL-query.

### Understand your Timeouts

Timeouts are a way to limit the time that a request can run, and otherwise an attacker could simply run a denial-of-service with a simple request. But there are many configurations in several layers: Web server, PHP, application, database, curl.

### Web server

Mostly you will use Apache or Nginx as Web server and in the end it makes not really a difference, there are different timeout settings, but the idea is almost the same: The Web server will stop the execution and kills the PHP process, now you got a 504 HTTP error (Gateway Timeout) and you will lose your stack trace and error-tracking because we killed our application in the middle of nothing. So, we should keep the Web server running as long as needed.

`grep -Ri timeout /etc/apache2/`

Here you can see all configurations for Apache2 timeouts, but we only need to change etc/apache2/conf-enabled/timeout.conf`` because it will overwrite `/etc/apache2/apache2.conf` anyway.

PS: Remember to reload / restart your Web server after you change the configurations.

### Solution

We can combine different timeout, but the timeout from the called commands e.g. database, curl, etc. will be combined with the timeout from PHP (max_execution_time) itself. The timeout from the Web server (e.g. Apache2: Timeout) and from PHP-FPM (request_terminate_timeout) need to be longer than the combined timeout from the application so that we still can use our PHP error handler.

e.g.: ~ 5 min. timeout

1. MySQL read timeout: 240s ⇾ 4 min.

link->options(MYSQLI_OPT_READ_TIMEOUT, 240);

2. PHP timeout: 300s ⇾ 5 min.
max_execution_time = 300

3. Apache timeout: 360s ⇾ 6 min.
Timeout 360

4. PHP-FPM: 420s ⇾ 7 min.
request_terminate_timeout = 420

## Problem: Processing long audios a `ERR_EMPTY_RESPONSE` will appear due to the timeout setup on Apache and wsgi

### Overview

[Link Reference](https://stackoverflow.com/a/18130604/21590238)

While you could change your timeout on the server or other tricks to try to keep the page "alive", keep in mind that there might be other parts of the connection that you have no control over that could timeout the request (such as the timeout value of the browser, or any proxy between the browser and server, etc). Also, you might need to constantly up your timeout value if the task takes longer to complete (becomes more advanced, or just slower because more people use it).

In the end, this sort of problem is typically solved by a change in your architecture.

#### Use a Separate Process for Long-Running Tasks

Rather than submitting the request and running the task in the handling view, the view starts the running of the task in a separate process, then immediately returns a response. This response can bring the user to a "Please wait, we're processing" page. That page can use one of the many push technologies out there to determine when the task was completed (long-polling, web-sockets, server-sent events, an AJAX request every N seconds, or the dead-simplest: have the page reload every 5 seconds).

####  Have your web request "Kick Off" the separate process

Anyway, as I said, the view handling the request doesn't do the long action: it just kicks off a background process to do the task for it. You can create this background process dispatch yourself (check out this Flask snippet for possible ideas), or use a library like Celery or (RQ).

Once the task is complete, you need some way of notifying the user. This will be dependent on what sort of notification method you picked above. For a simple "ajax request every N seconds", you need to create a view that handles the AJAX request that checks if the task is complete. A typical way to do this is to have the long-running task, as a last step, make some update to a database. The requests for checking the status can then check this part of the database for updates.

#### Advantages and Disadvantages

Using this method (rather than trying to fit the long-running task into a request) has a few benefits:

1. Handling long-running web requests is a tricky business due to the fact that there are multiple points that could time out (besides the browser and server). With this method, all your web requests are very short and much less likely to timeout.

2. Flask (and other frameworks like it) is designed to only support a certain number of threads that can respond to web queries. Assume it has 8 threads: if four of them are handling the long requests, that only leaves four requests to actually handle more typical requests (like a user getting their profile page). Half of your web server could be tied up doing something that is not serving web content! At worse, you could have all eight threads running a long process, meaning your site is completely unable to respond to web requests until one of them finishes.

#### The main drawback

There is a little more set up work in getting a task queue up and running, and it does make your entire system slightly more complex. However, I would highly recommend this strategy for long-running tasks that run on the web.

### Solution 1

[Reference link](https://stackoverflow.com/questions/18127128/time-out-issues-with-chrome-and-flask)

Let's assume:

- This is not a server issue, so we don't have to go fiddle with Apache, nginx, etc. timeout settings.

- The delay is minutes, not hours or days, just to make the scenario manageable.

- You control the web page on which the user hits submit, and from which user interaction is managed.

If those obtain, I'd suggest not using a standard HTML form submission, but rather have the submit button kick off a JavaScript function to oversee processing. It would put up a "please be patient...this could take a little while" style message, then use jQuery.ajax, say, to call the long-time-taking server with a long timeout value. jQuery timeouts are measured in milliseconds, so 60000 = 60 seconds. If it's longer than that, increase your specified timeout accordingly. I have seen reports that not all clients will allow super-extra-long timeouts (e.g. Safari on iOS apparently has a 60-second limitation). But in general, this will give you a platform from which to manage the interactions (with your user, with the slow server) rather than being at the mercy of simple web form submission.

There are a few edge cases here to consider. The web server timeouts may indeed need to be adjusted upward (Apache defaults to 300 seconds aka 5 minutes, and nginx less, IIRC). Your client timeouts (on iOS, say) may have maximums too low for the delays you're seeing. Etc. Those cases would require either adjusting at the server, or adopting a different interaction strategy. But an AJAX-managed interaction is where I would start.

Example:

- app.py

```python
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/long_request')
def long_request():
    # This route simulates a long request that takes 30 seconds to complete
    import time
    time.sleep(30)
    return 'Long request complete!'

if __name__ == '__main__':
    app.run(debug=True)
```

- index.html

```html
<!DOCTYPE html>
<html>
<head>
 <title>Long Request Example</title>
 <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
 <h1>Long Request Example</h1>
 <button id="long-request-btn">Make Long Request</button>
 <div id="result"></div>

 <script>
  $(document).ready(function() {
   $('#long-request-btn').click(function() {
    $.ajax({
     url: '/long_request',
     timeout: 60000, // Set timeout to 60 seconds
     success: function(data) {
      $('#result').html(data);
     },
     error: function(xhr, status, error) {
      $('#result').html('Error: ' + error);
     }
    });
   });
  });
 </script>
</body>
</html>
```

In this example, we have a Flask app with two routes: '/' and '/long_request'. The '/' route renders the 'index.html' template, which contains a button and a div to display the result of the long request. The '/long_request' route simulates a long request that takes 30 seconds to complete.

When the user clicks the 'Make Long Request' button, jQuery.ajax is used to send a request to the '/long_request' route. We set the timeout value to 60 seconds using the 'timeout' option. If the server takes longer than 60 seconds to respond, the 'error' callback will be called with a 'timeout' status. Otherwise, the 'success' callback will be called with the response data, which we then display in the 'result' div.

### Solution 2

[Reference link](https://stackoverflow.com/questions/18127128/time-out-issues-with-chrome-and-flask)

Use daemon mode and adding the request-timeout=60 option.

Example:

```bash
WSGIRestrictEmbedded On

<IfModule mod_ssl.c>
  <VirtualHost _default_:443>
    WSGIDaemonProcess telephone-flask threads=5 request-timeout=60
    WSGIScriptAlias /telephone-flask /var/www/telephone-flask/telephone-flask.wsgi process-group=telephone-flask application-group=%{GLOBAL}
    <Directory /var/www/telephone-flask>
      Require all granted
    </Directory>
    LogLevel info
  </VirtualHost>
</IfModule>
```

Most likely your application is locking up and Apache is eventually just killing its child worker processes.

The request-timeout will force a restart of daemon process when requests get stuck for too long as well (average of 60 seconds across all threads). When this occurs, it will dump out Python stack traces of where your code is getting stuck.

But then it also may now run fine as well, since the incorrect path to Directory meant WSGIApplicationGroup was ignored. It you were using third party C extensions for Python, there was a chance they were deadlocking if they weren't implemented to work in sub interpreters properly.
