# Deploy Flask app on a subdomain (Apache)

[**Back to index**](../../README.md)

[Some reference](https://stackoverflow.com/questions/14181364/running-a-flask-website-in-a-subdirectory-with-apache)

## Configure the DNS for the subdomain

You need to create a DNS record for the subdomain in your DNS zone file. You can do this by adding an A record or a CNAME record pointing to the IP address or hostname of the Apache server.

[Point domain to server IP]

Habría que generar un **registro A** así:

- Tipo: A
- Nombre: subdomain-name
- Apunta a: server-IP

En cuanto al **CNAME**, podrías crear uno que diga:

- Tipo: CNAME
- Nombre: www.subdomain-name
- Apunta a: domain.com

Example:

- Option – Using A record and CNAME

| Name               | TTL   | Type  | Address    |
| ------------------ | ----- | ----- | ---------- |
| subdomain-name     | 14400 | A     | server IP  |
| www.subdomain-name | 14400 | CNAME | domain.com |

Ejemplo:

| Name    | TTL   | Type  | Address            |
| ------- | ----- | ----- | ------------------ |
| a4p     | 14400 | A     | 194.31.55.62       |
| www.a4p | 14400 | CNAME | ezequielabregu.com |

**IMPORTANTE**: No es necesario crear 2 registros A, porque solo se hace para el dominio (o subdominio), lo que sería la parte www. Se crea sí o sí con CNAME, tal cual como te lo escribí

## Configure Apache for the subdomain

You need to create a virtual host configuration file for the subdomain in the Apache configuration directory (/etc/apache2/sites-available/ on Ubuntu/Debian.

The configuration file should contain the following:

```bash
<VirtualHost *:80>

   DocumentRoot /var/www/html/app-folder
   ServerName subdomain.domain.com

    #AppProcessName is the demond process
    #it must match with name on WSGIProcessGroup (see below)
   WSGIDaemonProcess AppProcessName threads=5
   WSGIScriptAlias / /var/www/html/apps-folder/app.wsgi

   <Directory /absolute/path/to/app-folder>
    WSGIProcessGroup AppProcessName
    WSGIApplicationGroup %{GLOBAL}
    Order deny,allow
    Allow from all
   </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

### Enable the virtual host

Enable the virtual host configuration by creating a symbolic link from the configuration file to the "sites-enabled" directory:

`sudo ln -s /etc/apache2/sites-available/subdomain.example.com.conf /etc/apache2/sites-enabled/subdomain.example.com.conf`

and install all the requirements

`pip install -r requirements.txt`

### Copy the flask app files to the new directory

```bash
cd flask_app_folder
git clone repository-address . 
```

### Set the appropriate permissions for the directory and files

```bash
sudo chown -R www-data:www-data /var/www/subdomain.example.com/flaskapp/
sudo chmod -R 755 /var/www/subdomain.example.com/flaskapp/
```

### Enable the new virtual host and restart Apache

```bash
sudo a2ensite subdomain.example.com.conf
sudo systemctl restart apache2
```

## Activate Virtual Environment

### `virtualenv` method

[Reference link 1](https://stackoverflow.com/questions/27462582/how-can-i-activate-a-pyvenv-vitrualenv-from-within-python-activate-this-py-was)

[Reference link 2](https://modwsgi.readthedocs.io/en/develop/user-guides/virtual-environments.html)

When needing to activate the Python virtual environment from within the WSGI script file as described, it is preferred that you be using the either virtualenv or virtualenvwrapper to create the Python virtual environment. This is because they both provide the activate_this.py script file which does all the work of setting up sys.path. When you use either pyvenv or python -m venv with Python 3, no such activation script is provided.

`pyvenv` and the venv module don't support this out of the box. The third party `virtualenv` package does support this using `activate_this.py`, but that feature was not included in the built-in venv module.

You could try to borrow a copy of `activate_this.py` from a virtualenv based environment; it seems to work, though I can't swear it will be perfect (venv/pyvenv uses some magic during startup; unclear if all of it is replicated via activate_this.py).

The virtualenv docs for it are out of date for Python 3 (they claim you use execfile, which doesn't exist).

#### wsgi setup (app.wsgi)

[reference link](https://youtu.be/w0QDAg85Oow)

```bash
import sys
sys.path.insert(0, '/var/www/html/app-folder')

activate_this = '/var/www/html/app-folder/venv/bin/activate_this.py'
with open(activate_this) as file:
    exec(file.read(), dict(__file__=activate_this))

from app import app as application
```

Another alternative could be (see this section above):
[Reference link](https://help.pythonanywhere.com/pages/UpgradingToTheNewVirtualenvSystem/)

```bash
activate_this = '/home/myusername/.virtualenvs/django17/bin/activate_this.py'
with open(activate_this) as f:
    code = compile(f.read(), activate_this, 'exec')
    exec(code, dict(__file__=activate_this))
```

---

### Copy of `Activate_this.py`

[Reference link 1](https://github.com/pypa/virtualenv/blob/main/src/virtualenv/activation/python/activate_this.py)

ATTENTION:
Replace `Python3.9` with your current python version.
(see the line `for lib in "../lib/python3.9/site-packages".split(os.pathsep):`)

```python
"""Activate virtualenv for current interpreter:

Use exec(open(this_file).read(), {'__file__': this_file}).

This can be used when you must use an existing Python interpreter, not the virtualenv bin/python.
"""
import os
import site
import sys

try:
    abs_file = os.path.abspath(__file__)
except NameError:
    raise AssertionError("You must use exec(open(this_file).read(), {'__file__': this_file}))")

bin_dir = os.path.dirname(abs_file)
base = bin_dir[: -len("bin") - 1]  # strip away the bin part from the __file__, plus the path separator

# prepend bin to PATH (this file is inside the bin directory)
os.environ["PATH"] = os.pathsep.join([bin_dir] + os.environ.get("PATH", "").split(os.pathsep))
os.environ["VIRTUAL_ENV"] = base  # virtual env is right above bin directory

# add the virtual environments libraries to the host python import mechanism
prev_length = len(sys.path)
for lib in "../lib/python3.9/site-packages".split(os.pathsep):
    path = os.path.realpath(os.path.join(bin_dir, lib))
    site.addsitedir(path.decode("utf-8") if "" else path)
sys.path[:] = sys.path[prev_length:] + sys.path[0:prev_length]

sys.real_prefix = sys.prefix
sys.prefix = base

```

## pyenv / python -m venv method

[link reference](https://modwsgi.readthedocs.io/en/develop/user-guides/virtual-environments.html)

If you cannot for some reason and are stuck with pyvenv or python -m venv, you can instead use the following code in the WSGI script file:

```python
python_home = '/usr/local/envs/myapp1'

import sys
import site

# Calculate path to site-packages directory.

python_version = '.'.join(map(str, sys.version_info[:2]))
site_packages = python_home + '/lib/python%s/site-packages' % python_version

# Add the site-packages directory.

site.addsitedir(site_packages)

```

As before this code should be placed in the WSGI script file before any other module imports in the WSGI script file, with the exception of from `__future__` imports used to enable Python feature flags.

When using this method, do be aware that the additions to the Python module search path are made at the end of sys.path. For that reason, you must set the python-home option to WSGIDaemonProcess to the location of an empty Python virtual environment. If you do not do this, any additional Python package installed in the main Python installation will hide those in the Python virtual environment for the application.

There is extra code you could add which would reorder sys.path to make it work in an equivalent way to the activate_this.py script provided when you use virtualenv or virtualenvwrapper but it is messy and more trouble than it is worth:

```python
python_home = '/usr/local/envs/myapp1'

import sys
import site

# Calculate path to site-packages directory.

python_version = '.'.join(map(str, sys.version_info[:2]))
site_packages = python_home + '/lib/python%s/site-packages' % python_version
site.addsitedir(site_packages)

# Remember original sys.path.

prev_sys_path = list(sys.path)

# Add the site-packages directory.

site.addsitedir(site_packages)

# Reorder sys.path so new directories at the front.

new_sys_path = []

for item in list(sys.path):
    if item not in prev_sys_path:
        new_sys_path.append(item)
        sys.path.remove(item)

sys.path[:0] = new_sys_path
```

It is better to avoid needing to manually activate the Python virtual environment from inside of a WSGI script by using a separate daemon process group per WSGI application. At the minimum, at least avoid pyvenv and python -m venv.

### Install SSL certificate

[see this](./Install%20SSL%20on%20VPS%20using%20Cerbot.md)
