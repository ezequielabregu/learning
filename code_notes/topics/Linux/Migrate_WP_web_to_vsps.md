# Step-by-step commands to install and set up an Ubuntu VPS server to migrate a website manually

[**Back to index**](../../README.md)

Main References

[Migrate a WordPress site from localhost to Ubuntu VPS](https://www.interserver.net/tips/kb/migrate-wordpress-site-localhost-ubuntu-vps/)

[How to Migrate Your Website From Shared Hosting to VPS](https://www.hostinger.com/tutorials/migrate-website-from-shared-hosting-to-vps)

[How To Install Linux, Apache, MySQL, PHP (LAMP) Stack on Ubuntu 22.04](ttps://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-22-04)

## Guide stetp by step

### Connect to your VPS server using SSH

sudo ssh root@VPS_public_adrress

### Update the package list and install Apache, PHP, and MySQL by running the following command

```bash
sudo apt update
sudo apt install apache2 php mysql-server php-mysql
```

### Enable the Apache server and MySQL service to start automatically on boot

```bash
sudo systemctl enable apache2
sudo systemctl enable mysql
```

### (Optional) How to Install LAMP on Ubuntu

- Apache

`sudo apt install apache2`

Once the installation is finished, you’ll need to adjust your firewall settings to allow HTTP traffic. Ubuntu’s default firewall configuration tool is called Uncomplicated Firewall (UFW). It has different application profiles that you can leverage. To list all currently available UFW application profiles, execute this command:

`sudo ufw app list`

To secure the MySQL installation, run the following command:

```bash
sudo mysql_secure_installation
```

This will prompt you to set a root password and remove anonymous users, test databases, and remote root login.
Here’s what each of these profiles mean:

**Apache:** This profile opens only port 80 (normal, unencrypted web traffic).

**Apache Full:** This profile opens both port 80 (normal, unencrypted web traffic) and port 443 (TLS/SSL encrypted traffic).

**Apache Secure:** This profile opens only port 443 (TLS/SSL encrypted traffic).

For now, it’s best to allow only connections on port 80, since this is a fresh Apache installation and you don’t yet have a TLS/SSL certificate configured to allow for HTTPS traffic on your server.

To only allow traffic on port 80, use the Apache profile:

`sudo ufw allow in "Apache"`

Verify the changes:

`sudo ufw status`

Traffic on port 80 is now allowed through the firewall.

You can do a spot check right away to verify that everything went as planned by visiting your server’s public IP address in your web browser (view the note under the next heading to find out what your public IP address is if you do not have this information already):

- **mysql**

`sudo apt install mysql-server`

see more dedatails and bugs [here](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-22-04)

- **PHP**

`sudo apt install php libapache2-mod-php php-mysql`

Once the installation is finished, run the following command to confirm your PHP version:

`php -v`

### Copy your website files to the appropriate directory on the VPS server

The default directory for Apache on Ubuntu is `/var/www/html/`

Upload files via SCP. Scp command follows this pattern

```bash
scp [Options] [Source] [Destination]
```

example:

`sudo scp /Users/ezequielabregu/Downloads/backup_2.04.2023/u205951928.20230402165706.tar.gz ssh root@194.31.55.62:/var/www/html/ezequielabregu`

and then extract the compressed file

`tar -xvzf /path/to/filename.tar.gz`

- x : Tells tar to extract files from an archive.
- v : Tells tar to verbosely list all the files being extracted.
- z : Tells tar to decompress the archive using gzip.
- f : Tells tar the name of the archive to operate upon. This must be specified as last option, and the tar file must be specified immediately after it.

### Create a new MySQL database and user for your website

```bash
sudo mysql -u root -p
```

Enter your MySQL root password when prompted, then create a new database and user with the following commands:

```bash
CREATE DATABASE mywebsite;
CREATE USER 'myuser'@'localhost' IDENTIFIED BY 'mypassword';
GRANT ALL PRIVILEGES ON mywebsite.* TO 'myuser'@'localhost';
FLUSH PRIVILEGES;
exit;
```

Make sure to replace mywebsite, myuser, and mypassword with your desired values.

- import data base
  
```bash
mysql -u myuser -p mywebsite <  /path/to/exported_DB_NAME.sql
```

### Update the ownership and permissions of the website directory and files

```bash
sudo chown -R www-data:www-data /var/www/html/
sudo chmod -R 755 /var/www/html/
```

### Update the database credentials on wp-config.php

For example, if you're using WordPress, edit the `wp-config.php` file and update the following lines with your MySQL database, user, and password:

```bash
define('DB_NAME', 'mywebsite');
define('DB_USER', 'myuser');
define('DB_PASSWORD', 'mypassword');
define('DB_HOST', 'localhost');
```

Enter new values in DB_NAME, DB_USERNAME and DB_PASSWORD we set when we created a database on our VPS.

### Create virtual Host and Activate it (Apache)

If you are using Apache web server, it is going to be easy for you as Apache provides in-build command to activate or deactivate the virtual hosts. Execute the following command to create a new virtual host in your Apache server.

`sudo nano /etc/apache2/sites-available/mywebsite.conf`

And paste the following content in the file.

```bash
<VirtualHost *:80>
    ServerAdmin support@example.com
    ServerName mywebsite.com
    ServerAlias www.mywebsite.com

    DocumentRoot /var/www/mywebsite.com

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

```

Here is an example:

```bash
<VirtualHost *:80>
    ServerAdmin ezequielabregu.code@gmail.com
    ServerName ezequielabregu.com
    ServerAlias www.ezequielabregu.com

    DocumentRoot /var/www/html/ezequielabregu/public_html

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

```

### Restart the Apache server for the changes to take effect

Enable the new virtual host:

`sudo a2ensite mywebsite`

And restart Apache to take effect:

```bash
sudo systemctl restart apache2
```

### Create Virtual Host and Activate it (Nginx)

If you are using Nginx server, you have to manually create a symlink of the virtual host in order to activate it. The first step is to create a virtual host. Execute the following command to create a new virtual host in Nginx.

`sudo nano /etc/nginx/sites-available/example.conf`

Now, Paste the following content in the virtual host file. Do not forget to replace the example data with the actual data in the file, otherwise your site might not work.

```bash
server {
    listen 80;
    server_name example.com www.example.com;
    root /var/www/example.com;

    access_log /var/log/nginx/example-access.log;
    error_log /var/log/nginx/example-error.log;
}
```

Now, execute the following commands to create a symlink to activate the virtual host. We also have to restart the Nginx server to load the new configuration.

`sudo ln -s /etc/nginx/sites-available/example.conf /etc/nginx/sites-enabled/example.conf`

`sudo service nginx restart`

Once the Nginx server is restarted, your site will be live on the Internet.

## Point a Domain Name to VPS

[Reference link](https://www.hostinger.com/tutorials/dns/how-to-point-domain-to-vps)

### Method 1 – Pointing Domain to VPS via A record

- Option 1 – Using two A records

| Name            | TTL   | Type | Address       |
| --------------- | ----- | ---- | ------------- |
| example.com     | 14400 | A    | 153.92.211.25 |
| www.example.com | 14400 | A    | 153.92.211.25 |

- Option 2 – Using A record and CNAME

| Name            | TTL   | Type  | Address       |
| --------------- | ----- | ----- | ------------- |
| example.com     | 14400 | A     | 153.92.211.25 |
| www.example.com | 14400 | CNAME | example.com   |

Then, heck whether the records were added correctly:

```bash
dig A +short example.com
```

### Point the domain to the VPS server

<https://support.hostinger.com/en/articles/1583227-how-to-point-domain-to-your-vps>

### Install phpMyAdmin on Ubuntu

<https://www.hostinger.com/tutorials/how-to-install-and-setup-phpmyadmin-on-ubuntu>

### Change the site URL

<https://wordpress.org/documentation/article/changing-the-site-url/>
