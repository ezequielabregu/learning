# Monitor Apache logs

[**Back to index**](../../README.md)

## Error.log

The Apache error log is the first place to check for any error messages related to the server. The default location for the error log file is /var/log/apache2/error.log on Ubuntu. You can use the tail command to view the last few lines of the error log in real-time:

- Realtine monitor
  
`sudo tail -f /var/log/apache2/error.log`

- Monitoring the last 100 entries

`sudo tail -100 /var/log/apache2/error.log`

### Terminal server monitors

[Reference link](https://www.reddit.com/r/linux/comments/mlcv6j/i_made_a_terminal_utility_to_monitor_some_system/)

- Installed:

[Bashtop](https://github.com/aristocratos/bashtop)

Hey guys please see the below list of alternative apps as well:

- Glances - My go to, this one is light and quite amazing.

- Bpytop - very pretty, and I love its adjustable speed of monitoring.

- Gtop - This one was pretty heavy on my system, but it was quite pretty.

- Htop - This ones an OG at this point, I still use it, and it functions more as a task manager than a monitor, but I use it for both.

- Netdata - This hosts a website on your machine with an insane amount of live updated system information, not really command line, but I use it for the same reasons I use these sorts of programs.
