# Multilanguage website using JSON and Javascript

**Step 1:** Create JSON Files for Each Language
Create a separate JSON file for each language you want to support. Each file will contain key-value pairs where the key is a unique identifier for a piece of text, and the value is the translation of that text in the respective language.

For example, create en.json for English:

{\
  "welcome": "Welcome",\
  "goodbye": "Goodbye"\
}

And es.json for Spanish:

{\
  "welcome": "Bienvenido",\
  "goodbye": "Adiós"\
}

**Step 2:** Create a Function to Load Translations
Create a JavaScript function that fetches the appropriate JSON file based on the selected language, and then applies the translations to the text elements on the page.

```js
function loadTranslations(language) {
  fetch(`path/to/${language}.json`)
    .then(response => response.json())
    .then(translations => {
      document.querySelectorAll('[data-i18n]').forEach(element => {
        element.textContent = translations[element.getAttribute('data-i18n')];
      });
    });
}
```
This function uses the Fetch API to load the JSON file for the selected language. It then selects all elements with a data-i18n attribute (which you'll add in the next step), and sets their text content to the corresponding translation from the JSON file.

**Step 3:** Add Data Attributes to Text Elements
Add a data-i18n attribute to each text element on your page that you want to translate. The value of the attribute should be the key of the corresponding translation in the JSON files.

```html
<h1 data-i18n="welcome">Welcome</h1>
<p data-i18n="goodbye">Goodbye</p>
```

**Step 4:** Load Translations on Page Load
Call the loadTranslations function when the page loads, passing in the selected language. You can store the selected language in local storage to remember the user's choice across sessions.

```html
const selectedLanguage = localStorage.getItem('selectedLanguage') || 'en';
loadTranslations(selectedLanguage);
```

**Step 5:** Add a Language Selector
Add a language selector to your page that allows the user to change the language. When the user selects a language, update the selected language in local storage and reload the translations.

```js
<select id="languageSelector">
  <option value="en">English</option>
  <option value="es">Español</option>
</select>

<script>
document.getElementById('languageSelector').addEventListener('change', function() {
  localStorage.setItem('selectedLanguage', this.value);
  loadTranslations(this.value);
});
</script>
```