# Create a New Branch

[**Back to index**](../../README.md)

Initially, we are on the `main` branch.

## 1. Create the new branch

```bash
# create the new branch
git branch new-branch
# switch to the new branch
git checkout new-branch
```

Alternatively, you can do both steps at once with the -b option:

```bash
git checkout -b new-branch
```

## 2. Push the new branch to the remote repository

```bash
git push -u origin new-branch
```

Another way to do the same:

```bash
git push --set-upstream origin new-style
```

