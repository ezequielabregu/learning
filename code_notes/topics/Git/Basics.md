# Git Basics

[**Back to index**](../../README.md)

## Getting Started

`git init`

or

`git clone url`

### Configuration

```bash
git config --global color.ui true
git config --global push.default current
git config --global core.editor vim
git config --global user.name "John Doe"
git config --global user.email foo@citrix.com
git config --global diff.tool meld
```

### Create a local repository

```bash
mkdir folder_name
cd folder_name_path
git init
git status
git add [filename]
git commit -m 'description'
```

- Another ways to commit

```bash
git commit
```

and it will open the default editor to add the legend ''

- Check the current configuration

```bash
git config -l
```

- Sumarize the commits message history

```bash
git log
```

- Get the difference between the old code and the actual code

```bash
git diff namefile
```
