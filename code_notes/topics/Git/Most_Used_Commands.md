# Git most used commands

[**Back to index**](../../README.md)

### Getting & Creating Projects

| Command | Description |
| ------- | ----------- |
| `git init` | Initialize a local Git repository |
| `git clone ssh://git@github.com/[username]/[repository-name].git` | Create a local copy of a remote repository |

### Basic Snapshotting

| Command | Description |
| ------- | ----------- |
| `git status` | Check status |
| `git add [file-name.txt]` | Add a file to the staging area |
| `git add -A` | Add all new and changed files to the staging area |
| `git commit -m "[commit message]"` | Commit changes |
| `git rm -r [file-name.txt]` | Remove a file (or folder) |

### Branching & Merging

| Command | Description |
| ------- | ----------- |
| `git branch` | List branches (the asterisk denotes the current branch) |
| `git branch -a` | List all branches (local and remote) |
| `git branch [branch name]` | Create a new branch |
| `git branch -d [branch name]` | Delete a branch |
| `git push origin --delete [branch name]` | Delete a remote branch |
| `git checkout -b [branch name]` | Create a new branch and switch to it |
| `git checkout -b [branch name] origin/[branch name]` | Clone a remote branch and switch to it |
| `git branch -m [old branch name] [new branch name]` | Rename a local branch |
| `git checkout [branch name]` | Switch to a branch |
| `git checkout -` | Switch to the branch last checked out |
| `git checkout -- [file-name.txt]` | Discard changes to a file |
| `git merge [branch name]` | Merge a branch into the active branch |
| `git merge [source branch] [target branch]` | Merge a branch into a target branch |
| `git stash` | Stash changes in a dirty working directory |
| `git stash clear` | Remove all stashed entries |

### Sharing & Updating Projects

| Command | Description |
| ------- | ----------- |
| `git push origin [branch name]` | Push a branch to your remote repository |
| `git push -u origin [branch name]` | Push changes to remote repository (and remember the branch) |
| `git push` | Push changes to remote repository (remembered branch) |
| `git push origin --delete [branch name]` | Delete a remote branch |
| `git pull` | Update local repository to the newest commit |
| `git pull origin [branch name]` | Pull changes from remote repository |
| `git remote add origin ssh://git@github.com/[username]/[repository-name].git` | Add a remote repository |
| `git remote set-url origin ssh://git@github.com/[username]/[repository-name].git` | Set a repository's origin branch to SSH |

### Inspection & Comparison

| Command | Description |
| ------- | ----------- |
| `git log` | View changes |
| `git log --summary` | View changes (detailed) |
| `git log --oneline` | View changes (briefly) |
| `git diff [source branch] [target branch]` | Preview changes before merging |

Git Popular Commands
====

No more GUI! Git CLI FTW!
Much credit for this file goes to the [incredible resource provided by Tower](http://www.git-tower.com/blog/git-cheat-sheet-detail/).

Quick start
----

`git pull --rebase`

`git add .`

`git commit -m "commit message"`

`git push`


Let's get started
----

`git init` - Create a new local repository

`git help <command>` - Get help with a specific git command.

Checking what's up
----

`git status` - Lists every file that has changed since the last commit, and files currently staged for commit

`git diff` - Lists every individual change to every file that has changed since the last commit. Add the '--staged' tag to show changes currently staged for committing.

Committing
----

`git add .` - Add all current changes to the staging area, ready for the next commit.

`git add -p <filename>` - Add some changes in `<filename>` to the staging area, ready for the next commit.

`git commit -a` - Commit all local changes in tracked files to the staging area, ready for the next commit.

`git commit` - Commit previously staged changes.

`git commit <filename> -m 'My commit message'` - Commits changes to the `<filename>` file.

`git commit --amend` - Change the last commit. *Don‘t amend published commits!!*

Commit History
----

`git log --oneline` - Show all commits, starting with newest. The oneline flag cleans the entries up a bit to display each commit on one line.

`git log -p <file-name>` - Show changes over time for a specific file.


Branching Out
----

`git checkout <branch-name>` - Allows you to switch between git branches, adding the `-b` flag creates the branch you are switching to if it doesn't already exist. You can also specify specific commit hashes instead of branch names.

If you want to checkout a specific file you can `git checkout <branch-name-or-commit> <file-path>` which is powerful as it allows you to selectively merge in files or rewind changes.

`git branch` without any parameters will list all current branches. You can also use it to create a new branch with `git branch <new-branch-name>` which will create a branch based on your current HEAD but not check it out (which is why `git checkout -b <new-branch-name>` is so useful).

`git checkout --track <remote-name>/<branch-name>` - Create a new tracking branch based on a remote branch.

`git branch -d <branch-name>` - Delete a local branch.

`git push origin --delete <branch-name>` - Delete a remote branch.

Phoning Home
----

As evidenced by the popularity of remote repo hosting services (hello Github!), One of the most powerful features with git is the ability to track remote repositories.

`git remote -v` - List all currently configured remotes. The `-v` flag stands for *verbose* and allows you to see more information about the remote repos, like the url.

`git remote show <remote-name>` - Show more information about a specific remote.

`git remote add <remote> <url>` - Add new remote repository, named `<remote>` and with a source url of `<url>`.

`git fetch <remote>` - Download all changes from `<remote>`, but don‘t integrate into HEAD.

`git fetch --dry-run -v` -  `--dry-run` Show what would be done, without making any changes. `-v` Be verbose.

`git pull <remote> <branch-name>` - Download changes and directly merge/integrate.

`git push <remote> <branch-name>` - Publish local changes to the remote repo.

`git branch -dr <remote-name>/<branch-name>` - Delete a branch on the remote repo.

`git push --tags` - Publish any tags you've added.


Making Synergy!
----

Dealing with differences and changes between repos is the real power behind git.

`git merge <branch-name>` - Merge `<branch-name>` into your current HEAD.

`git rebase <branch-name>` - Rebase your current HEAD onto `<branch-name>`. *Don‘t rebase published commits!!*

`git rebase --abort` - Aborts a rebase in progress.

`git rebase --continue` - A rebase will pause when it detects a conflict and will wait for you to *resolve* it/them by choosing which side of the merge to keep. When you've resolved the conflicts, this command continues the rebase.

`git mergetool` - Use your configured merge tool to solve conflicts.

`git add <resolved-file>` - Use your editor to manually solve conflicts and (after resolving) mark file as resolved.

`git rm <resolved-file>`

Ctrl/Cmd-Z!
----

We all make mistakes.

`git reset --hard HEAD` - Discard all local changes in your working directory.

`get checkout -f` - Discard all local changes in your working directory. (same behavior as `git reset --hard HEAD`)

`git checkout HEAD <file-name>` - Discard local changes in a specific file.

`git revert <commit>` - Revert a commit (by producing a new commit with contrary changes).

`git reset --hard <commit>` - Reset your HEAD pointer to a previous commit and discard all changes since then.

`git reset <commit>` - Reset your HEAD pointer to a previous commit and preserve all changes as unstaged changes.

`git reset --keep <commit>` - Reset your HEAD pointer to a previous commit and preserve uncommitted local changes.

Tips and Tricks
----

``git checkout -- `git ls-files -m` `` - Reset modified files in a directory

``rm -rf `git ls-files --other --exclude-standard` `` - Remove untracked files (use caution)

`git log -G "<search-string>" --pretty=format:"%C(yellow)%h %Creset%s %Cgreen(%cr) %Cblue[%cn - %ce]" --decorate` - Search commits for a string or code

The following snippet is handy if you're dealing with a monolithic repo with many branches and submodules:  
``git checkout <branch-name> && rm -rf `git ls-files --other --exclude-standard` && git submodule update --init --recursive`` - Reset-checkout.. checks out branch, removes untracked files, and re-inits submodules

## Remote Ubuntu server is not sync with Github

If you want your local copy to exactly match the remote repository, including the deletion of files, you can use `git reset --hard` followed by `git clean`. Here's how:

1. Fetch the latest changes from the remote repository:

    ```bash
    git fetch origin
    ```

2. Reset your local repository to match the remote repository:

    ```bash
    git reset --hard origin/<your-branch>
    ```

    Replace `<your-branch>` with the name of your branch.

3. Remove untracked files and directories:

    ```bash
    git clean -fd
    ```

The `git clean -fd` command removes untracked files (`-f`) and directories (`-d`). Be careful with this command, as it will permanently delete these files and directories.

## Untrack files from the local repository and update the remote repository

Navigate to the root directory of your Git repository

`cd /path/to/your/repository`

Remove the files in the "cache" directory from the Git repository

`git rm -r --cached cache/`

Commit the changes

`git commit -m "Remove cached files"`

Push the changes to the remote repository

`git push`