
# `.gitignore`

[**Back to index**](../../README.md)

Create a `.gitignore` file in your root directory

## Ignoring single files

**Must specify filename and extension**

```bash
example.txt
```

## Keeping single files

```bash
!example.txt
```

## Multiple files with the same extension

```bash
*.txt
```

## Multiple files with the same name

```bash
example*
```

## Folders

```bash
examples/
```

## Files inside of folders

**You can apply the same techniques for multiple files inside the root directory**

```bash
examples/example.txt
```

## Everything inside of a folder except for some files

**When first ignoring the whole folder, you must have a star at the end.**

**The star means you are ignoring the files in the folder, while not having a star means that you are ignoring the whole folder**

```bash
examples/*
!examples/example.txt
```

## Ignoring files in every directory

**This ignores all files named example.txt in every folder. You can use the same techniques for ignoring specific names or extensions with this syntax as well.**

```bash
**/example.txt
```

## Ignoring files only in the root directory

**Must include a slash in the beginning**

```bash
/example.txt
```

## Matching many characters

**This ignores files named `Example.txt` and `example.txt`. You can match against as many characters as you like at once.**

```bash
[Ee]xample.txt
```

## Banishing .DS_Store files

1. Remove existing .DS_Store files from your Git repository and prevent them from being saved in the future.

2. Project Root
Make sure you are in the root of your project folder.

3. Remove .DS_Store

Use the terminal command:

```bash
find . -name .DS_Store -print0 | xargs -0 git rm --ignore-unmatch' to find all previously committed .DS_Store files and remove them from your project.
```

4. Add to .gitignore

To prevent .DS_Store from entering your project code again, add 

**/.DS_Store 

to your .gitignore file. If it does not exist, create the .gitignore file in the root folder of your project.

Commit Changes
Commit your changes and push them to your remote repository.

---

### Another tutorial

If .DS_Store was never added to your git repository, simply add it to your .gitignore file.

#### If you don't have one, create a file called
```
.gitignore
```

In your the root directory of your app and simply write
```
**/.DS_Store
```

In it. This will never allow the .DS_Store file to sneak in your git.


#### if it's already there, write in your terminal:
```
find . -name .DS_Store -print0 | xargs -0 git rm -f --ignore-unmatch
```
then commit and push the changes to remove the .DS_Store from your remote repo:
```
git commit -m "Remove .DS_Store from everywhere"

git push origin master
```
And now add .DS_Store to your .gitignore file, and then again commit and push with the 2 last pieces of code (git commit..., git push...)


#### Other Solution



If .DS_Store already committed:
```
find . -name .DS_Store -print0 | xargs -0 git rm --ignore-unmatch
```

To ignore them in all repository: (sometimes it named ._.DS_Store)

```
echo ".DS_Store" >> ~/.gitignore_global
echo "._.DS_Store" >> ~/.gitignore_global
echo "**/.DS_Store" >> ~/.gitignore_global
echo "**/._.DS_Store" >> ~/.gitignore_global
git config --global core.excludesfile ~/.gitignore_global