# Git discart local changes

[**Back to index**](../../README.md)

[reference link](https://www.git-tower.com/learn/git/faq/git-force-pull)

If you are sure that you don't need them anymore, you can discard your local changes completely:

`git reset --hard`

If you also have untracked / new files, you will have to use the "git clean" command to get rid of these, too:

`git clean -fd`

and then:

`git pull`