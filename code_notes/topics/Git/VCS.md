# Version Control Service (VCS)

[**Back to index**](../../README.md)

### Create a new repository on the command line & Push a local repository to Github / Gitlab

```bash
echo "# speech-text" >> README.md
git init
git add README.md
git commit -m "first commit"
git branch -M main
git remote add origin [github repo adress]
git push -u origin main
```

### Push an existing repository from the command line

```
git remote add origin [github repo adress]
git branch -M main
git push -u origin main
```

### Create a repository on Github on a especific adress

```bash
git clone repository_adress
git clone repository_adress adress_path
```

Make changes into the files & commit them.\
PUSH the commited files to github

```bash
git push
```

### To avoid enter user + password on every push (only 15 minutes)

`git config --global credential.helper cache`

### How to Remove Remote Origin from a Git Repository

Check remote conections:

`git remote -v`

Remove git remote origin

`git remote rm origin`

- Another alternative is delete de remote origin through the Git configuration file.

`nano .git/config`

and then detele all the [remote  "origin"] section
