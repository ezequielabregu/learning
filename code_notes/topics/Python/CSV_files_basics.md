# Python DSP

[**Back to index**](../../README.md)

[Link Reference 1](https://docs.python.org/3/library/csv.html)

## Write the first row content into the csv

In this case, I use name and email:

```bash
with open(ROOT_DIR+'contactos.csv', mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['name', 'email'])
    file.close()
```
