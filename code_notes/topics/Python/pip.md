# Python Virtual Env

[**Back to index**](../../README.md)

## Remove a library and its dependencies

You can install and use the pip-autoremove utility to remove a package plus unused dependencies.

```bash
# install pip-autoremove
pip install pip-autoremove
# remove "somepackage" plus its dependencies:
pip-autoremove somepackage -y
```
