# Schedule library

[**Back to index**](../../README.md)

[Reference link](https://theautomatic.net/2020/11/18/how-to-schedule-a-python-script-on-a-mac/)

[Schudle Library documentation](https://schedule.readthedocs.io/en/stable/)

In addition to using crontab to run Python scripts, we can also use the schedule library for handling scheduling Python tasks. This package is especially useful for scheduling specific functions within Python applications. It also works across different operating systems. Let’s get started with the schedule package by installing it with pip:

`pip install schedule`

We’ll start by creating a simple function called test that simply prints out a message. Then, we can use schedule to schedule a task running this function every 10 seconds. To kick off this task, we can use the run_pending method, which will run inside of a while loop.

```python
import schedule
 
def test():
    print("Test this out!")
 
schedule.every(10).seconds.do(test)
 
while True:
    schedule.run_pending()
```

The recurrence of the task can be adjusted by changing “seconds” to “minutes” or “hours” like below.

## every 10 minutes

`schedule.every(10).minutes.do(test)`
 
## every 10 hours

`schedule.every(10).hours.do(test)`

Likewise, we can change the time unit length from 10 to any other number.

## every 1 minute

`schedule.every(1).minutes.do(test)`
 
## every 30 seconds

`schedule.every(30).seconds.do(test)`

Listing and clearing tasks
You can print out a list of the scheduled tasks by running schedule.jobs:

`schedule.jobs`

To clear out all tasks from the scheduler, we can run the following line of code:

`schedule.clear()`
