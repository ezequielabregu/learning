# Selenium web scraping

[**Back to index**](../../README.md)

## XPath in Selenium Python

[Reference link](https://aniquekhan.com/how-to-use-xpath-in-selenium-python/)

```python
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

#for Firefox users 
from webdriver_manager.firefox import GeckoDriverManager 
browser= webdriver.Firefox(executable_path=GeckoDriverManager().install())
#for Chrome users 
from webdriver_manager.chrome import ChromeDriverManager 
browser= webdriver.Chrome(executable_path=ChromeDriverManager().install())
#Opening Instagram
browser.get("https://www.instagram.com")
#For Username
browser.find_element("xpath","/html/body/div[1]/div/div/div/div[1]/div/div/div/div[1]/section/main/article/div[2]/div[1]/div[2]/form/div/div[1]/div/label/input").send_keys("PUT YOUR USERNAME HERE ")
#For Password
browser.find_element("xpath","/html/body/div[1]/div/div/div/div[1]/div/div/div/div[1]/section/main/article/div[2]/div[1]/div[2]/form/div/div[2]/div/label/input").send_keys("PUT YOUR Password HERE ")
#For Clicking Login Button
browser.find_element("xpath","/html/body/div[1]/div/div/div/div[1]/div/div/div/div[1]/section/main/article/div[2]/div[1]/div[2]/form/div/div[3]").click()
```

## Locating elements

[Reference link](https://selenium-python.readthedocs.io/locating-elements.html)

There are various strategies to locate elements in a page. You can use the most appropriate one for your case. Selenium provides the following method to locate elements in a page:

find_element

## To find multiple elements (these methods will return a list)

Example usage:

```python
from selenium.webdriver.common.by import By

driver.find_element(By.XPATH, '//button[text()="Some text"]')
driver.find_elements(By.XPATH, '//button')
The attributes available for the By class are used to locate elements on a page. These are the attributes available for By class:

ID = "id"
NAME = "name"
XPATH = "xpath"
LINK_TEXT = "link text"
PARTIAL_LINK_TEXT = "partial link text"
TAG_NAME = "tag name"
CLASS_NAME = "class name"
CSS_SELECTOR = "css selector"

```

The ‘By’ class is used to specify which attribute is used to locate elements on a page. These are the various ways the attributes are used to locate elements on a page:

```

find_element(By.ID, "id")
find_element(By.NAME, "name")
find_element(By.XPATH, "xpath")
find_element(By.LINK_TEXT, "link text")
find_element(By.PARTIAL_LINK_TEXT, "partial link text")
find_element(By.TAG_NAME, "tag name")
find_element(By.CLASS_NAME, "class name")
find_element(By.CSS_SELECTOR, "css selector")
If you want to locate several elements with the same attribute replace find_element with find_elements.
```

## Locating by Id

Use this when you know the id attribute of an element. With this strategy, the first element with a matching id attribute will be returned. If no element has a matching id attribute, a NoSuchElementException will be raised.

For instance, consider this page source:

```html
<html>
 <body>
  <form id="loginForm">
   <input name="username" type="text" />
   <input name="password" type="password" />
   <input name="continue" type="submit" value="Login" />
  </form>
 </body>
</html>
```

The form element can be located like this:

login_form = driver.find_element(By.ID, 'loginForm')

## Locating by Name

Use this when you know the name attribute of an element. With this strategy, the first element with a matching name attribute will be returned. If no element has a matching name attribute, a NoSuchElementException will be raised.

For instance, consider this page source:

```html
<html>
 <body>
  <form id="loginForm">
   <input name="username" type="text" />
   <input name="password" type="password" />
   <input name="continue" type="submit" value="Login" />
   <input name="continue" type="button" value="Clear" />
  </form>
</body>
</html>
```

The username & password elements can be located like this:

```python
username = driver.find_element(By.NAME, 'username')
password = driver.find_element(By.NAME, 'password')
```

This will give the “Login” button as it occurs before the “Clear” button:

continue = driver.find_element(By.NAME, 'continue')

## Locating by XPath

XPath is the language used for locating nodes in an XML document. As HTML can be an implementation of XML (XHTML), Selenium users can leverage this powerful language to target elements in their web applications. XPath supports the simple methods of locating by id or name attributes and extends them by opening up all sorts of new possibilities such as locating the third checkbox on the page.

One of the main reasons for using XPath is when you don’t have a suitable id or name attribute for the element you wish to locate. You can use XPath to either locate the element in absolute terms (not advised), or relative to an element that does have an id or name attribute. XPath locators can also be used to specify elements via attributes other than id and name.

Absolute XPaths contain the location of all elements from the root (html) and as a result are likely to fail with only the slightest adjustment to the application. By finding a nearby element with an id or name attribute (ideally a parent element) you can locate your target element based on the relationship. This is much less likely to change and can make your tests more robust.

For instance, consider this page source:

```html
<html>
 <body>
  <form id="loginForm">
   <input name="username" type="text" />
   <input name="password" type="password" />
   <input name="continue" type="submit" value="Login" />
   <input name="continue" type="button" value="Clear" />
  </form>
</body>
</html>
```

The form elements can be located like this:

```python
login_form = driver.find_element(By.XPATH, "/html/body/form[1]")
login_form = driver.find_element(By.XPATH, "//form[1]")
login_form = driver.find_element(By.XPATH, "//form[@id='loginForm']")
```

Absolute path (would break if the HTML was changed only slightly)
First form element in the HTML
The form element with attribute id set to loginForm
The username element can be located like this:

```python
username = driver.find_element(By.XPATH, "//form[input/@name='username']")
username = driver.find_element(By.XPATH, "//form[@id='loginForm']/input[1]")
username = driver.find_element(By.XPATH, "//input[@name='username']")
```

First form element with an input child element with name set to username
First input child element of the form element with attribute id set to loginForm
First input element with attribute name set to username
The “Clear” button element can be located like this:

```python
clear_button = driver.find_element(By.XPATH, "//input[@name='continue'][@type='button']")
clear_button = driver.find_element(By.XPATH, "//form[@id='loginForm']/input[4]")
```

Input with attribute name set to continue and attribute type set to button
Fourth input child element of the form element with attribute id set to loginForm
These examples cover some basics, but in order to learn more, the following references are recommended:

- W3Schools XPath Tutorial
- W3C XPath Recommendation
- XPath Tutorial - with interactive examples.

Here is a couple of very useful Add-ons that can assist in discovering the XPath of an element:

xPath Finder - Plugin to get the elements xPath.
XPath Helper - for Google Chrome
4.4. Locating Hyperlinks by Link Text
Use this when you know the link text used within an anchor tag. With this strategy, the first element with the link text matching the provided value will be returned. If no element has a matching link text attribute, a NoSuchElementException will be raised.

For instance, consider this page source:

```html
<html>
 <body>
  <p>Are you sure you want to do this?</p>
  <a href="continue.html">Continue</a>
  <a href="cancel.html">Cancel</a>
</body>
</html>
```

The continue.html link can be located like this:

continue_link = driver.find_element(By.LINK_TEXT, 'Continue')
continue_link = driver.find_element(By.PARTIAL_LINK_TEXT, 'Conti')
4.5. Locating Elements by Tag Name
Use this when you want to locate an element by tag name. With this strategy, the first element with the given tag name will be returned. If no element has a matching tag name, a NoSuchElementException will be raised.

For instance, consider this page source:

```html
<html>
 <body>
  <h1>Welcome</h1>
  <p>Site content goes here.</p>
</body>
</html>
```

The heading (h1) element can be located like this:

heading1 = driver.find_element(By.TAG_NAME, 'h1')
4.6. Locating Elements by Class Name
Use this when you want to locate an element by class name. With this strategy, the first element with the matching class name attribute will be returned. If no element has a matching class name attribute, a NoSuchElementException will be raised.

For instance, consider this page source:

```html
<html>
 <body>
  <p class="content">Site content goes here.</p>
</body>
</html>
```

The “p” element can be located like this:

content = driver.find_element(By.CLASS_NAME, 'content')
4.7. Locating Elements by CSS Selectors
Use this when you want to locate an element using CSS selector syntax. With this strategy, the first element matching the given CSS selector will be returned. If no element matches the provided CSS selector, a NoSuchElementException will be raised.

For instance, consider this page source:

```html
<html>
 <body>
  <p class="content">Site content goes here.</p>
</body>
</html>
```

The “p” element can be located like this:

content = driver.find_element(By.CSS_SELECTOR, 'p.content')
Sauce Labs has good documentation on CSS selectors.

## .send.keys method

[Reference link](https://stackoverflow.com/questions/69781872/handling-select-from-computer-button-in-selenium-python-upload-file)

```python
def Post(self,items):
   time.sleep(2)
   select_file = self.driver.find_element_by_css_selector("input[type='file']")
   select_file.send_keys("abolute-file-path")
   ```

## Update functions to upload files to Instagram

[Reference link](https://github.com/eddyizm/insta_delete/blob/master/insta_upload.py)

[Repo 2023](https://github.com/eddyizm/insta_delete)

```python
# -*- coding: UTF-8 -*-
''' writing a script to automate photo uploads '''
import os
import pyautogui
import logging
from glob import glob
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.file_detector import UselessFileDetector
from sys import exit
from random import shuffle

import insta_base as ib
import caption

log = logging.getLogger(__name__)
pyautogui.FAILSAFE = False


def get_image(folder : str):
    ''' get a list of image from a folder recursively and randomize before returning one for posting '''
    folders = glob(folder+'/**/*.jpg', recursive=True)
    shuffle(folders)
    fullpath = ''
    for filename in folders:
        if os.path.isfile(filename):
            fullpath = filename
            break # get first directory if it exists
    return fullpath.replace('/','\\')


def select_local_file(full_file_path):
    """pyautogui actions to select file and close pop up"""
    log.info(f'selecting file on local file system: {full_file_path}')
    pyautogui.write(full_file_path)
    ib.random_time()
    log.info('tabbing over')
    pyautogui.press('tab')
    pyautogui.press('tab')
    ib.random_time()
    pyautogui.press('enter')
    log.info('window explorer tabbed and hit entered to close')


def find_upload_button(browser):
    upload_button = browser.find_element(by=By.XPATH,
                        value="//button[text()='Select from computer']")
    log.info('found select from computer button')
    ib.click_element(browser, upload_button, 'upload button')
    ib.random_time()
    

def find_new_post(browser):
    log.info('locating new post option')
    new_post_option = browser.find_element(by=By.XPATH,
                        value="//*[local-name()='svg' and @aria-label='New post']")
    log.info('found new post option')
    ib.click_element(browser, new_post_option, 'new post option')
    ib.random_time()


def upload_image(browser : webdriver, filepath : str):
    try:
        log.info('finding upload image button')
        ib.random_time()
        browser.file_detector = UselessFileDetector()
        find_new_post(browser)
        find_upload_button(browser)
        select_local_file(filepath)
    except Exception as ex:
        browser.quit()
        log.error('error in upload_image():', exc_info=True)


def find_next_button(browser):
    '''find delete button and click!'''
    log.info(f'finding next button...')
    ib.random_time()
    next = browser.find_element(by=By.XPATH,
                                value="//div[@role='button' and text()='Next']")
    log.info('found next button')                            
    ib.click_element(browser, next, 'next')


def share_image(browser):
    log.info('locating share button')
    share_button = browser.find_element(by=By.XPATH, value="//div[@role='button' and text()='Share']")
    log.info('sharing post')
    ActionChains(browser).move_to_element(share_button).click().perform()
    ib.random_time()
    log.info('post successful!')


def add_captions(browser, caption):
    add_text = browser.find_elements(by=By.XPATH, value="//*[local-name()='div' and @aria-label='Write a caption...']")[0]
    log.info('writing caption')
    ActionChains(browser).move_to_element(add_text).click().send_keys(caption).perform()
    ib.random_time()


def process_image(browser_object : webdriver, tags : str):
    try:
        log.info('starting process_image')
        find_next_button(browser_object)
        ib.random_time()
        find_next_button(browser_object)
        add_captions(browser=browser_object, caption=tags)
        share_image(browser_object)
        ib.save_cookies(browser_object)
        return True
    except Exception as ex:
        ib.screenshot('process_image')
        log.error('error in process_image():',  exc_info=True)
        browser_object.quit()
        return False


def main():
    ib.start_end_log(__file__)
    driver = ib.login_with_cookies()
    try:
        file = get_image(ib.Settings.image_path)
        tag = caption.get_caption(file)
        upload_image(driver, file)
        if process_image(driver, tag):
            ib.close_shop(driver)
            os.remove(file)
            log.info(f'file posted successfully and deleted: {file}')
    except Exception as ex:
        log.info(f'error posting file.', exc_info=True)
        ib.start_end_log(__file__, True)
        exit(1)


if __name__ == '__main__':
    main()
    ib.start_end_log(__file__, True)
    exit(0)
```

## How to upload an image in instagram using selenium using python

```python
import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import pyautogui

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://www.instagram.com/")
time.sleep(5)
my_email=driver.find_element_by_xpath('//*[@id="loginForm"]/div/div[1]/div/label/input')
my_email.send_keys("------")

my_password=driver.find_element_by_xpath('//*[@id="loginForm"]/div/div[2]/div/label/input')
my_password.send_keys("-------")
time.sleep(10)

login=driver.find_element_by_xpath('//*[@id="loginForm"]/div/div[3]')
login.click()

time.sleep(5)
upload=driver.find_element_by_xpath('//div[@class="QBdPU "]')
upload.click()
time.sleep(2)
img_upload=driver.find_element_by_xpath('/html/body/div[8]/div[2]/div/div/div/div[2]/div[1]/div/div/div[2]').click()
time.sleep(2)
path = "D:\Pictures\ph.png" # your imagepath
pyautogui.write(path) 
time.sleep(2)
pyautogui.press('enter')
```

##  Deprecated find_element_by_* and find_elements_by_* are now removed

[Reference link 1](https://stackoverflow.com/questions/72754651/attributeerror-webdriver-object-has-no-attribute-find-element-by-xpath)

[Reference link 2](https://github.com/SeleniumHQ/selenium/blob/a4995e2c096239b42c373f26498a6c9bb4f2b3e7/py/CHANGES)

```bash
.find_element_by_class_name(
.find_element(By.CLASS_NAME, 

.find_element_by_css_selector(
.find_element(By.CSS_SELECTOR, 

.find_element_by_id(
.find_element(By.ID, 

.find_element_by_link_text(
.find_element(By.LINK_TEXT, 

.find_element_by_name(
.find_element(By.NAME, 

.find_element_by_partial_link_text(
.find_element(By.PARTIAL_LINK_TEXT, 

.find_element_by_tag_name(
.find_element(By.TAG_NAME, 

.find_element_by_xpath(
.find_element(By.XPATH, 

.find_elements_by_class_name(
.find_elements(By.CLASS_NAME, 

.find_elements_by_css_selector(
.find_elements(By.CSS_SELECTOR, 

.find_elements_by_id(
.find_elements(By.ID, 

.find_elements_by_link_text(
.find_elements(By.LINK_TEXT, 

.find_elements_by_name(
.find_elements(By.NAME, 

.find_elements_by_partial_link_text(
.find_elements(By.PARTIAL_LINK_TEXT, 

.find_elements_by_tag_name(
.find_elements(By.TAG_NAME, 

.find_elements_by_xpath(
.find_elements(By.XPATH,
```

`last = test.find_element_by_xpath('//*[@id="mG61Hd"]/div[2]/div/div[2]/div[1]/div/div/div[2]/div/div[1]/div/div[1]/input')`

`last = test.find_element(By.XPATH, '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[1]/div/div/div[2]/div/div[1]/div/div[1]/input')`
You will also need to import By as follows:
`from selenium.webdriver.common.by import By`
