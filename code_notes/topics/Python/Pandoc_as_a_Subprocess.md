# Pandoc as a subprocess

[**Back to index**](../../README.md)

[Reference link](https://stackoverflow.com/questions/12649908/calling-pandoc-from-python-using-subprocess-popen)

[Reference link 2](https://pandoc.org/demos.html)

- Basic code (mmd to docx)

```python
import subprocess

args_docx = ['pandoc', '-s', filename.md,'-o', filename.docx]
subprocess.check_call(args_docx)
```

- Complete code to convert md to pdf (LaTex) and docx

```python
def converter(filename):
    file_1 = 'output/markdown/' + filename +'.md'
    file_pdf = 'output/pdf/' + filename + '.pdf'
    file_docx = 'output/docx/' + filename + '.docx'
    
    args_pdf = ['pandoc', file_1, '--pdf-engine=/usr/local/texlive/2023basic/bin/universal-darwin/xelatex', '-o', file_pdf]
    subprocess.check_call(args_pdf)

    args_docx = ['pandoc', '-s', file_1,'-o', file_docx]
    subprocess.check_call(args_docx)

    return "Files converted sucessfully"
```

## Problem with pdf convertion

### Solution 1 (xelatex not found)

[Reference link](https://gist.github.com/peterhurford/75957ba9335e755013b87254ec85fab1)

Install basiclatex

`brew install --cask basictex`

use the absolute path on

`--pdf-engine=/usr/local/texlive/2023basic/bin/universal-darwin/xelatex`

### Solution 2

**Cause of the problem:** MacTeX installs a symbolic link /usr/texbin pointing to the TeX binaries. GUI applications use this link to find the binaries. But in El Capitan, the location /usr is reserved for Apple, and even users with root access are not allowed to write to that location.

**Solution:** The solution to this problem is to relocate the link to /Library/TeX/texbin, and reconfigure GUI applications to look in this location. Consequently, MacTeX-2015 and BasicTeX-2015 write two links, /usr/texbin and /Library/TeX/texbin, when installing on systems earlier than El Capitan, and write /Library/TeX/texbin when installing on El Capitan. These installations configure shell applications to look for TeX in both locations. You, therefore, need only to reconfigure GUI, as instructed here.

Reconfiguring TeXLive Utility: Version 1.23 of TEX Live Utility automatically detects the loss of /usr/texbin under El Capitan and prompts the user to automatically fix the problem. Previous versions need manual fixing:

- Open TeXLive Utility Preferences
- Click on Choose...
- Press Shift + cmd+g
- Enter /Library/TeX and click ok
- Double click on texbin
- Reconfiguring TeXShop: As pointed out in this document, - TeXShop versions from 3.52 onwards (July 2015) automatically redirect themselves to the correct link; if you cannot/want not update, you should:

  Open TeXShop Preferences
Go in the Engine tab
Change the Path settings for (pdf)TeX from /usr/texbin to /Library/TeX/texbin
And you should be good to go.

Reconfiguring LaTeXit:

Open LaTeXit Preferences
Go under Typesetting - Behaviour
Change all paths that start with /usr/texbin with /Library/TeX/texbin
Reconfiguring TeXWorks:

Go on the TeXWorks Preference, Typesetting Pane
click on the + button in the Paths for TeX
Click on the popup menu and select your hard drive
Double-Click on Library and then TeX
Click on texbin to select it and Click on the Choose button to add /Library/TeX/texbin. You can delete the /usr/texbin item by selecting it and clicking one the - button.
