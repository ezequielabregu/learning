# Chrome automation on linux server

[**Back to index**](../../README.md)

[Reference Link]

You can monkey-patch pyautogui internals. Tested on 'xvfb' backend.

```python 
import os
from pyvirtualdisplay import Display
import pyautogui
import Xlib.display

v_display = Display(visible=1, size=(1600,900))
v_display.start()  # this changes the DISPLAY environment variable
# sadly, pyautogui does not detect this change
pyautogui._pyautogui_x11._display = Xlib.display.Display(
                os.environ['DISPLAY']
            )
...
pyautogui.click(...)  # clicks on v_display
...

v_display.stop()
```

Note: this should be sufficient to enable pyautogui mouse, using keyboard may require additional configuration of key mapping. For more info, please see:

<https://github.com/asweigart/pyautogui/blob/master/pyautogui/_pyautogui_x11.py>

---

## PYAUTOGUI HEADLESS Docker mode without display in Python

[Reference link](https://abhishekvaid13.medium.com/pyautogui-headless-docker-mode-without-display-in-python-480480599fc4)

So lets begin to crack the code-

1. Firstly you do not set your scrapper to Headless mode, the scrapper will by default become headless inside Docker which we will make latter in this post. It will use a virtual display and will be headless by default.

2. Second you have to create a virtual Display as shown in the above image in side your scrapper.py file. The code for the same is down below.

```python
from pyvirtualdisplay.display import Display
disp = Display(visible=True, size=(1366, 768), backend="xvfb", use_xauth=True)
disp.start()
```

As shown above, you first import Pyvirtual display. Then you set visible to True, Size resolution is the size of the display you are using on your device, set backend to xvfb and set use_xauth to true.

3. Start you display by disp.start(), as shown.

Now you have to provide PYAUTOGUI the path to your display. To do so use the code-

```python
import Xlib.display
import pyautogui
pyautogui._pyautogui_x11._display = Xlib.display.Display(os.environ['DISPLAY'])
```

As shown you now after the display has started import pyautogui inside your scrapper.py script and provide pyautogui the path to Xdisplay.

After this you get your website and start using pyautogui as intended and make the whole scrapper or do whatever you wish to do with it. After this we make the docker image.

Now it is time for the Grand finale- to actually build a docker image to run PYAUTOGUI Headless

```bash
FROM ubuntu:bionic

RUN apt-get update && apt-get install python3 tesseract-ocr python3-pip curl unzip -yf
# Install Chrome
RUN apt-get update -y
RUN apt-get install -y dbus-x11
RUN curl https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -o /chrome.deb
RUN dpkg -i /chrome.deb || apt-get install -yf
RUN rm /chrome.deb
RUN apt-get -y install libssl1.0-dev
RUN curl https://chromedriver.storage.googleapis.com/87.0.4280.20/chromedriver_linux64.zip -o /usr/local/bin/chromedriver.zip
RUN cd /usr/local/bin && unzip chromedriver.zip
RUN chmod +x /usr/local/bin/chromedriver
USER root
RUN apt update
RUN apt-get install -y poppler-utils
RUN apt-get clean
RUN apt install -y python3 python3-pip
RUN DEBIAN_FRONTEND=noninteractive apt install -y python3-xlib python3-tk python3-dev
RUN apt install -y xvfb xserver-xephyr
python3-tk python3-dev
RUN Xvfb :99 -ac &
RUN export DISPLAY=:99
RUN pip3 install virtualenv
RUN mkdir /app
COPY requirements.txt /app
WORKDIR /app
RUN virtualenv .venv
RUN /bin/bash -c "source .venv/bin/activate"
RUN pip3 install -r requirements.txt
RUN apt-get install -y scrot
COPY . /app
ENTRYPOINT [ "python3", "scrapper.py" ]
```

So as shown in the above code you first get ubuntu operating system in your dockerfile because Xdisplay works on Ubuntu.

Then install python3 and chromedriver using the subsequent commands in the same script.

After this set user to root and install python3-xlib, python3-tk and python3-dev and do set DEBIAN_FRONTEND=noninteractive as they are needed to run pyautogui in ubuntu and docker does not allow intervention while installation.

After this install xvfb for virtual display backend capability.

Then RUN xvfb display on port 99 and assign port 99 in export display to docker.

After this RUN the virtual environment and provide path for activation using the subsequent commands in the script above.

Now a basic requirement.txt for installation should look something like the code below.

## "--enable-automation" and "--remote-debugging-port=0"

[Reference link](https://stackoverflow.com/questions/75553778/selenium-chrome-why-do-i-first-need-to-manually-run-chrome-before-i-can-succeed)

It turns out that if both "--enable-automation" and "--remote-debugging-port=0" are used at the same time when calling chrome, then it creates problems for the javascript application that is running at amtrak.com (specifically it seems to prevent the application from writing json to the sessionStorage area).

The workaround that I've implemented is to add the following lines to my selenium code:

   options.add_experimental_option("excludeSwitches",["enable-automation"])
   options.add_argument("--remote-debugging-port=9999")
The first line prevents selenium from adding "--enable-automation" to the commandline when it calls chrome (Note that this also turns off the "Chrome is being controlled by automated test software" banner). The second line sets the remote debugging port to anything other than 0. (If anyone knows how to completely eliminate this option from being passed onto chrome, I'm all ears.)

The combination of both actions sets up the selenium-initiated browser to correctly run the application that amtrak.com currently has in place... without any need to run in a static area that has been pre-conditioned with a manual browser run.

(This also eliminates the need to use the --user-data-dir and --profile-directory options).

##

13


If there is some dynamic content on the website you need to wait some time until you can retrieve the wished element. Try to following code examples:

Check Configuration

Did you install a backend for pyvirtualdisplay like xvfb and xephyr? If not,

try: `sudo apt-get install xvfb xserver-xephyr`

**First try**: Add a simple time.sleep()

```python
import time
from pyvirtualdisplay import Display
from selenium import webdriver

display = Display(visible=0, size=(1024, 768))
display.start()

browser = webdriver.Firefox()
actions = webdriver.ActionChains(browser)
browser.get('some_url_I_need')
time.sleep(5) # sleep for 5 seconds
content = browser.find_element_by_id('content') # Error on this line
```

**Second try**: Add browser.implicitly_wait(30) to your Selenium webdriver.

```python
from pyvirtualdisplay import Display
from selenium import webdriver

display = Display(visible=0, size=(1024, 768))
display.start()

browser = webdriver.Firefox()
browser.implicitly_wait(30) # seconds
actions = webdriver.ActionChains(browser)
browser.get('some_url_I_need')
content = browser.find_element_by_id('content') # Error on this line
```
---

```python
from pyvirtualdisplay import Display 
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.keys import Keys
import unittest, time, re, random

capabilities = DesiredCapabilities.FIREFOX.copy()

capabilities['marionette'] = False
 #display = Display(visible=0, size=(1024, 768))
 #display.start()

driver = webdriver.Firefox(capabilities=capabilities)
driver.implicitly_wait(20)

base_url = "http://xxx.yyy.zzz.aaa/sss/sss-Login/login/main_login.php"

RANDINT = random.random()*10000

verificationErrors = []

driver.get(base_url + "")

username = driver.find_element_by_id("myusername")

username.send_keys("xxxxxxxx")

driver.implicitly_wait(20)

password = driver.find_element_by_id("mypassword")

 #password.send_keys("xxxxzz" + Keys.ENTER)

password.send_keys("xxxxzzc" )

driver.implicitly_wait(20)

driver.find_element_by_xpath("//*[@id='submit']").click() 


 # Click on category link 


driver.find_element_by_xpath("//*[@id='stylefour']/ul/li[3]/a").click()
driver.find_element_by_xpath("//*[@id='stylefour']/ul/li[1]/a").click()
driver.find_element_by_xpath("//*[@id='stylefour']/ul[2]/li[4]/a").click
 # Click on sub-category link

driver.find_element_by_xpath("//*[@id='top']/body/div/div[2]/div[2]/div/div[2]/ul/li[4]/a/span").click()
 # Click on product image
driver.find_element_by_xpath("//*[@id='product-collection-image-374']").click()
 # Click Checkout button
driver.find_element_by_xpath("//*[@id='checkout-button']/span/span").click()
driver.find_element_by_id("billing:firstname").clear()
driver.find_element_by_id("billing:firstname").send_keys("selenium", RANDINT, "_fname")
driver.find_element_by_id("billing:lastname").clear()
driver.find_element_by_id("billing:lastname").send_keys("selenium", RANDINT, "_lname")
 # Click Place Order

driver.find_element_by_xpath("//*[@id='order_submit_button']").click()

driver.quit()

display.stop()
``` 
