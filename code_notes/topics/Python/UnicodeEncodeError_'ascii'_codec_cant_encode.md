# UnicodeEncodeError: 'ascii' codec can't encode character in position

[**Back to index**](../../README.md)

[Reference Link 1](https://bobbyhadz.com/blog/python-unicodeencodeerror-ascii-codec-cant-encode-character-in-position)

[Reference Link 2](https://nedbatchelder.com/text/unipain.html)

[Reference Link 3](https://stackoverflow.com/questions/9942594/unicodeencodeerror-ascii-codec-cant-encode-character-u-xa0-in-position-20)

## Problem

The Python "UnicodeEncodeError: 'ascii' codec can't encode character in position" occurs when we use the ascii codec to encode a string that contains non-ascii characters.

To solve the error, specify the correct encoding, e.g. utf-8.

```bash
my_str = 'one ф'

# ⛔️ UnicodeEncodeError: 'ascii' codec can't encode character '\u0444' in position 4: ordinal not in range(128)
my_bytes = my_str.encode('ascii')
```

## Solutions

The utf-8 encoding is capable of encoding over a million valid character code points in Unicode.

### Use the correct encoding to encode the string

```bash
my_str = 'one ф'

my_bytes = my_str.encode('utf-8')

print(my_bytes)  # 👉️ b'one \xd1\x84'
```

If you got the error when opening a file, set the encoding keyword argument to utf-8 in the call to the open() function.

### Write a textfile .txt with encoding utf-8

```bash
my_str = 'one ф'

# 👇️ set encoding to utf-8
with open('example.txt', 'w', encoding='utf-8') as f:
    f.write(my_str)
```

### Set the errors keyword argument to ignore

```bash
my_str = 'one ф'

# 👇️ encode str to bytes
my_bytes = my_str.encode('utf-8', errors='ignore')
print(my_bytes)  # 👉️ b'one \xd1\x84'

# 👇️ decode bytes to str
my_str_again = my_bytes.decode('utf-8', errors='ignore')
print(my_str_again)  # 👉️ "one ф"
```

### Try using the ascii encoding to encode the string

```bash
my_str = 'one ф'

# 👇️ encode str to bytes
my_bytes = my_str.encode('ascii', errors='ignore')
print(my_bytes)  # 👉️ b'one '

# 👇️ decode bytes to str
my_str_again = my_bytes.decode('ascii', errors='ignore')
print(my_str_again)  # 👉️ "one"
```

### Set the encoding keyword argument to utf-8 when opening a file

```bash
my_str = 'one ф'

# 👇️ set encoding to utf-8
with open('example.txt', 'w', encoding='utf-8') as f:
    f.write(my_str)

```

You can also set the errors keyword argument to ignore to ignore any encoding errors when opening a file.

```bash
my_str = 'one ф'

with open('example.txt', 'w', encoding='utf-8', errors='ignore') as f:
    f.write(my_str)
```

### Set the encoding keyword argument to utf-8 when sending emails

If you got the error when using the smtplib module, encode the string using the utf-8 encoding before sending it.

```python
my_str = 'one ф'

encoded_message = my_str.encode('utf-8')

server.sendmail(
    'from@gmail.com',
    'to@gmail.com',
    encoded_message
)
```

Notice that we passed the encoded message as an argument to server.sendmail().

If you don't encode the message yourself, Python will try to encode it using the ASCII codec when you call the sendmail() method.

Since the message contains non-ASCII characters, the error is raised.
