# Convert seconds into time format

[**Back to index**](../../README.md)

[Reference link](https://www.geeksforgeeks.org/python-program-to-convert-seconds-into-hours-minutes-and-seconds/)

## Approach #1

Naive This approach is simply a naive approach to get the hours, minutes and seconds by simple mathematical calculations.

```python
# Python Program to Convert seconds
# into hours, minutes and seconds

def convert(seconds):
    seconds = seconds % (24 * 3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    
    return "%d:%02d:%02d" % (hour, minutes, seconds)
 
# Driver program
n = 12345
print(convert(n))

```

OUTPUT:

`3:25:45`

## Approach #2

Alternate to the Naive approach By using the divmod() function, which does only a single division to produce both the quotient and the remainder, you can have the result very quickly with only two mathematical operations.

```python
# Python Program to Convert seconds
# into hours, minutes and seconds

def convert(seconds):
    min, sec = divmod(seconds, 60)
    hour, min = divmod(min, 60)
    return '%d:%02d:%02d' % (hour, min, sec)

# Driver program
n = 12345
print(convert(n))
```

## Approach #3

Using timedelta (Object of datetime module) Datetime module provides timedelta object which represents a duration, the difference between two dates or times. datetime.timedelta can be used to represent seconds into hours, minutes and seconds format.

```python
# Python Program to Convert seconds
# into hours, minutes and seconds
import datetime

def convert(n):
    return str(datetime.timedelta(seconds = n))

# Driver program
n = 12345
print(convert(n))
```

## Approach #4

Using time.strftime() time.strftime() gives more control over formatting. The format and time.gmtime() is passed as argument. gmtime is used to convert seconds to special tuple format that strftime() requires.

```python
# Python Program to Convert seconds
# into hours, minutes and seconds

import time

def convert(seconds):
 return time.strftime("%H:%M:%S", time.gmtime(n))
 
# Driver program
n = 12345
print(convert(n))
```
