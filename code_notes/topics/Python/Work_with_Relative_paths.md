# Works with Relative Paths

[**Back to index**](../../README.md)

[References Link 1](https://towardsthecloud.com/get-relative-path-python#:~:text=A%20relative%20path%20starts%20with,path%20to%20the%20file%20want.)

[Reference Link 2](https://towardsdatascience.com/simple-trick-to-work-with-relative-paths-in-python-c072cdc9acb9)

## Use cases

Two quick example on where this trick comes in handy:

- Loading an environment file from e.g. /config/.env
- We have an API that receives files; the files should always be stored in data/received_audio_files for example.

## Method 1: How to find the relative path in Python

You can run your Python script on different operating systems, therefore you want to automatically find the full path of the file you wish to import into your code instead of hardcoding it. This can be accomplished by combining the absolute path with the relative path in order to get the file or folder in your project.

We’ll use the app.py from the previous example and from this working directory we want to get the ./src/lib relative path.

To get the relative path in Python you write the following code:

```python
import os

absolute_path = os.path.dirname(__file__)
relative_path = "src/lib"
full_path = os.path.join(absolute_path, relative_path)
```

First, you have to import the os module in Python so you can run operating system functionalities in your code.

Then you create the variable absolute_path which fetches the current directory relative to the root folder. This is the full path to your working directory, in this case, ~/home/projects/example-project/.

The advantage to getting the absolute path on your operating system is that it makes it possible to run the script on different systems on different working directories.

The relative_path variable is a string in which you define the location of the folder that you want to fetch relative to the working directory. In this case, "src/lib".

Then we use the absolute_path and combine it via join with the relative_path to get the full path to the lib folder which results in:

`/Users/dannysteenman/projects/example-project/src/lib/`

## Method 2: The Trick

The trick is to define a variable that calculate the absolute path to our root in a file and then import this variable from anywhere in the project. It works like this:

In our project root create a folder called `config` that contains a file called `definitions.py`. We’ll put the following code inside:

```python
import os
ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
```

- Example

```python
import os
from config.definitions import ROOT_DIR
print(os.path.join(ROOT_DIR, 'data', 'mydata.json'))
```
