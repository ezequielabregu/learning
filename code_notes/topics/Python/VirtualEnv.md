# Python Virtual Env

[**Back to index**](../../README.md)

## Requirements.txt

<https://www.scaler.com/topics/how-to-create-requirements-txt-python/>

Traditionally, we create a requirements.txt file by creating a virtual environment. After activating a virtual environment, we have to run the command as follows:

`pip freeze > requirements.txt`

## Install project modules only

```bash
pip install pipreqs, 
pipreqs>requirements.txt
```

## Virtual envs

<https://python.land/virtual-environments/virtualenv>

extensive guide about venv\
<https://realpython.com/effective-python-environment/>

## Basics on venvs

<https://realpython.com/python-virtual-environments-a-primer/>

## Copying modules from different python versions

Assuming both interpreters are globally available, for example as python3.10 and python3.11

Get a list of all the modules installed on the 3.10 interpreter with pip freeze and then install them with 3.11's pip:

```bash
python3.10 -m pip freeze > requirements.txt
python3.11 -m pip install -r requirements.txt
```
