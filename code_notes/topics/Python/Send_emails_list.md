# Send Email list

[**Back to index**](../../README.md)

## Setting up gmail account

[Reference link](https://leimao.github.io/blog/Python-Send-Gmail/)

From May 30, 2022, no `“Allow less secure apps to ON”` is allowed for any Google account. Therefore, the previous Gmail authentication method in Python will not work anymore.

Google encouraged the user to use the Gmail API for managing the emails. However, the authentication setup process and verification process are complicated. Credential setups are saved in the application system environment, the application is deployed in a new environment, manual authentication process will be required. This prevented the application from being deployed on the cloud.

`App passwords` let you sign in to your Google Account from apps on devices that don’t support **2-Step Verification**. It is less secure. That’s probably why Google did not advocate it. However, it is less secure in a sense that someone could use your Gmail account to send emails on your behalf if they happen to get your App password, but they could not change your Google account password if they don’t know the it. The Google account owner could change or delete the App password anytime if they found the Gmail account has been compromised.

## Create & use App Passwords

If you use 2-Step-Verification and get a "password incorrect" error when you sign in, you can try to use an App Password.

`Search by using the search bar as "App Passwords"`

or

1. Go to your Google Account.
2. Select Security.
3. Under "Signing in to Google," select App Passwords. You may need to sign in. If you don’t have this option, it might be because:
   1. Step Verification is not set up for your account.
   2. Step Verification is only set up for security keys
   3. Your account is through work, school, or other organization.
   4. You turned on Advanced Protection.

4. At the bottom, choose Select app and choose the app you using and then Select device and choose the device you’re using and then Generate.

5. Follow the instructions to enter the App Password. The App Password is the 16-character code in the yellow bar on your device.

6. Tap Done.

`Tip: Most of the time, you’ll only have to enter an App Password once per app or device, so don’t worry about memorizing it.`

## Python code

[Link Reference](https://realpython.com/python-send-email/)

The following is a basic script.
Open a CSV file and loop over its lines of content (skipping the header row). To make sure that the code works correctly before you send emails to all your contacts, I’ve printed Sending email to ... for each contact, which we can later replace with functionality that actually sends out emails:

```python
import csv

with open("contacts_file.csv") as file:
    reader = csv.reader(file)
    next(reader)  # Skip header row
    for name, email, grade in reader:
        print(f"Sending email to {name}")
        # Send email here
```

A complete code to send a list emails using CSV list:

```python
#!/usr/bin/env python

import csv, smtplib, ssl

sender = "eabregu.dev@gmail.com"
password = 'wbxjwtzkbuqbcfke' #input("Type your password and press enter: ")

#with open("mensaje.txt", "r", encoding='utf-8') as f:
#    message = f.readlines()

message = """\
Subject : Argentine Booking Artist


Hello,

My name is {name}

Regards.
"""

print('')
confirmation = input('Confirm you want to sent the email? \n \nPress y = YES or n = NO and then ENTER: ')

if confirmation == 'y':

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(sender, password)
        with open("/Users/ezequielabregu/code/laboratory/email/contactos.csv") as file:
            reader = csv.reader(file, delimiter=';')
            next(reader)  # Skip header row
            print('')
            print('=== Sending emails ===')
            for name, email in reader:
                server.sendmail(
                    sender,
                    email,
                    message.format(name=name),
                    #message.as_string(),
                    #server.quit()
                )
                print("{:<25} {:<25}".format(name, email))
    print('')
    print('SUCESS!')
    print('ALL EMAILS SENT SUCESSFULLY <3')
    print('')            

else:
    print('')
    print('You have canceled send email :-s ')
```

## Tips & tricks

### [SSL: CERTIFICATE_VERIFY_FAILED]

On mac, go to:

`/Applications/Python 3.10/Install Certificates.command`

and run `Ìnstall Certificates.command`

Other solutions:

<https://support.posit.co/hc/en-us/articles/13157633438359-SSL-certificate-error-when-publishing-Python-applications-to-Connect>

<https://support.chainstack.com/hc/en-us/articles/9117198436249-Common-SSL-Issues-on-Python-and-How-to-Fix-it>

### The emails sent is recognized as a SPAM

[Reference link 1](https://support.google.com/mail/answer/81126)

[Reference link 2](https://stackoverflow.com/questions/72887322/how-to-sent-email-with-gmail-with-python-flask-but-not-in-spam-folder)

- Set up valid reverse DNS records of your IP addresses that point to your domain.
  
- Ideally, send all messages from the same IP address. If you must send from multiple IP addresses, use different IP addresses for different types of messages. For example, use one IP address for sending account notifications and a different IP address for sending promotional messages.

- Don't mix different types of content in the same messages. For example, don't include content about promotions in purchase receipt messages.

- Messages of the same category should have the same email address in the From: header. For example, messages from a domain called solarmora.com might have From: headers like this: Purchase receipt messages: receipt@solarmora.com Promotional messages: deals@solarmora.com Account notification messages: alert@solarmora.com Check regularly to make sure your domain isn’t listed as unsafe with Google Safe Browsing. To check your domain status, enter your domain in the Safe Browsing site status page. Also check any domain that’s linked to yours.

- Don’t send sample phishing messages or test campaigns from your domain. Your domain’s reputation might be negatively affected, and your domain could be added to internet blocklists. Don’t impersonate other domains or senders without permission. This practice is called spoofing, and it can cause Gmail to mark the messages as spam.

- To help prevent valid messages from being marked as spam: Messages that have a From address in the recipient’s Contacts list are less likely to be marked as spam. Occasionally, valid messages might be marked as spam. Recipients can mark valid messages as not spam, so future messages from the sender should be delivered to their inbox.
