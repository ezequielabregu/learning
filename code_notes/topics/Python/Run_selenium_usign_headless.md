# Run bot selenium using headless mode

[**Back to index**](../../README.md)

## Switch to Python Selenium headless mode

[Reference link](https://www.zenrows.com/blog/headless-browser-python#scrape-the-data)

Once the page is open, the rest of the process will be easier. Of course, we don't want the browser to appear on the monitor, and we want Chrome to run headlessly. Switching to headless chrome in Python is pretty straightforward.

We only need to add two lines of code and use the options parameter when calling the Webdriver.

```python
from selenium import webdriver 
from selenium.webdriver.chrome.service import Service as ChromeService 
from webdriver_manager.chrome import ChromeDriverManager 
 
url = "https://scrapeme.live/shop/" 
options = webdriver.ChromeOptions() #newly added 
options.headless = True #newly added 
with webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options) as driver: #modified 
 driver.get(url)
```

But is the code successfully reaching the page? How to verify if the scraper opens the right page? Is there any error? Those are some questions raised when you perform a Python Selenium headless.

Your code must create a log for as much as needed to address the question above.

The logs can vary, and it depends on your needs. It can be a log file in text format, a dedicated log database, or a simple output on the Terminal.

For simplicity, we'll create a log by outputting the scraping result on the terminal. Let's add two lines of code below to print the page URL and title. That'll ensure that the Python Selenium headless runs as wanted.

```python
with webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options) as driver: 
 # ... 
 print("Page URL:", driver.current_url) 
 print("Page Title:", driver.title)
```

## Driving Headless Chrome with Python

[Reference link](https://www.geeksforgeeks.org/driving-headless-chrome-with-python/)

Normal mode

```python
import time

from selenium import webdriver

# initializing webdriver for Chrome
driver = webdriver.Chrome()

# getting GeekForGeeks webpage
driver.get('https://www.geeksforgeeks.org')

# sleep for 5 seconds just to see that
# the browser was opened indeed
time.sleep(5)

# closing browser
driver.close()
```

### Drive headless Chrome

Here we will automate the browser with headless, for we will use this function:

webdriver.Chrome(): Returns us an instance of Chrome driver through which we will be interacting with Chrome browser.
Options(): Through attributes of this class we can send browser launch parameters. In our case it is options.headless = True which will launch browser without UI(headless).
driver.get(url): Send the browser a signal to get the specified URL.
print(driver.title): Print webpage title into the terminal where we running our script.
driver.close(): Send the browser a signal to close itself.

```python
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# instance of Options class allows
# us to configure Headless Chrome
options = Options()

# this parameter tells Chrome that
# it should be run without UI (Headless)
options.headless = True

# initializing webdriver for Chrome with our options
driver = webdriver.Chrome(options=options)

# getting GeekForGeeks webpage
driver.get('https://www.geeksforgeeks.org')

# We can also get some information
# about page in browser.
# So let's output webpage title into
# terminal to be sure that the browser
# is actually running.
print(driver.title)

# close browser after our manipulations
driver.close()
```

## easyprocess.EasyProcessCheckInstalledError: cmd=['Xvfb', '-help'] OSError=[Errno 2] No such file or directory

[Reference link](https://stackoverflow.com/questions/32173839/easyprocess-easyprocesscheckinstallederror-cmd-xvfb-help-oserror-errno)

I encountered this error on a Mac and (finally) solved it by install Xquartz: <https://www.xquartz.org/> (reboot after install!)

## How I can attach the mouse movement (pyautogui) to pyvirtualdisplay with selenium webdriver (python)

[Reference link](https://stackoverflow.com/questions/35798478/how-i-can-attach-the-mouse-movement-pyautogui-to-pyvirtualdisplay-with-seleniu/36372919#36372919)

[Reference link 2](https://forums.raspberrypi.com/viewtopic.php?t=284391)

```python
import os
from pyvirtualdisplay import Display
import pyautogui
import Xlib.display

v_display = Display(visible=1, size=(1600,900))
v_display.start()  # this changes the DISPLAY environment variable
# sadly, pyautogui does not detect this change
pyautogui._pyautogui_x11._display = Xlib.display.Display(
                os.environ['DISPLAY']
            )
...
pyautogui.click(...)  # clicks on v_display
...

v_display.stop()
```

## PYAUTOGUI HEADLESS Docker mode without display in Python

[Reference link](https://abhishekvaid13.medium.com/pyautogui-headless-docker-mode-without-display-in-python-480480599fc4)

So lets begin to crack the code-

1. Firstly you do not set your scrapper to Headless mode, the scrapper will by default become headless inside Docker which we will make latter in this post. It will use a virtual display and will be headless by default.

2. Second you have to create a virtual Display as shown in the above image in side your scrapper.py file. The code for the same is down below.

```python
from pyvirtualdisplay.display import Display
disp = Display(visible=True, size=(1366, 768), backend="xvfb", use_xauth=True)
disp.start()
```

As shown above, you first import Pyvirtual display. Then you set visible to True, Size resolution is the size of the display you are using on your device, set backend to xvfb and set use_xauth to true.

3. Start you display by disp.start(), as shown.

Now you have to provide PYAUTOGUI the path to your display. To do so use the code-

```python
import Xlib.display
import pyautogui
pyautogui._pyautogui_x11._display = Xlib.display.Display(os.environ['DISPLAY'])
```

As shown you now after the display has started import pyautogui inside your scrapper.py script and provide pyautogui the path to Xdisplay.

After this you get your website and start using pyautogui as intended and make the whole scrapper or do whatever you wish to do with it. After this we make the docker image.

Now it is time for the Grand finale- to actually build a docker image to run PYAUTOGUI Headless

```bash
FROM ubuntu:bionic

RUN apt-get update && apt-get install python3 tesseract-ocr python3-pip curl unzip -yf
# Install Chrome
RUN apt-get update -y
RUN apt-get install -y dbus-x11
RUN curl https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -o /chrome.deb
RUN dpkg -i /chrome.deb || apt-get install -yf
RUN rm /chrome.deb
RUN apt-get -y install libssl1.0-dev
RUN curl https://chromedriver.storage.googleapis.com/87.0.4280.20/chromedriver_linux64.zip -o /usr/local/bin/chromedriver.zip
RUN cd /usr/local/bin && unzip chromedriver.zip
RUN chmod +x /usr/local/bin/chromedriver
USER root
RUN apt update
RUN apt-get install -y poppler-utils
RUN apt-get clean
RUN apt install -y python3 python3-pip
RUN DEBIAN_FRONTEND=noninteractive apt install -y python3-xlib python3-tk python3-dev
RUN apt install -y xvfb xserver-xephyr
python3-tk python3-dev
RUN Xvfb :99 -ac &
RUN export DISPLAY=:99
RUN pip3 install virtualenv
RUN mkdir /app
COPY requirements.txt /app
WORKDIR /app
RUN virtualenv .venv
RUN /bin/bash -c "source .venv/bin/activate"
RUN pip3 install -r requirements.txt
RUN apt-get install -y scrot
COPY . /app
ENTRYPOINT [ "python3", "scrapper.py" ]
```

So as shown in the above code you first get ubuntu operating system in your dockerfile because Xdisplay works on Ubuntu.

Then install python3 and chromedriver using the subsequent commands in the same script.

After this set user to root and install python3-xlib, python3-tk and python3-dev and do set DEBIAN_FRONTEND=noninteractive as they are needed to run pyautogui in ubuntu and docker does not allow intervention while installation.

After this install xvfb for virtual display backend capability.

Then RUN xvfb display on port 99 and assign port 99 in export display to docker.

After this RUN the virtual environment and provide path for activation using the subsequent commands in the script above.

Now a basic requirement.txt for installation should look something like the code below.

## Setup the browser windows size

[Reference link 1](https://stackoverflow.com/questions/64311719/selenium-unable-to-locate-element-only-when-using-headless-chrome-python)

[Reference link 2](https://stackoverflow.com/questions/55012755/how-to-set-window-size-in-selenium-chrome-python)

Along with specifying --no-sandbox option, try changing the window size passed to the webdriver

`chrome_options.add_argument('--window-size=1920,1080')`

This window size worked in my case.

Can you please try this.

`options.add_argument('window-size=1920x1080')`

OR

`options.add_argument("--start-maximized")`

- Example

```python
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

options = Options()
options.add_argument("--headless")
options.add_argument("window-size=1400,600")
driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\Utility\BrowserDrivers\chromedriver.exe', service_args=["--log-path=./Logs/DubiousDan.log"])
driver.get("http://google.com/")
print ("Headless Chrome Initialized")
print(driver.get_window_size())
driver.set_window_size(1920, 1080)
size = driver.get_window_size()
print("Window size: width = {}px, height = {}px".format(size["width"], size["height"]))
driver.quit()
```

## importing pyautogui in ubuntu throwing KEYERROR :DISPLAY

[Reference link](https://stackoverflow.com/questions/52244164/importing-pyautogui-in-ubuntu-throwing-keyerror-display)

You have to correctly set environment variable DISPLAY. It should be defined by your OS. If it's not, you can define it manually.

- Option 1 - run python with:

`DISPLAY=:0 python`

- Option 2 - set environment variable in Python

`import os`

os.environ['DISPLAY'] = ':0'

- Option 3 - put it to you .bashrc file

`echo "DISPLAY=:0" >> ~/.bashrc`

`source ~/.bashrc`

## Pyvirtualdisplay

[Reference link](https://github.com/ponty/PyVirtualDisplay)

pyvirtualdisplay is a python wrapper for [Xvfb][1], [Xephyr][2] and [Xvnc][3]

Links:

- home: <https://github.com/ponty/pyvirtualdisplay>
- PYPI: <https://pypi.python.org/pypi/pyvirtualdisplay>

![workflow](https://github.com/ponty/pyvirtualdisplay/actions/workflows/main.yml/badge.svg)

Features:

- python wrapper
- supported python versions: 3.6, 3.7, 3.8, 3.9, 3.10, 3.11
- back-ends:  [Xvfb][1], [Xephyr][2] and [Xvnc][3]

Possible applications:

- headless run
- GUI testing
- automatic GUI screenshot

Installation
============

install the program:

```console
python3 -m pip install pyvirtualdisplay
```

optional: [Pillow][pillow] should be installed for ``smartdisplay`` submodule:

```console
python3 -m pip install pillow
```

optional: [EasyProcess][EasyProcess] should be installed for some examples:

```console
python3 -m pip install EasyProcess
```

optional: xmessage and gnumeric should be installed for some examples.

On Ubuntu 20.04:

```console
sudo apt install x11-utils gnumeric
```

If you get this error message on Linux then your Pillow version is old.

```
ImportError: ImageGrab is macOS and Windows only
```

Install all dependencies and backends on Ubuntu 20.04:

```console
sudo apt-get install xvfb xserver-xephyr tigervnc-standalone-server x11-utils gnumeric
python3 -m pip install pyvirtualdisplay pillow EasyProcess
```

Usage
=====

Controlling the display with context manager:

```py
from pyvirtualdisplay import Display
with Display() as disp:
    # display is active
    print(disp.is_alive()) # True
# display is stopped
print(disp.is_alive()) # False
```

Controlling the display with `start()` and `stop()` methods (not recommended):

```py
from pyvirtualdisplay import Display
disp = Display()
disp.start()
# display is active
disp.stop()
# display is stopped
```

After Xvfb display is activated "DISPLAY" environment variable is set for Xvfb.
(e.g. `os.environ["DISPLAY"] = :1`)
After Xvfb display is stopped `start()` and `stop()` are not allowed to be called again, "DISPLAY" environment variable is restored to its original value.

Selecting Xvfb backend:

```py
disp=Display()
# or
disp=Display(visible=False)
# or
disp=Display(backend="xvfb")
```

Selecting Xephyr backend:

```py
disp=Display(visible=True)
# or
disp=Display(backend="xephyr")
```

Selecting Xvnc backend:

```py
disp=Display(backend="xvnc")
```

Setting display size:

```py
disp=Display(size=(100, 60))
```

Setting display color depth:

```py
disp=Display(color_depth=24)
```

Headless run
------------

A messagebox is displayed on a hidden display.

```py
# pyvirtualdisplay/examples/headless.py

"Start Xvfb server. Open xmessage window."

from easyprocess import EasyProcess

from pyvirtualdisplay import Display

with Display(visible=False, size=(100, 60)) as disp:
    with EasyProcess(["xmessage", "hello"]) as proc:
        proc.wait()

```

Run it:

```console
python3 -m pyvirtualdisplay.examples.headless
```

If `visible=True` then a nested Xephyr window opens and the GUI can be controlled.

vncserver
---------

The same as headless example, but it can be controlled with a VNC client.

```py
# pyvirtualdisplay/examples/vncserver.py

"Start virtual VNC server. Connect with: vncviewer localhost:5904"

from easyprocess import EasyProcess

from pyvirtualdisplay import Display

with Display(backend="xvnc", size=(100, 60), rfbport=5904) as disp:
    with EasyProcess(["xmessage", "hello"]) as proc:
        proc.wait()

```

Run it:

```console
python3 -m pyvirtualdisplay.examples.vncserver
```

<!-- embedme doc/gen/vncviewer_localhost:5904.txt -->
Check it with vncviewer:

```console
vncviewer localhost:5904
```

![](doc/gen/vncviewer_localhost:5904.png)

GUI Test
--------

```py
# pyvirtualdisplay/examples/lowres.py

"Testing gnumeric on low resolution."
from easyprocess import EasyProcess

from pyvirtualdisplay import Display

# start Xephyr
with Display(visible=True, size=(320, 240)) as disp:
    # start Gnumeric
    with EasyProcess(["gnumeric"]) as proc:
        proc.wait()

```

<!-- embedme doc/gen/python3_-m_pyvirtualdisplay.examples.lowres.txt -->
Run it:

```console
python3 -m pyvirtualdisplay.examples.lowres
```

Image:

![](doc/gen/python3_-m_pyvirtualdisplay.examples.lowres.png)

Screenshot
----------

```py
# pyvirtualdisplay/examples/screenshot.py

"Create screenshot of xmessage in background using 'smartdisplay' submodule"
from easyprocess import EasyProcess

from pyvirtualdisplay.smartdisplay import SmartDisplay

# 'SmartDisplay' instead of 'Display'
# It has 'waitgrab()' method.
# It has more dependencies than Display.
with SmartDisplay() as disp:
    with EasyProcess(["xmessage", "hello"]):
        # wait until something is displayed on the virtual display (polling method)
        # and then take a fullscreen screenshot
        # and then crop it. Background is black.
        img = disp.waitgrab()
img.save("xmessage.png")

```

<!-- embedme doc/gen/python3_-m_pyvirtualdisplay.examples.screenshot.txt -->
Run it:

```console
python3 -m pyvirtualdisplay.examples.screenshot
```

Image:

![](doc/gen/xmessage.png)

Nested Xephyr
-------------

```py
# pyvirtualdisplay/examples/nested.py

"Nested Xephyr servers"
from easyprocess import EasyProcess

from pyvirtualdisplay import Display

with Display(visible=True, size=(220, 180), bgcolor="black"):
    with Display(visible=True, size=(200, 160), bgcolor="white"):
        with Display(visible=True, size=(180, 140), bgcolor="black"):
            with Display(visible=True, size=(160, 120), bgcolor="white"):
                with Display(visible=True, size=(140, 100), bgcolor="black"):
                    with Display(visible=True, size=(120, 80), bgcolor="white"):
                        with Display(visible=True, size=(100, 60), bgcolor="black"):
                            with EasyProcess(["xmessage", "hello"]) as proc:
                                proc.wait()

```

<!-- embedme doc/gen/python3_-m_pyvirtualdisplay.examples.nested.txt -->
Run it:

```console
python3 -m pyvirtualdisplay.examples.nested
```

Image:

![](doc/gen/python3_-m_pyvirtualdisplay.examples.nested.png)

xauth
=====

Some programs require a functional Xauthority file. PyVirtualDisplay can
generate one and set the appropriate environment variables if you pass
``use_xauth=True`` to the ``Display`` constructor. Note however that this
feature needs ``xauth`` installed, otherwise a
``pyvirtualdisplay.xauth.NotFoundError`` is raised.

Concurrency
===========

If more X servers are started at the same time then there is race for free display numbers.

_"Recent X servers as of version 1.13 (Xvfb, too) support the -displayfd command line option: It will make the X server choose the display itself"_
<https://stackoverflow.com/questions/2520704/find-a-free-x11-display-number/>

Version 1.13 was released in 2012: <https://www.x.org/releases/individual/xserver/>

First help text is checked (e.g. `Xvfb -help`) to find if `-displayfd` flag is available.
If `-displayfd` flag is available then it is used to choose the display number.
If not then a free display number is generated and there are 10 retries by default
which should be enough for starting 10 X servers at the same time.

`displayfd` usage is disabled on macOS because it doesn't work with XQuartz-2.7.11, always 0 is returned.

Thread safety
=============

All previous examples are not thread-safe, because `pyvirtualdisplay` replaces `$DISPLAY` environment variable in global [`os.environ`][environ] in `start()` and sets back to original value in `stop()`.
To make it thread-safe you have to manage the `$DISPLAY` variable.
Set `manage_global_env` to `False` in constructor.

```py
# pyvirtualdisplay/examples/threadsafe.py

"Start Xvfb server and open xmessage window. Thread safe."

import threading

from easyprocess import EasyProcess

from pyvirtualdisplay.smartdisplay import SmartDisplay


def thread_function(index):
    # manage_global_env=False is thread safe
    with SmartDisplay(manage_global_env=False) as disp:
        cmd = ["xmessage", str(index)]
        # disp.new_display_var should be used for new processes
        # disp.env() copies global os.environ and adds disp.new_display_var
        with EasyProcess(cmd, env=disp.env()):
            img = disp.waitgrab()
            img.save("xmessage{}.png".format(index))


t1 = threading.Thread(target=thread_function, args=(1,))
t2 = threading.Thread(target=thread_function, args=(2,))
t1.start()
t2.start()
t1.join()
t2.join()

```

Run it:

```console
python3 -m pyvirtualdisplay.examples.threadsafe
```

Images:

![](doc/gen/xmessage1.png)
![](doc/gen/xmessage2.png)

Hierarchy
=========

![Alt text](https://g.gravizo.com/source/svg?https%3A%2F%2Fraw.githubusercontent.com/ponty/pyvirtualdisplay/master/doc/hierarchy.dot)

[1]: http://en.wikipedia.org/wiki/Xvfb
[2]: http://en.wikipedia.org/wiki/Xephyr
[3]: https://tigervnc.org/
[pillow]: https://pillow.readthedocs.io
[environ]: https://docs.python.org/3/library/os.html#os.environ
[EasyProcess]: https://github.com/ponty/EasyProcess
