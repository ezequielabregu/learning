# Python DSP

[**Back to index**](../../README.md)

## Autotune implemented in Python

(Wolfsound version)

<https://thewolfsound.com/how-to-auto-tune-your-voice-with-python/>

<https://youtu.be/JLQF5fDkgYo>

<https://github.com/JanWilczek/python-auto-tune>
