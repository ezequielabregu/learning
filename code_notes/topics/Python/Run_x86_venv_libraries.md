# Run x86 venv & libraries on mac M1 Sillicon

[**Back to index**](../../README.md)

[Reference link](https://www.roguelynn.com/words/m1-dev-setup/)

[Reference link](https://pareekshithkatti.medium.com/setting-up-python-for-data-science-on-m1-mac-ced8a0d05911)

## Solution 1: Change the Terminal architecture

### Install Rosseta2

This will install Rosetta 2 on your Mac. Once installed, you can run x86-based applications, including Python x86 libraries.

`/usr/sbin/softwareupdate --install-rosetta --agree-to-license`

### Change terminal to x86-64

`arch -x86_64 /bin/bash`

You can create a virtual environment on your M1 Mac and install the x86 version of Python within the virtual environment.

Check the architecture:

```bash
arch

uname -m
```

### Create the virtual environment

`python3.9 -m venv venv_x86 --arch x86_64`

or use absolute path for the rigth x86-64 Python version.
Example:

`/usr/local/bin/python3-intel64 -m venv venv_x86`

### activate the virtual environment

`source venv_x86/bin/activate`

---

## Solution 2: iTerm2 for Rosetta and Native

You may find it a lot easier to have two copies of iTerm.app (or Terminal.app), one that runs with Rosetta, and one that does not. Having both makes it easy to visually separate which environment you’re working in, as you can now customize the look and theme of each terminal app.

### Setup

1. Open Finder to /Applications (or /Applications/Utilities if using Terminal.app).

2. Create a copy of iTerm.app (or Terminal.app).

3. Name the copy Rosetta-iTerm.app (or Rosetta-Terminal.app, or whatever that makes sense to you).

4. Right-click on the new Rosetta terminal copy, and click “Get Info”.

5. Check “Open using Rosetta” then close the “Get Info” window.

Check the architecture:

```bash
arch

uname -m
```

---

## Solution 3: Python virtual environments with pyenv

[Link Reference](http://sixty-north.com/blog/pyenv-apple-silicon.html)

Apple's recent transition to the new architecture for its Mac computers has caused rather predictable problems for developers whose workflow depends on certain versions of pre-compiled libraries for x86 architecture. While the latest releases of Python come with a universal installer that allows to build universal binaries for M1 systems, those who prefer to manage Python environments with pyenv, may find it difficult to choose the correct version for installation.

This problem can be solved by installing both x86 and arm64 Python executables. To do that, we need to be able to run pyenv in x86 mode and make sure that all system dependencies are met for both architectures. In other words, we'll need both x86 and arm64 Homebrew packages that we'll keep separate using two installations of Homebrew.

First of all, to be able to run x86 executables, we'll need to install Rosetta:

$ softwareupdate —-install-rosetta
Now we can install the x86 Homebrew:

$ arch -x86_64 /bin/bash -c "$(curl -fsSL <https://raw.githubusercontent.com/Homebrew/install/master/install.sh>)"
It will be installed in the /usr/local/bin/ directory. For convenience, you can create an alias by adding the following line in your shell configuration file:

alias brew86="arch -x86_64 /usr/local/bin/brew"
Now we can invoke the x86 Homebrew as brew86 and install packages required by pyenv:

$ brew install openssl readline sqlite3 xz zlib

$ brew86 install openssl readline sqlite3 xz zlib
You can check whether the installation was successful and you have packages for both architectures using the file command, for example:

$ file /opt/homebrew/Cellar/openssl@1.1/1.1.1k/bin/openssl
/opt/homebrew/Cellar/openssl@1.1/1.1.1k/bin/openssl: Mach-O 64-bit executable arm64

$ file /usr/local/Cellar/openssl@1.1/1.1.1k/bin/openssl
/usr/local/Cellar/openssl@1.1/1.1.1k/bin/openssl: Mach-O 64-bit executable x86_64
To install x86 Python, you'll need to call pyenv with the arch -x86_64 prefix. For convenience, let's create an alias for this command by adding the following line in the shell config file:

alias pyenv86="arch -x86_64 pyenv"
Now you can install x86 Python binaries by calling:

$ pyenv86 install <PYTHON_VERSION>
By default, pyenv doesn't allow you to specify custom names for the installed Python versions, but you can use the pyenv-alias plugin to give your installations appropriate names:

$ VERSION_ALIAS="3.x.x_x86" pyenv86 install 3.x.x
Note that with aliases for your pyenv and Homebrew installations, you’ll have to specify them in all commands and locations, for example:

$ CFLAGS="-I$(brew86 --prefix openssl)/include" \
LDFLAGS="-L$(brew86 --prefix openssl)/lib" \
VERSION_ALIAS="3.x.x_x86" \
pyenv86 install -v 3.x.x
