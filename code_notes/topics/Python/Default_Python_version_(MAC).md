# Default Python Version (MAC)

[**Back to index**](../../README.md)

[link reference](https://dev.to/malwarebo/how-to-set-python3-as-a-default-python-version-on-mac-4jjf)

```bash
sudo ln -s /usr/local/bin/pip3 /usr/local/bin/pip
```

```bash
ln -s -f /usr/local/bin/python3.7 /usr/local/bin/python
```
