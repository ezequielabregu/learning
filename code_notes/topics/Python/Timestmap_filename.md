# Send Email list

[**Back to index**](../../README.md)

[Reference link](https://stackoverflow.com/questions/10607688/how-to-create-a-file-name-with-the-current-date-time-in-python)

## Solution 1

```python
from datetime import datetime

datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")
```

>`'2020_08_12-03_29_22_AM'`

## Solution 2

```python
import datetime

time_now  = datetime.datetime.now().strftime('%m_%d_%Y_%H_%M_%S') 
print(time_now)
```

>02_03_2021_22_44_50

## Solution 3

```python
import datetime

def print_time():
    parser = datetime.datetime.now() 
    return parser.strftime("%d-%m-%Y %H:%M:%S")

print(print_time())
```

> 03-02-2021 22:39:28
