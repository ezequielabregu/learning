# Executable Python file

[**Back to index**](../../README.md)

1. Ensure the first line of your Python script is

   `#!/usr/bin/env python`

2. Change the extension of the file to `.command`

(i.e. If the file you want to make executable is called Test.py, change it to Test.command)

3. In Terminal make the Python script file executable by running:

`chmod +x Test.command`
