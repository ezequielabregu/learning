def split(list):
    '''
    Devide the unsorted list at midpoint into sublists
    Returns two sublists - left and right
    Takes overall O(log n) time
    '''
    mid = len(list)//2
    left = list[:mid]
    right = list[mid:]
    
    return left, right

def merge(left, right):
    '''
    Merges two lists (arrays), sorting them in the process
    Retuns a new merged list

    Runs in overall O(n) time
    '''
    l = []
    i = 0
    j = 0

    while i < len(left) and j < len(right):
        if left[i] < right[j]:
            l.append(left[i])
            i += 1
        else:
            l.append(right[j])
            j += 1

    while i < len(left):
        l.append(left[i])
        i+=1

    while j < len(right):
        l.append(right[j])
        j+=1
    
    return l        

def merge_sort(list):
    '''
    Sort the list in a ascending order
    Return a new sorted list
    
    Devide: Find the midpoint of the list an devide into sublists
    Conquer: Recursevely sort the sublist created in previous step
    Combine: Merge the sorted sublists created in previous step

    Takes overall O(log n) times
    '''

    #if the list has 1 element, is already sorted
    if len(list) <= 1:
        return list
    
    #devide (split) the list in two parts
    left_half, rigth_half = split(list)
    left = merge_sort(left_half)
    right = merge_sort(rigth_half)

    return merge(left, right)

def verify_sorted(list):
    n = len(list)
    #if the list has 0 or 1 element
    if n == 0 or n == 1:
        return True
    #recursive evaluation, incremental advance from second element to the end
    return list[0] < list[1] and verify_sorted(list[1:])



alist = [54, 62, 93, 17, 77, 31, 44, 55, 20]
l = merge_sort(alist)
print(l)
print (verify_sorted(alist))
