# Algorithms & Data Structures

[**Back to index**](../../README.md)

[Algorithms and Data Structures Tutorial](https://youtu.be/8hly31xKli0)


This course was developed by Pasan Premaratne and Jay McGavren. It was made possible by a grant from **teamtreehouse.com**

## Definition

A set of steps or instructions for completing a task.

## Linear Search

- Move sequencially to a targuet.
- The algorithm repeat the same task until reach the target.

### Definitions

1. Start at the beginning
2. Compare current value to targuet
3. Move sequentially
4. Reach end of list

#### Requirements

- Must have a defined problem statement, input, and output.
- The steps in the algorithm need to be ina very specific order.
- The steps also need to be distinct.
- The algorithm should produce a result.
- The algorithm should complete in a finite amount of time.

## Algorithm problem definition

- To identify the problem we must define the input and the output.

- Prove of induction:

## Efficiency Evaluation

To evaluate the efficiency we must consider:

1. **Time complexity** (Running time): how long takes the algorithm to complete the task
2. **Space complexity** (): how much memory the algorithm consumes to complete the task.
3. **Average Case** (Related to Time complexity)
4. **Worst case**: The algorithm never will perform worst than this.

![Linear search performance](./images/00_linear_search_algorithm.png)
*Linear search performance*

## Binary search

Return the index in the list in which the target is located.

1. The steps in the algorithm need to be in a very specific order
2. The stpes also need to be distintic
3. The algorithm should produce a result
4. The algorithm should complete in a finite amount of time

### Requirements

1. Detetect the middle position of a sorted list
2. Compare the target with the middle position
3. If it doesn't match, compare the target if is lower or higher.
4. If it is, determine a new middle position of the range in which the target is located.
5. Repeat the process until the target is found.
6. If the target is not found, the target doesn't exist in the list.

![Linear search performance](./images/01_Binary_vs_Linear_performance.png)

## Big O

Theoretical definition of the complexity of an algorithm as a function of the size.

O(n) = Order of the magnitude of complexity - A fucntion of a size

O(1) indicate that the time is constant.

O(log)

¿How many time it must be multiply to reach the target?

log2 16 = 4
log2 8 = 3

log2 of n + 1 = Logarithm runtime

![Log runtime](images/02_Logarithm_runtime.png)

O(n)The worst case scenario in the linear search is equal to the end of value range (LINEAR TIME).

## Quadratic runtime

![quadratic runtime](images/03_quadratic_runtime.png)

## Cubic runtime

## Quasilinear runtime

![](images/04_performance_comparison.png)

## Merge sort

O(n log n)

Example:

- Unsorted list

8 4 5 1 3 2 6 7

- Split the list in the middle

8 4 5 1
3 2 6 7

- Split the list again

8 4
5 1
3 2
6 7

- split again

8
4
5
1
3
2
6
7

- Sort in two elements

4 8
1 5
2 3
6 7

- Sort again in groups of four

1 4 5 8
2 3 6 7

- Sort again

1 2 3 4 5 6 7 8

![Merge Sort](images/05_Merge_sort.png)

## Worst case runtime

- Linear runtime

- Logarithm runtime = O(log n)

