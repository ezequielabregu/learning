'''
Requirements:

- the list must be sorted
- the target is knowed

Steps:

1. Middle position of the list
2. Compare the element with the value of the middle position
3. If the target and the middle position match, the output is true. 
    If not, the middle position value is compare if is lower than the target
    If it is lower, from middle position to the end is splited
    If is is not lower, from the middle position to the start is splited
    
'''

def linear_search(list, target):
    for i in range(0, len(list)):
        if list[i] == target:
            return i
    return None

def verify(index):
    if index is not None:
        print("Target found at index: ", index)
    else:
        print("Target not found in list")

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

result = linear_search(numbers, 1)

verify(result)


