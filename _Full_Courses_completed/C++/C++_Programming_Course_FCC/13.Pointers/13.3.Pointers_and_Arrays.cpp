#include <iostream>
using namespace std;


int main()
{
    int luckyNumbers[5] = {2,3,5,7,9};
    //this line gives the pointer address of the array
    cout << luckyNumbers << endl;
    // this line gives the same address as before = the name of the address is the same as the first element address
    cout << &luckyNumbers[0] << endl;
    // gives the third element of the array
    cout << luckyNumbers[2] << endl;
    // this line gives the same value as previous value
    // means adress of the fist element od the array + 2 more spaces. The result will be 5
    cout << *(luckyNumbers + 2) << endl;
    // iteration= looping to enter manualy five values for my array
    for (int i = 0; i <= 4; i++){
        cout << "Number ";
        cin >> luckyNumbers[i];
    }
    // show the array values using the technic of the first element address + the value of i 
    for (int i = 0; i <= 4; i++){
        cout << *(luckyNumbers + i) << " ";
    }
}