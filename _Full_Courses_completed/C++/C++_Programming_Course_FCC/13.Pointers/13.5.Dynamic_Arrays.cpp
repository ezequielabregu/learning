#include <iostream>
using namespace std;

int main()
{   
    // static arrays: the array store values in each memory space - MUST USE a fix size value
    //int myArray[5];
    // what happen if add o delete elements of an array?
    // there are two names = Allocate (new) and deallocate (delete)
    // Alocate uses a new space of free memory
    // Deallocate delete and leave a spece of free memory ready to use

    // declare a pointer as array. The size 
    int size;
    cout << "size: ";
    cin >> size;
    // the user specified the size of this array
    // new = allocate --> write a value 
    // delete = deallocate --> make free a memory space
    // if I use NEW I must use delete to free memory
    int* myArray = new int[size]; 
    // in this iteration you can enter manualy each element of a array (size entered by the user)
    for(int i=0; i<size;i++){
        cout << "Array[" << i << "] ";
        cin >> myArray[i];
    }
    // print all elements of myArray
    for(int i=0; i<size;i++){
        cout << myArray[i] << " ";
        // following 
        // cout << *(myArray+i) << " ";
    }
    // I MUST deallocate memory usind delete[] --> delete the memory used by the array
    delete[]myArray;
    // allow the value of nothing to myArray (is NOT using memory anymore)
    myArray = NULL;
}