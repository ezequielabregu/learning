#include <iostream>
using namespace std;

// POINTERS store addresses (memory location)
// pointer must be the same type of the variable

int main()
{   // n is a variable stored in memory address
    int n = 5;
    // & get the memory address of a specific variable
    cout << &n << endl;
    //declare a point int*, ptr(pointer name)
    // ptr must be equal &n (must be the same type)
    int* ptr = &n;
    cout << ptr << endl;
    // require the pointer store value (n = 5 in this case) 
    cout << *ptr << endl;
    // get a new value for *p
    *ptr = 10; 
    cout << *ptr << endl;
    // n = *ptr
    cout << n << endl;

    // declarin this variable give an address memory to the pointer
    int v;
    // memory address given by the variable declared
    int* ptr2 = &v;
    *ptr2 = 7;
    cout << "v= " << v << endl;
}