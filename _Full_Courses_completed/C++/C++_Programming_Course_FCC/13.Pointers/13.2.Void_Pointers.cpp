#include <iostream>
using namespace std;

// create the function with a pinter as an argument (integer data type)
void printNumber(int* numberPtr){
    // print the argument
    cout << *numberPtr << endl;
}
// char data type
void printLetter(char* charPtr){
    // print the argument
    cout << *charPtr << endl;
}
// void* (void pointer) can use ANY data type (become generic pointer)
// the 1º parameter data type will be identified by de 2º parameter (is a reference data type)
// if the 2º parameter is 'i', void* will be integer
void print(void* ptr, char type){
    switch (type) {
        case 'i': cout << *((int*)ptr)<< endl; //handle int* (integer pointer)
        case 'c': cout << *((char*)ptr)<< endl; // handle char* (char pointer)
    }
}

int main()
{
    int number = 5;
    char letter = 'a';
    // print number variable as a pointer
    printNumber(&number);
    printLetter(&letter);
    print(&number, 'i');
    print(&letter, 'c');
}