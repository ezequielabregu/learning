#include <iostream>
using namespace std;

// this function get the minimum value of an array
int getMin(int numbers[], int size) {
    int min = numbers[0];
    //iteration scaning the array searching the minimum value 
    // it starts from the second index [1] because we just already know the value of the first index [0]
    for(int i=1; i<size; i++){
        // if the value of the array is minor than min, so min get a new value. (Is starts from the first value)
        if (numbers[i]<min)
        min = numbers[i];
    }
    return min;
}
// this function gets the maximum value of an array
int getMax(int numbers[], int size) {
    int max = numbers[0];
    for(int i=1; i < size; i++){
        if (numbers[i] > max)
        max = numbers[i];
    }
    return max;
}
// this function returns two values.
// we use pointers as a parameters. Then, we invoque using addresses (&) of each pointer
void getMinMax(int numbers[], int size, int* min, int* max){
    for(int i=1; i < size; i++){
        if (numbers[i] > *max)
            *max = numbers[i];
         if (numbers[i] < *min)
            *min = numbers[i];
    }
}
int main()
{   
    int numbers[5]={5, 4,-2,29,6};
    //cout << "Min is " << getMin(numbers, 5) << endl;
     //cout << "Max is " << getMax(numbers, 5) << endl;
    
    int min = numbers[0];
    int max = numbers[0];
    // %min and &max are adresses of *min and *max pointers
    getMinMax(numbers, 5, &min, &max);
    cout << "min is "  << min << endl;
    cout << "min is "  << max << endl;
}