#include <iostream>
using namespace std;

int main()
{   // LEAP YEAR FORMULA = (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
    int year, month;
    cout << "Year, month: ";
    cin >> year >> month;

    switch(month)
    {
        case 2:(year % 4 == 0 && year % 100 != 0 || year % 400 == 0)? //ternary conditional
            cout << "29 days month": cout << "28 days month"; break;
        // following cases has the same characteristics. I don't use break; because in any case 4, 6, 9 or 11 will have the same legend 
        case 4: 
        case 6:
        case 9:
        case 11: cout << "30 days month"; break;
        // same as before. case 1, 3, 7 , 8, 10 and 12 has the same legend. DONT' USE break;
        case 1:
        case 3:
        case 7:
        case 8:
        case 10:
        case 12: cout << "31 days month"; break;
        default: cout << "NOT VALID";
    }
    
}