#include <iostream>
using namespace std;

int main()
{
    float num1, num2;
    char operation;
    cout << "*** Calculator ***" << endl;
    cin >> num1 >> operation >> num2;
    //operation input will be the condition to navigate the cases
    switch (operation)
    { //if operation is "-" s rest num1 and num 2. break is to stop the program
        case '-':cout << num1 << operation << num2 << "=" << num1 - num2; break;
        case '+':cout << num1 << operation << num2 << "=" << num1 + num2; break;
        case '/':cout << num1 << operation << num2 << "=" << num1 / num2; break;
        case '*':cout << num1 << operation << num2 << "=" << num1 * num2; break;
        case '%': // modulator operation must use integers
            bool isNum1Int, isNum2Int; 
            isNum1Int = ((int)num1==num1); // num1 integer converted is == to num1 -->> to see if num1 is integer
            isNum2Int = ((int)num2==num2);
            // if isNum1Int is TRUE and isNum2Int is TRUE
            if (isNum1Int && isNum2Int) // == isNum1Int == TRUE && isNum2Int == TRUE
                cout << num1 << operation << num2 << "=" << int(num1) % int(num2); 
            else
                cout << "Not valid!" << endl;
            break;
        // if the user use another symbol or operator 
        default: cout << "Not valid operation" << endl;    
    }
}