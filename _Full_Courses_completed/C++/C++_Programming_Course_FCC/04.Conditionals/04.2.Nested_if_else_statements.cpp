#include <iostream>
using namespace std;

// user enter side length of a triangle (a,b,c)
// Program should write out wheter that tiangle is equilateral, isosceles or scalene

int main()
{
    float a, b, c;
    cout << "a, b, c: ";
    cin >> a >> b >> c;

    if(a==b && b==c)
    {
        cout << "Equilateral triangle";
    }
    else
    {
        if(a != b && a != c && b != c)
        {
            cout << "Scalene triangle";
        }
        else
        {
            cout <<"Issosceles triangle";
        }
    }

}