#include <iostream>
using namespace std;

int main()
{   // both expression are the same
    //convert char to int
    cout << (int) 'a' << endl;
    //convert char to int
    cout << int('a') << endl;
    // capital case
    cout << int('A') << endl;
    // convert a number into ASCII 
    cout << char(65) << endl;

}