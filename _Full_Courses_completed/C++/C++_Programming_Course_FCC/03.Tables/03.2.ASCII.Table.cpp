#include <iostream>
using namespace std;

int main()
{   // i can create multiples same type variables 
    char c1, c2, c3, c4, c5;
    cout <<"Enter 5 letters: ";
    // the user can enter mutiple characters of the same TYPE
    cin >> c1 >> c2 >> c3 >> c4 >> c5;
    // ASCII to message converter
    cout << "ASCII message: " << int(c1) << " " << int(c2)
    << " " << int(c3) << " " << int(c4) << " " << int(c5);

}