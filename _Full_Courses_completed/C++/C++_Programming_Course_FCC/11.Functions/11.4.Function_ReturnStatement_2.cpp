#include <iostream>
using namespace std;

bool isPrimeNumber(int number){
        // initial value
    bool isPrimeFlag = true;
    // start on 2 because prime number es divisble by 1 or by itself
    for(int i=2; i<number; i++){
        // if number has rest (%) != 0, so is prime number 
        if (number % i == 0) {
            return false;
        }
    }
    // it must declare the RETURN of the function
    return true;
}

int counter = 0;
int main()
{
   for (int i = 1; i <= 1000000; i++){
    bool isPrime = isPrimeNumber(i);
    if(isPrime){
        cout << i << " is prime number" << endl;
        counter++;
        }
   }
    cout << "The is " << counter << " prime numbers" << endl;   
}
