#include <iostream>
using namespace std;

// declare generic data type variable with template
// to avoid repeat functions with diferent  data types variables
template<typename T> // = template<typename Type> 

// swap values of two variables
void swap(T& a, T& b){
    T temp = a;
    a = b;
    b = temp;
}

int main()
{
int a = 5, b = 7;
cout << a << " -  " << b << endl;
// swap function declaring int data type
swap<int>(a, b);
cout << a << " - " << b << endl;

char c = 'c', d = 'd';
cout << c << " - " << d << endl;
// declare char data type
swap<char>(c, d);
cout << c << " - " << d << endl;
}