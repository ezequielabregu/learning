#include <iostream>
using namespace std;

// Recursion is when a function invoques itselfs

// sum numbers between a range m-n
int recursiveSum(int m, int n){ // init 2, 4
    if (m == n)
        return m;
    return m + recursiveSum(m + 1,n);
    // 1º lap = 2 + 4 = 6
    // 2º lap = 6 + 3 = 9
    // end of the recurtion because m == n ( 4 == 4)       
}

int main()
{
    int m = 2, n = 4;
    cout << "Sum= " << recursiveSum(m, n) << endl;
}
