#include <iostream>
using namespace std;

// this function is a bollean one!
bool isPrimeNumber(int number){
        // initial value
    bool isPrimeFlag = true;
    // start on 2 because prime number es divisble by 1 or by itself
    for(int i=2; i<number; i++){
        // if number has rest (%) != 0, so is prime number 
        if (number % i == 0) {
            return false;
        }
    }
    // it must declare the RETURN of the function
    return true;
}

int main()
{
    int number;
    cout << "number: ";
    cin >> number;
    bool isPrimeFlag = isPrimeNumber(number);
//if isPrimeFlag is TRUE, print the output...    
    if (isPrimeFlag)
        cout << "Prime number baby" << endl;
    else 
        cout << "Not Prime number baby" << endl;
}
