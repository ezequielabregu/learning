#include <iostream>
using namespace std;

// int age = 0 to avoid and error if the user forget the parameter
void introduceMe(string name, string city, int age=0 ){
    cout << "My name is " << name << endl;
    cout << "I'm from " << city << endl;
    // if the user forget the parameter 'age', don`t print the next line
    if (age != 0)
        cout << "I'm " << age << " year old" << endl;
}

int main()
{
    // introduceMe(" Ezequiel", "Buenos Aires", 43);
    // introduceMe(" Saldina", "NY");

    string name, city;

    int age;
    cout << "Name: ";
    cin >> name;
    cout << "City: ";
    cin >> city;
    cout << "Age: ";
    cin >> age;

    introduceMe(name, city, age);
}