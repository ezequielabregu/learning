#include <iostream>
using namespace std;

// declaration function ( it's very important!)
void myFunction(); 

int main()
{
    cout << "Hello from main()" << endl;
    myFunction();
}

// definition function
void myFunction() {
    cout << "Hello from function()" << endl;
}