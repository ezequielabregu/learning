#include <iostream>
#include <list>
using namespace std;

// represent a real world object in form of "programming format"
// create an obbject called youtubeChannel
class youtubeChannel {
// access modifier---> public: all variables are available to use globally
public:    
    string Name;
    string ownerName;
    int suscribersCount;
    list<string> publishedVideoTitles;
};

int main()
{
    youtubeChannel ytChannel;
    ytChannel.Name = "CodeBeauty";
    ytChannel.ownerName = "Saldina";
    ytChannel.suscribersCount = 1800;
    ytChannel.publishedVideoTitles = {"video 1", "video 2", "video 3" };
    
    cout << "Name: " << ytChannel.Name << endl;
    cout << "Owner Name: " << ytChannel.ownerName << endl;
    cout << "Suscriber Count: " << ytChannel.suscribersCount << endl;
    cout << "Videos: " << endl;
    for(string videoTitle:ytChannel.publishedVideoTitles){
        cout << videoTitle << endl;
    }
}