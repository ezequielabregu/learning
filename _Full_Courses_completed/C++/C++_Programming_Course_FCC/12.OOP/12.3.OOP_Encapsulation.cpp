#include <iostream>
#include <list>
using namespace std;

class youtubeChannel {
//Encapsulation: means that the information inside the class is private, not public
private:    
    string Name;
    string ytName;
    int suscribersCount;
    list<string> publishedVideoTitles;
// this information is PUBLIC
public:
    youtubeChannel(string name, string ownerName) {
        Name = name;
        ytName = ownerName;
        suscribersCount =  0;
    }
    // CLASS METhods: this is to avoid repeat code
    void getInfo() {
        cout << "Name: " << Name << endl;
        cout << "Owner Name: " << ytName << endl;
        cout << "Suscriber Count: " << suscribersCount << endl;
        cout << "Videos: " << endl;
        for(string videoTitle : publishedVideoTitles){
            cout << videoTitle << endl;
        }
    }
    void suscribe(){
        suscribersCount++;
    }
    void unsuscribe(){
        if(suscribersCount > 0)
            suscribersCount--;
    }
    void publishVideo(string title) {
        // invoque a private encapsulated variable
        publishedVideoTitles.push_back(title);
    }
};

int main()
{  
    youtubeChannel ytChannel1("CodeBeauty", "Saldina");
    // add some videos to the list of strings 
    ytChannel1.publishVideo("video 1");
    ytChannel1.publishVideo("video 2");
    ytChannel1.publishVideo("video 3");
  
    //the following information is PUBLIC
    ytChannel1.suscribe();
    ytChannel1.suscribe();
    ytChannel1.suscribe();
    ytChannel1.suscribe();
    ytChannel1.unsuscribe();
    ytChannel1.getInfo();

}