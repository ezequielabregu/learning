#include <iostream>
#include <list>
using namespace std;

// represent a real world object in form of "programming format"
// create an obbject called youtubeChannel
class youtubeChannel {
// access modifier---> public: all variables are available to use globally
public:    
    string Name;
    string ytName;
    int suscribersCount;
    list<string> publishedVideoTitles;
    //CONSTRUCTOR
    // Constructor: must have the same name as the class
    // Constructor doesn't have return type
    youtubeChannel(string name, string ownerName) {
        Name = name;
        ytName = ownerName;
        suscribersCount =  0;
    }
    // CLASS METHOD: this is to avoid repeat code
    void getInfo() {
    cout << "Name: " << Name << endl;
    cout << "Owner Name: " << ytName << endl;
    cout << "Suscriber Count: " << suscribersCount << endl;
    cout << "Videos: " << endl;
    for(string videoTitle : publishedVideoTitles){
        cout << videoTitle << endl;
        }
    }
};

int main()
{   // invoque the constructors
    // it can use diferent name for the constructor
    youtubeChannel ytChannel1("CodeBeauty", "Saldina");
    // add some videos to the list of strings 
    ytChannel1.publishedVideoTitles.push_back("video 1");
    ytChannel1.publishedVideoTitles.push_back("video 2");
    ytChannel1.publishedVideoTitles.push_back("video 3");

    youtubeChannel ytChannel2("AmySing", "Amy");

    //invoque the class method
    ytChannel1.getInfo();
    ytChannel2.getInfo();
}