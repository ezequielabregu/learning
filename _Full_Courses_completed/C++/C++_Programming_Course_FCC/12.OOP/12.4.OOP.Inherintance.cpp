#include <iostream>
#include <list>
using namespace std;

class youtubeChannel {
//Encapsulation: means that the information inside the class is private, not public
private:    
    string Name;
    int suscribersCount;
    list<string> publishedVideoTitles;
// protected is accesible from a derivate class
protected:
    string ytName;
// this information is PUBLIC
public:
    youtubeChannel(string name, string ownerName) {
        Name = name;
        ytName = ownerName;
        suscribersCount =  0;
    }
    // CLASS METhods: this is to avoid repeat code
    void getInfo() {
        cout << "Name: " << Name << endl;
        cout << "Owner Name: " << ytName << endl;
        cout << "Suscriber Count: " << suscribersCount << endl;
        cout << "Videos: " << endl;
        for(string videoTitle : publishedVideoTitles){
            cout << videoTitle << endl;
        }
    }
    void suscribe(){
        suscribersCount++;
    }
    void unsuscribe(){
        if(suscribersCount > 0)
            suscribersCount--;
    }
    void publishVideo(string title) {
        // invoque a private encapsulated variable
        publishedVideoTitles.push_back(title);
    }
};
// INHERITANCE: this new class invoque a public part of youtubeChannel class (see above)
class cookingYoutubeChannel:public youtubeChannel {
public:    
    //CONSTRUCTOR: must have the same name as the class
    cookingYoutubeChannel(string name, string ownerName):youtubeChannel(name, ownerName) {
    } 
    // METHOD   
    void practice() {
        cout << ytName << " practicing cooking, learning new recipes" << endl;
    }
};

int main()
{  
    cookingYoutubeChannel ytChannel("Amy's Kitchen", "Amy");
    cookingYoutubeChannel ytChannel2("John's Kitchen", "John");
    ytChannel.publishVideo("Video 1");
    ytChannel.publishVideo("Video 2");
    ytChannel.suscribe();
    ytChannel.suscribe();
    ytChannel.getInfo();
    ytChannel.practice();
    ytChannel2.practice();
}

