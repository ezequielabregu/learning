#include <iostream>
#include <list>
using namespace std;

//Polymorphism: a class to take multiple forms. SAME NAME, DIFFERENT IMPLEMENTARIONS
class youtubeChannel {

private:    
    string Name;
    int suscribersCount;
    list<string> publishedVideoTitles;
protected:
    string ytName;
    int contentQuality;
public:
    youtubeChannel(string name, string ownerName) {
        Name = name;
        ytName = ownerName;
        suscribersCount =  0;
        contentQuality = 0;
    }
    void getInfo() {
        cout << "Name: " << Name << endl;
        cout << "Owner Name: " << ytName << endl;
        cout << "Suscriber Count: " << suscribersCount << endl;
        cout << "Videos: " << endl;
        for(string videoTitle : publishedVideoTitles){
            cout << videoTitle << endl;
        }
    }
    void suscribe(){
        suscribersCount++;
    }
    void unsuscribe(){
        if(suscribersCount > 0)
            suscribersCount--;
    }
    void publishVideo(string title) {
        // invoque a private encapsulated variable
        publishedVideoTitles.push_back(title);
    }
    void checkAnalytics(){
        if(contentQuality<5)
            cout <<  Name << " has quality content" << endl;
        else
            cout <<  Name << " has great content" << endl;
    }
};
// INHERITANCE: this new class invoque a public part of youtubeChannel class (see above)
class cookingYoutubeChannel:public youtubeChannel {
public:    
    cookingYoutubeChannel(string name, string ownerName):youtubeChannel(name, ownerName) {
    } 
    // METHOD   
    void practice() {
        cout << ytName << " practicing cooking, learning new recipes" << endl;
        contentQuality++;
    }
};

class singerYoutubeChannel:public youtubeChannel {
public:    
    singerYoutubeChannel(string name, string ownerName):youtubeChannel(name, ownerName) {
    } 
    void practice() {
        cout << ytName << " is taking classes, learning new songs " << endl;
        contentQuality++;
    }
};

int main()
{  
    cookingYoutubeChannel cookingYtChannel("Amy's Kitchen", "Amy");
    singerYoutubeChannel singersYtChannel("John Sing", "John");

    cookingYtChannel.practice();
    singersYtChannel.practice();
    singersYtChannel.practice();
    singersYtChannel.practice();
    singersYtChannel.practice();

    // creating two pointers
    youtubeChannel * yt1 = &cookingYtChannel;
    youtubeChannel * yt2 = &cookingYtChannel;

    yt1->checkAnalytics();
    yt2->checkAnalytics();
}
