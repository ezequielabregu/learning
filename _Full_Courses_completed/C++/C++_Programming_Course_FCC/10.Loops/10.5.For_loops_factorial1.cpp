#include <iostream>
using namespace std;

int main()
{
    // the factorial of a number
    // 6!= 1*2*3*4*5*6=720

    int number;

    cout << "Number: ";
    cin >> number;
    
      int factorial = 1;
    // declare the variable; condition, operation program
    for (int i=1; i <= number; i++) {
        factorial = factorial * i;
    }
cout << number << "!=" << factorial <<endl;
}

/*
1. take the variable value ( i=1)
2. evaluate de condition i <= number
3. make the code between brackets {}
4. do the operation i++
*/
