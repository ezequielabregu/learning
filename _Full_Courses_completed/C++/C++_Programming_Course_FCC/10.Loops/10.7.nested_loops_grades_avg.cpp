#include <iostream>
using namespace std;

int main()
{
int grade, sum = 0;
    for(int i = 0; i < 3; i++){

        do{
            cout << "Enter grade "<< i + 1 << " : ";
            cin >> grade; 
        // if grade is minor to 1 or grather than 5 repeat the same. Else, continue with for.
        } while(grade < 1 || grade > 5);
        sum += grade;
    }
    cout << "Sum = " << sum << endl;
    // it must use float to devide integers
    cout << "Average grade = "<<(float)sum / 3.0 << endl;
}