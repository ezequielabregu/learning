 #include <iostream>
using namespace std;

int main()
{
    int userPin = 1234, pin, errorCounter = 0;
    // WITH DO while, first execute the code and then evaluate the condition
    do { 
        cout << "PIN: ";
        cin >> pin;
    if (pin != userPin)
    errorCounter ++; 
    // if you don't use && pin != userPin the program never will continue to the next step
    // the program will continues only if the user enter a wrong PIN
    } while (errorCounter < 3 && pin != userPin);

if (errorCounter < 3)
    cout << "Loading app..";
else 
    cout <<"Blocked..." << endl;
    
}