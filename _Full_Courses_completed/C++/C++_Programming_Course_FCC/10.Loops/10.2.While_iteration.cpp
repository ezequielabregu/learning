#include <iostream>
using namespace std;

int main()
{
    //count digits of a number
    int number;
    cout << "Number ";
    cin >> number;

    if (number == 0)
        cout << "You have entered 0\n";
    else{
        // if number is negative, else follow the loop
        if(number<0)
            number = -1 * number; // positive number converter
        int counter = 0;
        while(number > 0) {
            number /=10;    //  number = number / 10; ---> divido por 10 hasta que de 0 o menos que 0
            counter++;      // how many times number can be divided by 10 and the result is grater t 0
      
        } 
    cout << "Number cointains " << counter << " digits\n";      
    }
}