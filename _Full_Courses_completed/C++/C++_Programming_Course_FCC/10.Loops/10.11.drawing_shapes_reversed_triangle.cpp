#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int length;

    cout << "Length: ";
    cin >> length;
    
    char symbol;
    cout << "Symbol: ";
    cin >> symbol;

    for (int i = length; i >= 1; i--){
        // i determine wow many times the loop is repeated
        for(int j = 1; j <= i; j++){
            cout << setw(2) << symbol;
        }
    cout << endl;
    }
}

