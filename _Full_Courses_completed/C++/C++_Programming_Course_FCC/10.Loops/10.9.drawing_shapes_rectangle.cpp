#include <iostream>
// this library include a function called setw() (set with) to set the field of the function 
#include <iomanip>
using namespace std;

int main()
{
    // drawing a rectangle
    int height, with;
    cout << "Height: ";
    cin >> height;
    cout << "With: ";
    cin >> with;
    char symbol;
    cout << "Symbol: ";
    cin >> symbol;

    for(int h = 0; h < height; h++){
        for(int w = 0; w < with; w++){
            // setw() setup, 3 characters of with of my shape
            cout << setw(3) << symbol;
        }
    cout << endl;
    } 
}