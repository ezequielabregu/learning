#include <iostream>
using namespace std;

int main()
{      
    float annualSalary;
    cout << "Enter your annual salary\n";
    cin >> annualSalary;
    float monthlySalary = annualSalary / 12;            
    cout << "Your monthly salary is $" << monthlySalary << endl;
    cout << "In 10 years you will earn $" << monthlySalary * 10 << endl; 
    // character use single quotations marks ' '
    char character = 'a';
    //system("read -p 'Press any key to continue' var");    
}
