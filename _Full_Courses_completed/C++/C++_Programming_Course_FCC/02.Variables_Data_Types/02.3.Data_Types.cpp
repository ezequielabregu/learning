#include <iostream>
using namespace std;

int main()
{      
    int yearOfBirth = 1995;
    char gender = 'f';
    bool isOlderThan18= true;
    float averageGrade = 4.5;
    double balance = 3453453451345;
    // sizeof: how much memory this variable is using
    cout << "Size on int is "<<sizeof(double)<< " bytes\n";
    cout << "Int min value is " << INT_MIN << endl;
    // INT_MAX max range to represent using 4 bytes (int) = 2^16 bits (2^4 bytes)
    cout << "Int max value is " << INT_MAX << endl;
    // max value without the sign
    cout << "Size of unsigned in t is " <<sizeof(unsigned int) << "bytes\n"; 
    cout << "UInt max value is " << UINT_MAX << endl;
    cout << "Size of bool is " << sizeof(bool) << " bytes\n";
    cout << "Size of char is " << sizeof(char) << " bytes\n";
    cout << "Size of float is " << sizeof(float) << " bytes\n";
    cout << "Size of double is " << sizeof(double) << " bytes\n";
    //system("read -p 'Press any key to continue' var");    
}
