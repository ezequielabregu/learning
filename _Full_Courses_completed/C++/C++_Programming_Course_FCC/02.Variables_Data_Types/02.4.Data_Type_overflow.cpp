#include <iostream>
using namespace std;

int main()
{
    // max numbers using int
    int intMax = INT_MAX;
    cout << intMax << endl;
    // Data type overflow: works like a anti-clock, but in reverse in this case, fot that reason is negative
    cout << intMax + 1 << endl;

}