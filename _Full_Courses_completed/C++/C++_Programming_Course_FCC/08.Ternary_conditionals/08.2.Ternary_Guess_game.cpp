#include <iostream>
using namespace std;

int main()
{
    system("clear");
    int hostUserNum, guestUserNum;
    cout << "Host: ";
    cin >> hostUserNum;
    system("clear");
    cout << "Guest: ";
    cin >> guestUserNum;
    
    //Ternary conditional structure --> Condition? TRUE : FALSE 
    (hostUserNum == guestUserNum)? cout << "You win!" << endl :  cout << "Fail!" << endl;    
}