#include <iostream>
using namespace std;

int main()
{
    // +,-,*,/,%
    cout << 5 +  2 << endl;
    // 5 / 2 = 2 --> INTEGER
    // one or both must have .0
    cout << 5.0 / 2.0 << endl;
    // module operator
    cout << 5 % 2 << endl;


    // ++ incremental
    int counter = 7;
    counter++;
    cout << counter << endl;
    counter--;
    cout << counter << endl;


    int counter2 = 7;
    // counter2++ happends POST increment. The result will be post
    cout << counter2++ <<endl;
    cout << counter2-- <<endl;
    cout << counter2 <<endl;

    // <,<,>=,<=, ==, !=
    int a = 5, b = 8;
    // if a > b = 1 (TRUE)
    cout << (a > b) << endl;
    cout << (a != b) << endl;

    // LOGICAL OPERATORS
    // &&, ||, ! (not)
    cout << (a == 5 && b == 5) << endl;
    cout << (a == 5 || b == 5) << endl;
    // if 1 is NOT = 5 or b is NOT ==5
    cout << !(a == 5 || b == 5) << endl;
    // the operator + has priority
    cout << (a == 5 && b == 5+3) << endl;


    // ASSIGNMENT OPERATORS
    // /=, +=, -=, /=, %=
    int x = 5;
    x += 7; //  ---> x = x + 7;
   
    cout << x << endl;
}

//PRIORITY ORDER
// 1° priority = // +,-,*,/,%
// 2° priority = <,<,>=,<=, ==, !=
// 3| priority = &&, ||, ! (not)