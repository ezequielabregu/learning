#include <iostream>
using namespace std;

int main()
{
    int a = 5, b = 4;
    cout << a + (b++) << endl; // = 9
    cout << (--a) + b << endl; // = 9

    cout << !(true && false) << endl; // FALSE
    cout << !(true && false || true) << endl;

    int x = 7;
    cout << x++ << endl;  // after the X, ++ doesn´t has priority 
    cout << ++x << endl; // before the X, ++ has priority
    
    int y = 7;
    cout << (++y <= 9 && y + 2 >= 10) << endl; // 8 <= 9 && 10 >= 10 ---> TRUE

    int w = 3;
    cout << (w == w++) << endl; // j++ doesn´t have priority

    float z = 8;
    z += 2;
    cout << z << endl; // 10
    z /= 2; // z / 2
    cout << z << endl;
}   