#OS: module to interact with operative system
import os
#.remove method remove a file
os.remove("novel.txt")

#rename a file
os.rename("guests.txt", "final_guests.txt")

#check if a file exists inside the folder
os.path.exists("guest.txt")

#get filesize
os.path.getsize("final_guests.txt")

