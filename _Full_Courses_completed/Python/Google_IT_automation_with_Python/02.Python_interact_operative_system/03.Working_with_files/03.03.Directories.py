'''More documentation in

https://docs.python.org/3/library/os.html

https://docs.python.org/3/library/os.path.html

https://en.wikipedia.org/wiki/Unix_time '''

import os

#get the current directorie
print(os.getcwd())


#create a new directorie
print(os.mkdir("new_dir"))

#go / enter to the new directorie
os.chdir("new_dir")
print(os.getcwd())

#remove directories (it must be empty! insted)
os.rmdir("new_dir")

#get the content of the current directorie (output a LIST)
os.listdir("website")