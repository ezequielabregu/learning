import csv

#a list of dictionaries, the keys are name, username, department
users = [   {"name": "Sol", "username": "Solm", "department": "IT"},
            {"name": "Lio", "username": "Lionel", "department": "UX"},
            {"name": "Grey", "username": "greyc", "department": "Dev"}]
#define the keys we want to write to the file
keys = ["name", "username", "department"]

#open the file for writting
with open("by_department.csv", "w") as by_department:
    #create the dictionary with the keys defined before
    writer = csv.DictWriter(by_department, fieldnames=keys)
    #create the csv header with the keys name, username, department
    writer.writeheader()
    #write the nexts rows with the users list of dictionaries content
    writer.writerows(users)