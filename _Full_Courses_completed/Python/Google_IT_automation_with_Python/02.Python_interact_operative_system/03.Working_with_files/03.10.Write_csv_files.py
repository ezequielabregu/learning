import csv

#create a list with the data to create the csv
hosts = [["workstation.local", "192.168.0.1"], ["webserver", "10.2.1.1"]]

#create the csv file
with open('host.csv', 'w') as host_csv:
    #create an instance of csv.writer class
    writer = csv.writer(host_csv)
    #.writerows method: create all the rows at the same time 
    writer.writerows(hosts)
