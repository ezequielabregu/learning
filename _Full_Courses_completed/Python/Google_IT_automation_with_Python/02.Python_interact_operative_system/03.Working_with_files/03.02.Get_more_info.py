#get filesize
import os
print(os.path.getsize("final_guests.txt"))

#get timestamp
print(os.path.getatime("final_guests.txt"))


#______________________________________________________
import datetime

#get the timestamp in a more readable format
timestamp = os.path.getmtime("final_guests.txt")
print(datetime.datetime.fromtimestamp(timestamp))

#get the absolute path
print(os.path.abspath("final_guests.txt"))