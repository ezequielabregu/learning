import csv

with open('software.csv') as software:
    #csv.DictReader conver the csv in a dictionary {"row":"value"}
    reader = csv.DictReader(software)
    for row in reader:
        print(("{} has {} users").format(row["name"],(row["users"])))