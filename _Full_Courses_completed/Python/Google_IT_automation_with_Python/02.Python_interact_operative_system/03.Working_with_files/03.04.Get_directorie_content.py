import os

dir = "website"

#iterate over the directory "website" content (iterate over each file)
for name in os.listdir(dir):
    #.path.join method create a full path (ex: website/favicon.ico)
    fullname = os.path.join(dir, name)
    # if fullname is a directory
    if os.path.isdir(fullname):
        print(f"{fullname} is a directory")
    else: 
        print(f"{fullname} is a file")