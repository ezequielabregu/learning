import re

# * means  match pi followed by any number of other characters followed by n
#the span of the match is the whole word
print(re.search(r"Py.*n", "Pygmalion"))

print(re.search(r"Py.*n", "Python Programming"))

# match='Python'
print(re.search(r"Py[a-z]*n", "Python Programming"))

#just match a pattern ol
#<re.Match object; span=(1, 3), match='ol'>
print(re.search(r"o+l+", "goldfish"))


print(re.search(r"o+l+", "woolly"))

#The P wasn't present but with the question mark we marked it as optional
print(re.search(r"p?each", "To each their own"))

print(re.search(r"p?each", "I like peaches"))