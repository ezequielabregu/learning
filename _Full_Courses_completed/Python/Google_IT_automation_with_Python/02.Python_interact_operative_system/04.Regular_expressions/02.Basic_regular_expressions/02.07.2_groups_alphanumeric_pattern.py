import re
def check_character_groups(text):
  result = re.search(r"\b\w+\s+\w+\b", text)
  return result != None

print(check_character_groups("One")) # False
print(check_character_groups("123  Ready Set GO")) # True
print(check_character_groups("username user_01")) # True
print(check_character_groups("shopping_list: milk, bread, eggs.")) # False

'''  
\b  word boundery, marks a start of a word (also works as start of string)
\w+  means alphanumeric characters  [a-zA-Z0-9_]  (one or more)
\s+  one or more whitespaces
\w+  next alphanumeric characters (1+)
\b  word boundery (works at string end too) '''