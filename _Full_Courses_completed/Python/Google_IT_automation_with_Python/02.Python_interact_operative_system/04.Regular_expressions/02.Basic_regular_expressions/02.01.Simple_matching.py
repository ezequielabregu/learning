import re

#search the pattern aza
result1 = re.search(r"aza", "plaza")
result2 = re.search(r"aza", "bazaar")

#means start with x
result3 = re.search(r"^x", "xenon")

# . means no matter the character
result4 = re.search(r"p.ng", "penguin")

# re.IGNORECASE 
result5 = (re.search(r"p.ng", "Pangea", re.IGNORECASE))

print(result1)
print(result2)
print(result3)
print(result4)
print(result5)

#<re.Match object; span=(2, 5), match='aza'>