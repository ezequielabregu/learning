import re

#match welcome as a match 
print (re.search(r".com", "welcome"))
#<re.Match object; span=(2, 6), match='lcom'>

#\ = scaping character
print (re.search(r"\.com", "welcome"))
#None

#\w matches numbers, letters & _
#the patter match until the space, because it is out of this characters
print (re.search(r"\w*", "This is an example"))

#in this example, the pattern match with the whole phrase, due to the underscore
print (re.search(r"\w*", "This_is_an_example"))