import re

#match only alpha characters from a-z + way
print(re.search(r"[a-z]way", "the end of the highway"))

#match from a-z or from A-Z or form 0-9
print(re.search("cloud[a-zA-Z0-9]", "cloudly"))


#Fill in the code to check if the text passed contains punctuation symbols: 
#commas, periods, colons, semicolons, question marks, and exclamation points.
def check_punctuation (text):
  result = re.search(r"[,.:;?!]", text)
  return result != None

print(check_punctuation("This is a sentence that ends with a period.")) # True
print(check_punctuation("This is a sentence fragment without a period")) # False
print(check_punctuation("Aren't regular expressions awesome?")) # True
print(check_punctuation("Wow! We're really picking up some steam now!")) # True
print(check_punctuation("End of the line")) # False

# ^ exclude all the characters a-z + A-Z 
#in this case, only match spaces
# <re.Match object; span=(4, 5), match=' '>
print(re.search(r"[^a-zA-Z]", "This is a phrase."))

#adding an space at the end of the [] exclude spaces
print(re.search(r"[^a-zA-Z ]", "This is a phrase."))

# | means "or"
print(re.search(r"cat|dog", "I like cats"))

print(re.search(r"cat|dog", "I like dogs"))


# with findall, we don't need to choose between cats and dogs.
print(re.findall(r"cat|dog", "I like both dogs and cats."))

