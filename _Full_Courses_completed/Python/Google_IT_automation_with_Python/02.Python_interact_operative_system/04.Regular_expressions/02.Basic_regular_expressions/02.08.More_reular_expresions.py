import re

#match starting with A and end with a
print(re.search(r"A.*a", "Argentina"))

#adding the $ we specify that it must start with A and end with a
print(re.search(r"A.*a$", "Azerbaijan"))

#start with a letter from the begining
#characters or underscore
#then characters _ & numbers 
pattern = r"^[a-zA-Z_][a-zA-Z0-9_]*$"
print(re.search(pattern, "this_is_a_variable_name"))

print(re.search(pattern, "my_variable1"))

#this will produce NONE, because start with a number
print(re.search(pattern, "2_my_variable2"))

