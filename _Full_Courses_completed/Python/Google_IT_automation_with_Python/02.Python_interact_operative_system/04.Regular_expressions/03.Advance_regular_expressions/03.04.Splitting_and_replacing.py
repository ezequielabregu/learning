import re

#split list without .?!
print(re.split(r"[.?!]", "One sentence. Another one? And the last sentence!"))

#split list with .?! ---> it use ()
print(re.split(r"([.?!])", "One sentence. Another one? And the last sentence")) 

#substitute the email with another word
print(re.sub(r"[\w.%+-]+@[\w.-]+", "[REDACTED]", "Receive and email for go_nuts@gmail.com"))

''' So once again we'd use parentheses to create capturing groups. 
In the first parameter, we've got an expression that contains the two groups that we want 
to match: one before the comma and one after the comma. We want to use a second parameter 
to replace the matching string. We use backslash two to indicate the second captured group 
followed by a space and backslash one to indicate the first captured group. 
When referring to captured groups, a backslash followed by a number indicates the 
corresponding captured group'''

print(re.sub(r"^([\w .-*]*), ([\w .-])*$", r"\2 \1", "Lovelace", "Ada"))

