import re

#letters that repeating 5 times ("ghost" has 5 letters )
print(re.search(r"[a-zA-Z]{5}", "a ghost"))


#with findall method we find in the whole phrase
print(re.findall(r"[a-zA-Z]{5}", "a scary ghost has apear"))

#\b = matches all words with only 5 letters long
print(re.findall(r"\b[a-zA-Z]{5}", "a scary ghost has apear"))


print(re.findall(r"\w{5,10}", "a scary ghost has apear"))

print(re.search(r"\w{,20}", "a scary ghost has apear"))