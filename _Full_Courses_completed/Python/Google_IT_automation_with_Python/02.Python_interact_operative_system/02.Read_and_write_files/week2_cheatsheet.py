#Official doc in https://docs.python.org/3/library/functions.html#open

#read all the text file
file = open("file.txt")

#readfile method: read the first line
print(file.readline())

#read the second line
print(file.readline())

#read method: read all lines
print(file.read())

#IMPORTANT ---> close the file
file.close()

#a practical tip to don't forget close de text file

with open("file.txt") as file:
    print (file.readline())

#_______________________________________________________________________________

#iteration over each file line and remove empty characters using .strip() method

with open("file.txt") as file:
    for line in file:
        print(line.strip().upper())


#_______________________________________________________________________________

file = open("file.txt")
lines = file.readlines()
file.close()
#the file was closed but it can be access to the file LIST
print(lines)

#_______________________________________________________________________________
#open a new text file called novel.txt a its name is file (file = open("novel.txt"))
# "w" allows us write content in the file
# "r" (read) is the default 
# "a" append mode
#"r+" read + write mode
with open("novel.txt", "w") as file:
    file.write("It's a dark and stormy nigth")
