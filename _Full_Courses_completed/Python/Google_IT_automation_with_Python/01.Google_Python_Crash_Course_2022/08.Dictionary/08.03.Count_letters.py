def count_letters(text):
    #create an empty dictionary ( this is importart, because {} indicates a DICTIONARY)
    result = {}
    #iteration over the dictionary
    for letter in text:
        #if letter is not in the dictionarie (refers to the key)
        if letter not in result:
            result[letter] = 0    
        result[letter] += 1
    return result

print(count_letters("aaaaaa"))
print(count_letters('tenant'))
print(count_letters('solo le pido a dios'))