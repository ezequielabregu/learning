#you can iterate over dicntionaries
#to get the key (names)
file_counts = {'jpg':10, 'txt':14, 'csv':2, 'py':23}
for extension in file_counts:
    print(extension)

#to get names + values, we use .items method
for extension, amount in file_counts.items():
    print(f'There are {amount} files with the .{extension}')

#we can get keys and values using methods (.keys() & .values())
print(file_counts.keys())
print(file_counts.values())

#iterate over the dictionarie using the method .values
for value in file_counts.values():
    print(value)
