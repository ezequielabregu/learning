#declare an empty dictionary
x = {}
print(type(x))

#dictionaries use symbol names instead index 
file_counts = {'jpg':10, 'txt':14, 'csv':2, 'py':23}
print (file_counts)
print(file_counts['txt'])

#we can also get boolean output using the method in
print('jpg' in file_counts)
print('html' in file_counts)

#DICTIONARIES ARE MUTABLE
#adding and element to the dictionary
file_counts['cfg'] = 8
print(file_counts)

#changing a value of a key value
file_counts['csv'] = 17
print(file_counts)

# delete and element
del file_counts['cfg']
print(file_counts)

#return de element (default if it doesn't has a value)
print(file_counts.get('jpg', 'default'))

#return all keys
print(file_counts.keys())
