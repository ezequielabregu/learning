def groups_per_user(group_dictionary):
    user_groups = {}
	# Go through group_dictionary
    for group, users in group_dictionary.items():
		# Now go through the users in the group
        for user in users:
            '''the method .get(user, []) means 
            1) give a value for each user (admin, userA, userB), and it is =
            2) get the value for each user (return a string). 
            IMPORTANT: I must indicate that the default value is a LIST (user, [])
            3) convert group into a list using [], then add to de result of 2)  
            '''
            user_groups[user] = user_groups.get(user, []) + [group]

            '''*** A BETTER SOLUTION: ***
            if user in user_groups:
                user_groups[user].append(group)
            else:
                user_groups[user] = [group]
            '''
                  
    return(user_groups)

print(groups_per_user({"local": ["admin", "userA"],
		                "public":  ["admin", "userB"],
		                "administrator": ["admin"] }))



'''
Use dict.get(key[, default]) to assign default values
The code below is functionally equivalent to the original code above, but this solution is more concise.

When get() is called, Python checks if the specified key exists in the dict. If it does, then get() returns the value of that key. If the key does not exist, then get() returns the value specified in the second argument to get().

dictionary = {"message": "Hello, World!"}

data = dictionary.get("message", "")

print(data)  # Hello, World!
'''