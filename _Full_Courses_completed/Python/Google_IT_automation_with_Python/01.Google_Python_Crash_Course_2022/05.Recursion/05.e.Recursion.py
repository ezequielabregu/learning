"""Problem: all the positive even numbers smaller or equal to 8 """

def evenNums(num):
    print (num)
    #if num is an odd number, send a message!
    if num % 2 != 0:
        print ("Please, enter an even number")
    #this is the base of the recursion
    #if the recursion is less == 2, that's our limit
    elif num == 2:
        return num
    #recursion
    else:
        return evenNums(num-2)

evenNums(8)