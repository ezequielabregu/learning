#1) create the class 
class Piglet1:
    #2) define a method off the class called speak
    def speak(self):
        print('oink oink')
#3) create an instance of the class Piglet1
hamlet = Piglet1()
#4) invoque the instance hamlet and the function speak
hamlet.speak()

'''______________________________________________________________'''

#1) create the class 
class Piglet2:
    name = ''
    #2) define a method
    def speak(self):
        #we must use self.name point to name='' parameter 
        print("Oink! I'm {}! Oink!".format(self.name))
#3) create an instance of the class Piglet1
hamlet = Piglet2()
#define the name method
hamlet.name = "Hamlet"
#4) invoque the instance hamlet and the function speak
hamlet.speak()

petunia = Piglet2()
petunia.name = 'Petunia'
petunia.speak()

'''___________________________________________________________________'''

class DogYears:
    years = 0
    def pig_years(self):
        return self.years * 7

lucy = DogYears()
lucy.years = 13
print(f"Lucy has {lucy.pig_years()} old")

