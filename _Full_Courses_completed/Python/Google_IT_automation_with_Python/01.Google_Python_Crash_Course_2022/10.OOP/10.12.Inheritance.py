#inherintance is like a cpopy paste of a specific class
class Fruit:
    def __init__(self, color, flavor):
        self.color = color
        self.flavor = flavor

#INHERITANCE, create a new class with the same parameters of Fruit class 
class Apple(Fruit):
    pass
#this class has the sme parameters like Fruit class
class Grape(Fruit):
    pass


granny_smith = Apple('green', 'tart')
carnelian = Grape('purple', 'sweet')
print(granny_smith.color)
print(granny_smith.flavor)

'''__________________________________________________________________________________________'''

class Animal:
    sound = ""
    def __init__(self, name):
        self.name = name
    def speak(self):
        print("{sound} I'm {name}! {sound}".format(name=self.name, sound=self.sound))

#INHERITANCE (copy paste) the same Animal class's parameters
class Piglet(Animal):
    sound = 'Oink!'
hamlet = Piglet('Hamlet')
hamlet.speak()

class Cow(Animal):
    sound = 'moooo'
cow = Cow('Milky white')
cow.speak()

'''____________________________________________________________________________________________'''

class Clothing:
  material = ""
  def __init__(self,name):
    self.name = name
  def checkmaterial(self):
	  print("This {} is made of {}".format(self.name,self.material))
			
class Shirt(Clothing):
  material="Cotton"

polo = Shirt("Polo")
polo.checkmaterial()