class Apple:
    #_init_ create a constructor. self represent the instance + 2 more parameters
    def __init__(self, color, flavor):
        self.color = color
        self.flavor = flavor

jonagold = Apple("red", "sweet")
print(jonagold.color)



'''_______________________________________________________'''

class Person:
    #create the contructor with the variable name
    def __init__(self, name):
        #give the variable value
        self.name = name
    #create a function without variable    
    def greeting(self):
        #print a string followed by the self.name variable
        return "hi, my name is {}".format(self.name) 

# Create a new instance of the class (object)
some_person = Person("Ezequiel")  
# Call the greeting method
print(some_person.greeting())

'''_______________________________________________________'''

class Apple_txt:
    def __init__(self, color, flavor):
        self.color = color
        self.flavor = flavor
    #This method allows us to define how an instance of an object will be printed when it’s passed to the print() function    
    def __str__(self):
        return "This apple is {} and its flavor is {}".format(self.color, self.flavor)

#this is an option to return the Apple_txt directly, without use its parameters
jonagold = Apple_txt("red", "sweet")
print(jonagold)
