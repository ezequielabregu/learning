import random

class Server:
    #contructor
    def __init__(self):
        """Creates a new server instance, with no active connections."""
        #empy dictionary
        self.connections = {}
    #function: return a random connection load (1-10)   
    def add_connection(self, connection_id):
        """Adds a new connection to this server."""
        connection_load = random.random()*10+1
        # Add the connection to the dictionary with the calculated load
        self.connections[connection_id] = connection_load    
    def close_connection(self, connection_id):
        """Closes a connection on this server."""
        # Remove the connection from the dictionary
        self.connections.pop(connection_id)
    def load(self):
        """Calculates the current load for all connections."""
        total = 0
        #iterate over the values from connections dictionary
        for load in self.connections.values():
            total += load
        return total
    def __str__(self):
        """Returns a string with the current load of the server"""
        return "{:.2f}%".format(self.load())

# Create a new instance of Server() class
server = Server()

class LoadBalancing:
    def __init__(self):
        """Initialize the load balancing system with one server"""
        #create an empty dictionary
        self.connections = {}
        #call the Server() class to initialize  wtih one server (use [] to convert a float to list)         
        self.servers = [Server()]

    def add_connection(self, connection_id):
        """Randomly selects a server and adds a connection to it."""
        #.choice method: Return a random element from a self.servers list:
        server = random.choice(self.servers)
        # Add the server connection (.choice method) to the dictionary
        self.connections[connection_id] = server
        # Add the connection to the server (this server is an instance declared above) & call the method .add_connection (see above)
        server.add_connection(connection_id)

    def close_connection(self, connection_id):
        """Closes the connection on the the server corresponding to connection_id."""
        # Find out the right server / Close the connection on the server /Remove the connection from the load balancer
        #iterate over the servers list (must iterate for each element self.servers)
        for server in self.servers:
            if connection_id in server.connections:
                server.close_connection(connection_id)

    def avg_load(self):
        """Calculates the average load of all servers"""
        # Sum the load of each server and divide by the amount of servers
        total_load=0
        #iteration over server dictionary (return keys)
        for server in self.servers:
            total_load += server.load()
            return total_load / len(self.servers)
        return 0

    def ensure_availability(self):
        """If the average load is higher than 50, spin up a new server"""
        if self.avg_load() > 50:
            self.servers.append(Server())
        pass

    def __str__(self):
        """Returns a string with the load for each server."""
        loads = [str(server) for server in self.servers]
        return "[{}]".format(",".join(loads))
#End Portion 2#

l = LoadBalancing()
l.add_connection("fdca:83d2::f20d")
l.add_connection("fa:8d2::f2")
l.close_connection("fdca:83d2::f20d")
""" print(l.avg_load())

l.servers.append(Server())
print(l.avg_load())

l.close_connection("fdca:83d2::f20d")
print(l.avg_load())
 """

