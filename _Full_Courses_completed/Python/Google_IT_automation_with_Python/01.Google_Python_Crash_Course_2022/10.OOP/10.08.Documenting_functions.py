class Apple_txt:
    def __init__(self, color, flavor):
        self.color = color
        self.flavor = flavor
    #This method allows us to define how an instance of an object will be printed when it’s passed to the print() function    
    def __str__(self):
        return "This apple is {} and its flavor is {}".format(self.color, self.flavor)

#get the help documentation for this class. But this documentation is not enought
#help(Apple_txt)


'''____________________________________________________________________________________'''

def to_seconds(hours, minutes, seconds):
    """Returns the amount of seconds in the given hours, minutes, and seconds"""
    return hours*36 + minutes*60 + seconds
#return the senstence I wrote before
help(to_seconds)

