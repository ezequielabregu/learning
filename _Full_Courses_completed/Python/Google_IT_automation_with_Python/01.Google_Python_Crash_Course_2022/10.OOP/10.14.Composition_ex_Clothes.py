class Clothing:
    #create a dictionary 
    stock={ 'name': [],'material' :[], 'amount':[]}
    #constructor        
    def __init__(self,name):
        material = ""
        self.name = name
    #method to add elements to the dictionary    
    def add_item(self, name, material, amount):
        Clothing.stock['name'].append(self.name)
        Clothing.stock['material'].append(self.material)
        Clothing.stock['amount'].append(amount)
    #method to count stock by material
    def Stock_by_Material(self, material):
        count=0
        n=0
        for item in Clothing.stock['material']:
            if item == material:
                count += Clothing.stock['amount'][n]
                n+=1
        return count
#Inherintance
class shirt(Clothing):
    #give a value to the material value
  material="Cotton"
class pants(Clothing):
  material="Cotton"
#instance of the class shirt & pants 
polo = shirt("Polo")
sweatpants = pants("Sweatpants")
#call the method to add items into the polo class
polo.add_item(polo.name, polo.material, 4)
#call the method to add items to seatpants class
sweatpants.add_item(sweatpants.name, sweatpants.material, 6)
#return the current stock by calling the current_stock method
current_stock = polo.Stock_by_Material("Cotton")

print(current_stock)