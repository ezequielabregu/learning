#1) create a new Class
#the class name must start with capital letter
class Apple:
    color = ''
    flavor = ''

#2) create a new instance of Apple class, called jonagold
jonagold = Apple()
jonagold.color = 'red'
jonagold.flavor = 'sweet'

#print the instance called jonagold
print(jonagold.color)
print(jonagold.flavor)
print(jonagold.color.upper())

#Create a new instance of the class Apple 
golden = Apple()
golden.color = 'Yellow'
golden.flavor = 'Soft'

print(golden.color)
print(golden.flavor)
print(golden.color.upper())

