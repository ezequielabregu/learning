#create a class
class Repository:
    #Constructor
    def __init__(self):
        #create a dictionary (it will contain the package objects available in this repository instance)
        self.packages = {}
    #method to add elements to the dictionary   
    def add_package(self, package):
        self.packages[package.name] = package
    #method to get total size of all packages in our repository, by iterating over values of the dictionary
    def total_size(self):
        result = 0
        for package in self.packages.values():
              result += package.size
        return result

