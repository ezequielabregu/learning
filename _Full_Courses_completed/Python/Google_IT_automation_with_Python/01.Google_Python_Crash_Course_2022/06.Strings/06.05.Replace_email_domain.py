def replace_domain(email, old_domain, new_domain):
    #if @yahoo.com is in email
    if '@' + old_domain in email:
        #give @yahoo index
        index = email.index('@' + old_domain)
        #replace from index 0 to index + @ + gmail.com
        new_email = email[:index] + '@' + new_domain
        return new_email
    return email

#WARNING: parameters must be strings, using quotation ''  ""
print(replace_domain('ezequiel@yahoo.com', 'yahoo.com', 'gmail.com'))
