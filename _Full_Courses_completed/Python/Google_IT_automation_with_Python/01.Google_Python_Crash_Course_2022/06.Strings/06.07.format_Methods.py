#usinf .format(arguments) to convert any data type in a string 
name = 'Manny'
number = len(name) * 3
print('Hello {}, your lucky number is {}'.format(name, number))

#using decimals (2 decimals)
price = 7.5
with_tax = price * 1.09
print('Base price: ${:.2f}. With Tax: ${:.2f}'.format(price, with_tax))


#limiting how many decimals can be shown
# :>3 means use 3 spaces to align de number
# :>6.2f means use 6 spaces to align the number using 2 decimals only
def to_celsius(x):
    return (x - 32) * 5 / 9

for x in range(0, 101, 10):
    print('{:>3} F | {:>6.2f} C'.format(x, to_celsius(x)))


'''using new format method. The important difference with the format method is 
that it takes the value of the variables from the current context, 
instead of taking the values from parameters.
'''
item = "Purple Cup"
amount = 5
price = amount * 3.25
print(f'Item: {item} - Amount: {amount} - Price: {price:.2f}')