def double_word(word):
    double_word = str(word) * 2
    return double_word + str(len(double_word))

print(double_word("hello")) # Should return hellohello10
print(double_word("abc"))   # Should return abcabc6
print(double_word(""))      # Should return 0