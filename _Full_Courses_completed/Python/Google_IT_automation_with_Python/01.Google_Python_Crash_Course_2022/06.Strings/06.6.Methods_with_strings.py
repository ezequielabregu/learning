answer = 'YES'
if answer.lower() =="yes":
    print("user said yes")

#avoid whitespaces and tabs
user_input = ' yes   '
print(user_input.strip())
print(user_input.rstrip())
print(user_input.lstrip())


#count
print("The number of times e occurs in the string".count("e"))

#isnumeric
print('Forest'.isnumeric())
print('12345'.isnumeric())

#int 
print(int('123') + int('456'))

#join
#using spaces
print(' '.join(['This', 'is', 'a', 'phrase']))
#using ...
print('...'.join(['This', 'is', 'a', 'phrase']))

#split
print('this is a split example'.split())
