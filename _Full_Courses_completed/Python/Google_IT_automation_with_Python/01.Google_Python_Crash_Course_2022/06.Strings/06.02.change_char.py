''' STRINGS in Python are inmutable'''

message = 'A kong message with a silly typo'
new_message = message[:1] + ' l' + message[3:]
print(new_message)