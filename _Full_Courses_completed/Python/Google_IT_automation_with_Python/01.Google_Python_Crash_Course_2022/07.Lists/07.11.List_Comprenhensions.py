'''this is a traditional way to get multiples of 7'''

multiples = []
for x in range(1,11):
    multiples.append(x*7)
print (multiples)


'''this is a COMPRENHENSIONS method
let us create new lists based on sequences or ranges
in a range from 1 to 10, x will be multiplied by 7'''

multiples = [x*7 for x in range(1,11)]
print (multiples)


'''Another example
List of diferent programing lenguages'''
languages = ["Python", "Perl", "Ruby", "Go", "Java", "C"]
#1) it takes the each element of languages list. Then return the len of that element (in the first case, 'Python')
lengths = [len(language) for language in languages]
print(lengths)


'''Another examples
Gettting multiples of 3 from 0 to 100

Go from 0 to 100 (x) Append elements to the list if (x) is multiple of 3
'''
z = [x for x in range(0,101) if x % 3 == 0]
print (z)


''' The odd_numbers function returns a list of odd numbers between 1 and n, inclusively. 
Fill in the blanks in the function, using list comprehension. 
Hint: remember that list and range counters start at 0 and end at the limit minus 1.'''

def odd_numbers(n):
	return [x for x in range (1, n + 1) if x % 2 != 0]

print(odd_numbers(5))  # Should print [1, 3, 5]
print(odd_numbers(10)) # Should print [1, 3, 5, 7, 9]
print(odd_numbers(11)) # Should print [1, 3, 5, 7, 9, 11]
print(odd_numbers(1))  # Should print [1]
print(odd_numbers(-1)) # Should print []