''' Try out the enumerate function for yourself in this quick exercise. 
Complete the skip_elements function to return every other element from the list, 
this time using the enumerate function to check if an element is in an even position or an odd position.'''

def skip_elements(elements):
    skip_list = []
    for i in range(0, len(elements), 2):
        skip_list.append(elements[i])
    return skip_list

print(skip_elements(["a", "b", "c", "d", "e", "f", "g"])) # Should be ['a', 'c', 'e', 'g']
print(skip_elements(['Orange', 'Pineapple', 'Strawberry', 'Kiwi', 'Peach'])) # Should be ['Orange', 'Strawberry', 'Peach']