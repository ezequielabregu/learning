def full_emails(people):
    #declare an empty list
    result = []
    #iteration over the list (people) --> in this case, the list has two elements
    for email, name in people:
        #format the output
        result.append("{} <{}>".format(name, email))
    return result


print(full_emails([('alex@example.com', 'Alex Diego'),
                    ('shay@example.com', 'Shay Brandt')]))
                    