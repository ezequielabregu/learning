x = ['now', 'we', 'are', 'cooking']

#what kind of data type
print(type(x))

#list length
print(len(x))

#if 'are' is in the list --> True
print('are' in x)

#print an element of the list according to an index
print(x[0])
print(x[3])

#print from index to index
print(x[1:3])
print(x[:2])
print(x[2:])