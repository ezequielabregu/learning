filenames = ["program.c", "stdio.hpp", "sample.hpp", "a.out", "math.hpp", "hpp.out"]
# Generate newfilenames as a list containing the new filenames
# using as many lines of code as your chosen method requires.
newfilenames = []
for names in filenames:
    if "hpp" in names[len(names)-3 : len(names)]:
        changeExt = names[:len(names) - 2]
        newfilenames.append(changeExt)
    else:
        newfilenames.append(names)
print(newfilenames) 
# Should be ["program.c", "stdio.h", "sample.h", "a.out", "math.h", "hpp.out"]