fruits = ["Pineapple", "Banana", "Apple", "Melon"]
fruits.append("kiwi")
print(fruits)

#insert method ---> insert an element on the index 
fruits.insert(0, "Orange")
print(fruits)

#remove method 
fruits.remove("Melon")
print(fruits)

#change only one list element
fruits[2] = "Strawberry"
print(fruits)

#pop method: remove an element using index
fruits.pop(4)
print(fruits)