winners = ['Ashley', 'Dylan', 'Reese']

''' https://www.geeksforgeeks.org/enumerate-in-python/

Enumerate() method adds a counter to an iterable 
and returns it in a form of enumerating object. 
This enumerated object can then be used directly for loops 
or converted into a list of tuples using the list() method.'''


for index, person in enumerate(winners):
    print('{} - {}'.format(index + 1,person))